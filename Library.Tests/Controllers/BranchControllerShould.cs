﻿using Library.Controllers;
using Library.Database.Entities.Content;
using Library.Services.Interfaces;
using Moq;
using NUnit.Framework;
using System.Collections.Generic;

namespace Library.Tests.Controllers
{
    [TestFixture]
    public class BranchControllerShould
    {
        [Test]
        public void Return_Branch_Index_View()
        {
            var mockBranchService = new Mock<IBranchService>();
            mockBranchService.Setup(r => r.GetAll()).Returns(GetAllBranches());
            var controller = new BranchController(mockBranchService.Object);
        }

        [Test]
        public void Return_BranchIndexModel()
        {
            var mockBranchService = new Mock<IBranchService>();
            mockBranchService.Setup(r => r.GetAll()).Returns(GetAllBranches());
            var controller = new BranchController(mockBranchService.Object);
        }

        private static IEnumerable<LibraryBranch> GetAllBranches()
        {
            return new List<LibraryBranch>()
            {
                new LibraryBranch
                {
                    Id = 123,
                    Name = "Sequoia Branch",
                    Address = "1 Main St"
                },

                new LibraryBranch
                {
                    Id = 431,
                    Name = "Lake Branch",
                    Address = "2 Oak Dr"
                },

                new LibraryBranch
                {
                    Id = 888,
                    Name = "Park Branch",
                    Address = "3 Commerce St"
                }
            };
        }

        private static LibraryBranch GetBranch()
        {
            return new LibraryBranch
            {
                Id = 888,
                Name = "Park Branch",
                Address = "3 Commerce St",
                Telephone = "123",
            };
        }
    }
}
