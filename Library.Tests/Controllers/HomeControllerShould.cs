﻿using FluentAssertions;
using Library.Controllers;
using NUnit.Framework;
using System.Web.Mvc;

namespace Library.Tests.Controllers
{
    [TestFixture]
    public class HomeControllerShould
    {
        [Test]
        public void Return_Home_Page()
        {
            var controller = new HomeController();
            var result = controller.Index();
            result.Should().BeOfType<ViewResult>();
        }
    }
}
