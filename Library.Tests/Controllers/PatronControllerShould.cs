﻿using FluentAssertions;
using Library.Controllers;
using Library.Database.Entities;
using Library.Database.Entities.Content;
using Library.Database.Entities.Content.Check;
using Library.Database.Entities.Identity;
using Library.Models.User;
using Library.Services.Interfaces;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Library.Tests.Controllers
{
    [TestFixture]
    public class PatronControllerShould
    {
        [Test]
        public void Return_Patron_Index_View()
        {
            var mockPatronService = new Mock<IPatronService>();
            mockPatronService.Setup(r => r.GetAll()).Returns(GetAllPatrons());
            var controller = new PatronController(mockPatronService.Object);

            var result = controller.Index();

            var viewResult = result.Should().BeOfType<ViewResult>();
            var viewModel = viewResult.Subject.ViewData.Model.Should().BeAssignableTo<PatronIndexModel>();
            viewModel.Subject.Patrons.Count().Should().Be(2);
        }

        [Test]
        public void Return_PatronIndexModel()
        {
            var mockPatronService = new Mock<IPatronService>();
            mockPatronService.Setup(r => r.GetAll()).Returns(GetAllPatrons());
            var controller = new PatronController(mockPatronService.Object);

            var result = controller.Index();

            var viewResult = result.Should().BeOfType<ViewResult>();
            viewResult.Subject.Model.Should().BeOfType<PatronIndexModel>();
        }

        [Test]
        public void Return_Patron_Detail_View()
        {
            var mockPatronService = new Mock<IPatronService>();
            mockPatronService.Setup(r => r.GetAsync(1)).ReturnsAsync(GetPatron());
            mockPatronService.Setup(r => r.GetCheckoutHistory(1)).Returns(new List<CheckoutHistory> { });
            mockPatronService.Setup(r => r.GetHolds(1)).Returns(new List<Hold> { });
            var sut = new PatronController(mockPatronService.Object);

            var result = sut.Detail(1);

            var viewResult = result.Should().BeOfType<ViewResult>();
            var viewModel = viewResult.Subject.ViewData.Model.Should().BeAssignableTo<PatronDetailModel>();
            viewModel.Subject.FirstName.Should().Be("Abc Def");
        }

        [Test]
        public void Return_PatronDetailModel()
        {
            var mockPatronService = new Mock<IPatronService>();
            mockPatronService.Setup(r => r.GetAsync(888)).ReturnsAsync(GetPatron());
            var controller = new PatronController(mockPatronService.Object);

            var result = controller.Detail(888);

            var viewResult = result.Should().BeOfType<ViewResult>();
            viewResult.Subject.Model.Should().BeOfType<PatronDetailModel>();
        }

        [Test]
        public void Return_Default_Values_For_Missing_Patron_Details()
        {
            var mockPatronService = new Mock<IPatronService>();
            mockPatronService.Setup(r => r.GetAsync(411)).ReturnsAsync(GetNoInfoPatron());
            var controller = new PatronController(mockPatronService.Object);

            var result = controller.Detail(411);

            var viewResult = result.Should().BeOfType<ViewResult>();
            var viewModel = viewResult.Subject.ViewData.Model.Should().BeAssignableTo<PatronDetailModel>();
            viewModel.Subject.FirstName.Should().Be("No First Name Provided");
            viewModel.Subject.LastName.Should().Be("No Last Name Provided");
            viewModel.Subject.Address.Should().Be("No Address Provided");
            viewModel.Subject.HomeLibrary.Should().Be("No Home Library");
            viewModel.Subject.Telephone.Should().Be("No Telephone Number Provided");
        }

        private static IEnumerable<User> GetAllPatrons()
        {
            return new List<User>
            {
                new User
                {
                    Id = 888,
                    FirstName = "Abc Def",
                },

                new User
                {
                    Id = 213,
                    FirstName = "Zxy Def",
                }
            };
        }

        private static User GetPatron()
        {
            return new User
            {
                Id = 888,
                FirstName = "Abc Def",
                LastName = "Last",
                LibraryCard = new LibraryCard()
                {
                    Id = -123,
                    Created = new DateTime(2018, 2, 12),
                },
                LibraryBranch = new LibraryBranch
                {
                    Id = 14,
                    Name = "Hawkins",
                }
            };
        }

        private static User GetNoInfoPatron()
        {
            return new User();
        }

        private static User GetNamelessPatron()
        {
            return new User
            {
                Id = 888,
                LibraryCard = new LibraryCard()
                {
                    Id = -123,
                    Created = new DateTime(2018, 2, 12),
                },

                LibraryBranch = new LibraryBranch
                {
                    Id = 14,
                    Name = "Hawkins",
                }
            };
        }
    }
}
