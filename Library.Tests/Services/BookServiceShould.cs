﻿using FluentAssertions;
using Library.Database.Entities.Content;
using Library.Models.Repository;
using Library.Services;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Tests.Services
{
    [TestFixture]
    public class BookServiceShould
    {
        [Test]
        public void Add_New_Book_To_Context()
        {
            var mockSet = new Mock<DbSet<Book>>();
            var mockCtx = new Mock<LibraryContext>();

            mockCtx.Setup(c => c.Books).Returns(mockSet.Object);
            var sut = new BookService(mockCtx.Object);

            sut.AddAsync(new Book());

            mockCtx.Verify(x => x.Books.Add(It.IsAny<Book>()), Times.Once());
            mockCtx.Verify(c => c.SaveChanges(), Times.Once());
        }

        [Test]
        public async void Get_Book_By_Id()
        {
            var books = new List<Book>
            {
                new Book
                {
                    Name = "The Waves",
                    Id = 1234
                },

                new Book
                {
                    Name = "The Snow Leopard",
                    Id = -6
                }
            }.AsQueryable();

            var mockSet = new Mock<DbSet<Book>>();
            mockSet.As<IQueryable<Book>>().Setup(b => b.Provider).Returns(books.Provider);
            mockSet.As<IQueryable<Book>>().Setup(b => b.Expression).Returns(books.Expression);
            mockSet.As<IQueryable<Book>>().Setup(b => b.ElementType).Returns(books.ElementType);
            mockSet.As<IQueryable<Book>>().Setup(b => b.GetEnumerator()).Returns(books.GetEnumerator);

            var mockCtx = new Mock<LibraryContext>();
            mockCtx.Setup(c => c.Books).Returns(mockSet.Object);

            var sut = new BookService(mockCtx.Object);
            var book = await sut.GetAsync(-6);

            book.Name.Should().Be("The Snow Leopard");
        }

        [Test]
        public void Get_All_Books()
        {
            var books = new List<Book>
            {
                new Book
                {
                    DeweyIndex = "ABC",
                    Id = 1234
                },

                new Book
                {
                    DeweyIndex = "SNO",
                    Id = -6
                }
            }.AsQueryable();

            var mockSet = new Mock<DbSet<Book>>();
            mockSet.As<IQueryable<Book>>().Setup(b => b.Provider).Returns(books.Provider);
            mockSet.As<IQueryable<Book>>().Setup(b => b.Expression).Returns(books.Expression);
            mockSet.As<IQueryable<Book>>().Setup(b => b.ElementType).Returns(books.ElementType);
            mockSet.As<IQueryable<Book>>().Setup(b => b.GetEnumerator()).Returns(books.GetEnumerator);

            var mockCtx = new Mock<LibraryContext>();
            mockCtx.Setup(c => c.Books).Returns(mockSet.Object);

            var sut = new BookService(mockCtx.Object);
            var queryResult = sut.GetAll().ToList();

            queryResult.Should().AllBeOfType(typeof(Book));
            queryResult.Should().HaveCount(2);
            queryResult.Should().Contain(a => a.DeweyIndex == "SNO");
            queryResult.Should().Contain(a => a.DeweyIndex == "ABC");
        }

        [Test]
        public void Get_Book_By_Author_Partial_Match()
        {
            var books = new List<Book>
            {
                new Book
                {
                    Name = "Siddhartha"
                },

                new Book
                {
                    Name = "Knulp"
                },

                new Book
                {
                    Name = "The Magic Mountain"
                }
            }.AsQueryable();

            var mockSet = new Mock<DbSet<Book>>();

            mockSet.As<IQueryable<Book>>().Setup(b => b.Provider).Returns(books.Provider);
            mockSet.As<IQueryable<Book>>().Setup(b => b.Expression).Returns(books.Expression);
            mockSet.As<IQueryable<Book>>().Setup(b => b.ElementType).Returns(books.ElementType);
            mockSet.As<IQueryable<Book>>().Setup(b => b.GetEnumerator()).Returns(books.GetEnumerator);

            var mockCtx = new Mock<LibraryContext>();
            mockCtx.Setup(c => c.Books).Returns(mockSet.Object);

            var sut = new BookService(mockCtx.Object);
            var queryResult = sut.GetByAuthor("Hesse").ToList();

            queryResult.Should().AllBeOfType(typeof(Book));
            queryResult.Should().HaveCount(2);
            queryResult.Should().Contain(b => b.Name == "Siddhartha");
            queryResult.Should().Contain(b => b.Name == "Knulp");
        }
    }
}

