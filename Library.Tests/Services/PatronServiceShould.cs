﻿using FluentAssertions;
using Library.Database.Entities.Identity;
using Library.Models.Repository;
using Library.Services;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Library.Tests.Services
{
    [TestFixture]
    public class PatronServiceShould
    {
        private static IEnumerable<User> GetPatrons()
        {
            return new List<User>
            {
                new User
                {
                    FirstName = "Jane",
                    LastName = "Doe",
                    DateOfBirth = new DateTime(1972, 01, 23)
                },

                new User
                {
                    FirstName = "Jack",
                    LastName = "Smith",
                    DateOfBirth = new DateTime(1983, 07, 3)
                }
            };
        }

        [Test]
        public void Add_New_Patron()
        {
            var mockSet = new Mock<DbSet<User>>();
            var mockCtx = new Mock<LibraryContext>();

            mockCtx.Setup(c => c.Patrons).Returns(mockSet.Object);
            var sut = new PatronService(mockCtx.Object);

            sut.Add(new User());

            mockCtx.Verify(x => x.Patrons.Add(It.IsAny<User>()), Times.Once());
            mockCtx.Verify(x => x.SaveChanges(), Times.Once());
        }

        [Test]
        public void Get_All_Patrons()
        {
            var patrons = GetPatrons().AsQueryable();

            var mockSet = new Mock<DbSet<User>>();
            mockSet.As<IQueryable<User>>().Setup(b => b.Provider).Returns(patrons.Provider);
            mockSet.As<IQueryable<User>>().Setup(b => b.Expression).Returns(patrons.Expression);
            mockSet.As<IQueryable<User>>().Setup(b => b.ElementType).Returns(patrons.ElementType);
            mockSet.As<IQueryable<User>>().Setup(b => b.GetEnumerator()).Returns(patrons.GetEnumerator);

            var mockCtx = new Mock<LibraryContext>();
            mockCtx.Setup(c => c.Patrons).Returns(mockSet.Object);

            var sut = new PatronService(mockCtx.Object);
            var queryResult = sut.GetAll().ToList();

            queryResult.Should().AllBeOfType(typeof(User));
            queryResult.Should().HaveCount(2);
            queryResult.Should().Contain(a => a.FirstName == "Jane");
            queryResult.Should().Contain(a => a.FirstName == "Jack");
        }

        [Test]
        public async void Get_Patron_By_Id()
        {
            var patrons = GetPatrons().AsQueryable();

            var mockSet = new Mock<DbSet<User>>();
            mockSet.As<IQueryable<User>>().Setup(b => b.Provider).Returns(patrons.Provider);
            mockSet.As<IQueryable<User>>().Setup(b => b.Expression).Returns(patrons.Expression);
            mockSet.As<IQueryable<User>>().Setup(b => b.ElementType).Returns(patrons.ElementType);
            mockSet.As<IQueryable<User>>().Setup(b => b.GetEnumerator()).Returns(patrons.GetEnumerator);

            var mockCtx = new Mock<LibraryContext>();
            mockCtx.Setup(c => c.Patrons).Returns(mockSet.Object);

            var sut = new PatronService(mockCtx.Object);
            var branch = await sut.GetAsync(1);

            branch.FirstName.Should().Be("Jane");
        }
    }
}
