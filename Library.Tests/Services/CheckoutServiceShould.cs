﻿using Library.Database.Entities.Content;
using Library.Models.Repository;
using Moq;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Library.Tests.Services
{
    [TestFixture]
    public class CheckoutServiceShould
    {
        private static IEnumerable<Checkout> GetCheckouts()
        {
            return new List<Checkout>
            {
                new Checkout
                {
                    Id = 1234,
                    Since = new DateTime(2018, 03, 09),
                    Until = new DateTime(2018, 04, 12),
                    LibraryCard = new LibraryCard
                    {
                        Id = -1,
                        Created = new DateTime(2008, 01, 21),
                        Fees = 123M
                    }
                },
                new Checkout
                {
                    Id = 999,
                    Since = new DateTime(2018, 03, 09),
                    Until = new DateTime(2018, 04, 12),
                    LibraryCard = new LibraryCard
                    {
                        Id = -14,
                        Created = new DateTime(2008, 01, 21),
                        Fees = 123M
                    }
                },
                new Checkout
                {
                    Id = 416,
                    Since = new DateTime(2017, 03, 09),
                    Until = new DateTime(2017, 05, 24),
                    LibraryCard = new LibraryCard
                    {
                        Id = 824,
                        Created = new DateTime(1994, 05, 16),
                        Fees = 123M
                    }
                }
            };
        }

        [Test]
        public void Add_New_Checkout_Calls_Context_Save()
        {
            var mockSet = new Mock<DbSet<Checkout>>();
            var mockCtx = new Mock<LibraryContext>();

            mockCtx.Setup(c => c.Checkouts).Returns(mockSet.Object);

            mockCtx.Verify(x => x.Checkouts.Add(It.IsAny<Checkout>()), Times.Once());
            mockCtx.Verify(x => x.SaveChanges(), Times.Once());
        }

        [Test]
        public void Get_All_Checkouts()
        {
            var cos = GetCheckouts().AsQueryable();

            var mockSet = new Mock<DbSet<Checkout>>();
            mockSet.As<IQueryable<Checkout>>().Setup(b => b.Provider).Returns(cos.Provider);
            mockSet.As<IQueryable<Checkout>>().Setup(b => b.Expression).Returns(cos.Expression);
            mockSet.As<IQueryable<Checkout>>().Setup(b => b.ElementType).Returns(cos.ElementType);
            mockSet.As<IQueryable<Checkout>>().Setup(b => b.GetEnumerator()).Returns(cos.GetEnumerator());

            var mockCtx = new Mock<LibraryContext>();
            mockCtx.Setup(c => c.Checkouts).Returns(mockSet.Object);
        }

        [Test]
        public void Get_Checkout_By_Id()
        {
            var cos = GetCheckouts().AsQueryable();

            var mockSet = new Mock<DbSet<Checkout>>();
            mockSet.As<IQueryable<Checkout>>().Setup(b => b.Provider).Returns(cos.Provider);
            mockSet.As<IQueryable<Checkout>>().Setup(b => b.Expression).Returns(cos.Expression);
            mockSet.As<IQueryable<Checkout>>().Setup(b => b.ElementType).Returns(cos.ElementType);
            mockSet.As<IQueryable<Checkout>>().Setup(b => b.GetEnumerator()).Returns(cos.GetEnumerator());

            var mockCtx = new Mock<LibraryContext>();
            mockCtx.Setup(c => c.Checkouts).Returns(mockSet.Object);
        }
    }
}
