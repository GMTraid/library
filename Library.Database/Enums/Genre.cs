﻿using System.ComponentModel.DataAnnotations;

namespace Library.Database.Enums
{
    /// <summary>
    /// Жанры лит-ры
    /// </summary>
    public enum Genre
    {
        [Display(Name = "Художественная литература")]
        Fiction = 0,

        [Display(Name = "Компьютеры")]
        Computers = 10,

        [Display(Name = "Деловая литература")]
        BusinessLterature = 20,

        [Display(Name = "Научная литература , образование")]
        ScientificLiterature = 30,

        [Display(Name = "Школьные учебники")]
        SchoolTextbook = 40,

        [Display(Name = "Детская литература")]
        ChildrensLiterature = 50,

        [Display(Name = "Дом,дача,семья")]
        Family = 60,

        [Display(Name = "Медицина")]
        Medicine = 70,

        [Display(Name = "Религия")]
        Religion = 80,

        [Display(Name = "Хобби")]
        Hobby = 90,

        [Display(Name = "Искусство")]
        Art = 100,
    }
}
