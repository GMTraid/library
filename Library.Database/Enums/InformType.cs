﻿using System.ComponentModel;

namespace Library.Database.Enums
{
    public enum InformType
    {
        /// <summary>
        /// Информация
        /// </summary>
        [Description("Информация")]
        Info = 1,

        /// <summary>
        /// Внимание
        /// </summary>
        [Description("Внимание")]
        Warning = 2,

        /// <summary>
        /// Ошибка
        /// </summary>
        [Description("Ошибка")]
        Error = 3,

        /// <summary>
        /// Успех
        /// </summary>
        [Description("Успех")]
        Success = 4
    }
}
