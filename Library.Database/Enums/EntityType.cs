﻿using System.ComponentModel;

namespace Library.Database.Enums
{
    public enum EntityType
    {
        [Description("Нет")]
        None = 0,

        [Description("Пользователь")]
        User = 80,

        [Description("Роль пользователя")]
        UserRole = 90,
    }
}
