﻿namespace Library.Database.Enums
{
    /// <summary>
    /// Статус quartz job
    /// </summary>
    public enum ScheduledJobStatus
    {
        /// <summary>
        /// Поставлена в исполнение
        /// </summary>
        Scheduled = 0,

        /// <summary>
        /// Перезапущена
        /// </summary>
        Rescheduled = 1,

        /// <summary>
        /// Выполняется
        /// </summary>
        Executing = 5,

        /// <summary>
        /// Снята с выполнения
        /// </summary>
        Unscheduled = 10,

        /// <summary>
        /// Выполнена
        /// </summary>
        Completed = 20,

        /// <summary>
        /// Ошибка при выполнении
        /// </summary>
        Failed = 30
    }
}
