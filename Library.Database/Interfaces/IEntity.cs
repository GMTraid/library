﻿using Library.Database.Interfaces.Base;

namespace Library.Database.Interfaces
{
    public interface IEntity : IEntity<long>
    {
    }
}
