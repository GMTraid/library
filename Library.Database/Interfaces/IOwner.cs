﻿namespace Library.Database.Interfaces
{
    public interface IOwner<TKey>
    {
       TKey OwnerId { get; set; }
    }
}
