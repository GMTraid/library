﻿namespace Library.Database.Interfaces.Base
{
    public interface IEntity<TKey>
    {
        TKey Id { get; set; }
    }
}
