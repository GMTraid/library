﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Database.Interfaces
{
    public interface IImportable
    {
        /// <summary>
        /// Внешний Id
        /// </summary>
        string ExternalId { get; set; }

        bool Deleted { get; set; }
    }
}
