﻿using DelegateDecompiler;
using Library.Database.Entities.Content;
using Library.Database.Entities.File;
using Library.Database.Interfaces;
using Microsoft.AspNet.Identity.EntityFramework;
using Newtonsoft.Json;
using System;

namespace Library.Database.Entities.Identity
{
    /// <summary>
    /// Patron
    /// </summary>
    public class User : IdentityUser<long, UserLogin, UserRole, UserClaim>, IEntity, IImportable
    {

        public string FirstName { get; set; }

        public string LastName { get; set; }

        public string Patronymic { get; set; }

        public string ExternalId { get; set; }

        public virtual DateTime DateOfBirth { get; set; }

        public string PlaceOfEmployment { get; set; }

        public bool Deleted { get; set; }

        public virtual UserProfilePhoto Photo { get; set; }

        public virtual LibraryCard LibraryCard { get; set; }

        public virtual LibraryBranch LibraryBranch { get; set; }

        [JsonIgnore]
        public override string PasswordHash { get; set; }

        [JsonIgnore]
        public override string SecurityStamp { get; set; }

        [Computed]
        public string FullName
        {
            get
            {
                return LastName + ((string.IsNullOrEmpty(FirstName) ? "" : (" " + FirstName.Substring(0, 1)) + "."))
                    + ((string.IsNullOrEmpty(Patronymic) ? "" : (" " + Patronymic.Substring(0, 1)) + "."));
            }
        }

        [Computed]
        public string FullFIO
        {
            get
            {
                return LastName + (string.IsNullOrEmpty(FirstName) ? "" : (" " + FirstName))
                    + (string.IsNullOrEmpty(Patronymic) ? "" : (" " + Patronymic));
            }
        }
    }
}
