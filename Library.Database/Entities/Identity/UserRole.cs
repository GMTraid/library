﻿using Library.Database.Interfaces;
using Microsoft.AspNet.Identity.EntityFramework;
using Newtonsoft.Json;
using System.ComponentModel.DataAnnotations.Schema;

namespace Library.Database.Entities.Identity
{
    public class UserRole : IdentityUserRole <long> , IEntity
    {
        [JsonIgnore]
        public virtual User User { get; set; }

        [JsonIgnore]
        public virtual Role Role { get; set; }

        [NotMapped]
        public long Id { get; set; }
    }
}
