﻿using Library.Database.Interfaces;
using Microsoft.AspNet.Identity.EntityFramework;

namespace Library.Database.Entities.Identity
{
    public class Role : IdentityRole<long, UserRole>, IEntity
    {
        public const string RootRole = "Root";
        public const string AdminRole = "Admin";
        public const string LibrarianRole = "Librarian";
        public const string ReaderRole = "Reader";

        public static Role[] RolesArray = new Role[] {
            new Role { Name = RootRole },
            new Role { Name = AdminRole },
            new Role { Name = LibrarianRole},
            new Role { Name = ReaderRole},
        };

        public bool Deleted { get; set; }
    }
}
