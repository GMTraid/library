﻿using Library.Database.Entities.Base;

namespace Library.Database.Entities.Content
{
    public class Status : Entity
    {
        public string Name { get; set; }

        public string Description { get; set; }
    }
}
