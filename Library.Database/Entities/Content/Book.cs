﻿using Library.Database.Entities.Base;
using Library.Database.Enums;
using Library.Database.Interfaces;

namespace Library.Database.Entities.Content
{
    public class Book : Entity, IImportable
    {
        public  string DeweyIndex { get; set; }

        public virtual LibraryBranch Location { get; set; }

        public bool Deleted { get; set; }

        /// <summary>
        /// Book status
        /// </summary>
        public virtual Status Status { get; set; }

        public string ImageUrl { get; set; }

        public int Year { get; set; }

        public string ExternalId { get; set; }

        /// <summary>
        /// Book name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// The number in the library
        /// </summary>
        public int Number { get; set; }

        /// <summary>
        /// Definition
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Book cover
        /// </summary>
        public virtual FileWrapper Cover { get; set; }

        /// <summary>
        /// Book author
        /// </summary>
        public virtual Author Author { get; set; }

        /// <summary>
        /// Book publisher
        /// </summary>
        public virtual Publisher Publisher { get; set; }
        /// <summary>
        /// Book genre
        /// </summary>
        public virtual Genre Genre { get; set; }
    }
}
