﻿using Library.Database.Entities.Base;
using Library.Database.Interfaces;
using System.Collections.Generic;

namespace Library.Database.Entities.Content
{
    public class Publisher : Entity, IImportable
    {
        public bool Deleted { get; set; }

        public string ExternalId { get; set; }

        /// <summary>
        /// Organization name
        /// </summary>
        public string Organization { get; set; }

        public virtual ICollection<Book> Books { get; set; }
    }
}
