﻿using Library.Database.Entities.Base;
using Library.Database.Interfaces;
using System.Collections.Generic;

namespace Library.Database.Entities.Content
{
    public class Author : Entity, IImportable
    {
        public bool Deleted { get; set; }

        public string ExternalId { get; set; }

        /// <summary>
        /// Author's name
        /// </summary>
        public string AuthorFIO { get; set; }

        public virtual ICollection<Book> Books { get; set; }
    }
}
