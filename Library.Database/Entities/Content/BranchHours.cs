﻿using Library.Database.Entities.Base;

namespace Library.Database.Entities.Content
{
    /// <summary>
    /// Working hours of the department
    /// </summary>
    public class BranchHours : Entity
    {
        /// <summary>
        /// Library Department
        /// </summary>
        public virtual LibraryBranch LibraryBranch { get; set; }

        public string DayOfWeek { get; set; }

        public string OpenTime { get; set; }

        public string CloseTime { get; set; }
    }
}
