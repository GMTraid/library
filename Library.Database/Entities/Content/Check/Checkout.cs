﻿using Library.Database.Entities.Base;
using System;

namespace Library.Database.Entities.Content
{
    public class Checkout : Entity
    {
        public virtual Book Book { get; set; }
        public virtual LibraryCard LibraryCard { get; set; }

        public DateTime Since { get; set; }

        public DateTime Until { get; set; }
    }
}
