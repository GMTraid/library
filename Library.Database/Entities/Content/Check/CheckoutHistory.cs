﻿using Library.Database.Entities.Base;
using System;

namespace Library.Database.Entities.Content.Check
{
    public class CheckoutHistory : Entity
    {
        public virtual Book Book { get; set; }

        public virtual LibraryCard LibraryCard { get; set; }

        public DateTime CheckedOut { get; set; }

        public DateTime? CheckedIn { get; set; }
    }
}
