﻿using Library.Database.Entities.Base;
using Library.Database.Entities.Identity;
using System;
using System.Collections.Generic;

namespace Library.Database.Entities.Content
{
    /// <summary>
    /// Library Department
    /// </summary>
    public class LibraryBranch : Entity
    {
        public string Name { get; set; }

        public string Address { get; set; }

        public string Telephone { get; set; }

        public string Description { get; set; }

        public DateTime OpenDate { get; set; }

        public virtual ICollection<User> Patrons { get; set; }

        public virtual ICollection<Book> Books { get; set; }

        public string ImageUrl { get; set; }
    }
}
