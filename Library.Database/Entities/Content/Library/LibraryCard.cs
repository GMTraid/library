﻿using Library.Database.Entities.Base;
using System;
using System.Collections.Generic;

namespace Library.Database.Entities.Content
{
    /// <summary>
    /// Card reader
    /// </summary>
    public class LibraryCard : Entity
    {
        public decimal Fees { get; set; }

        public DateTime Created { get; set; }

        public virtual ICollection<Checkout> Checkouts { get; set; }
    }
}
