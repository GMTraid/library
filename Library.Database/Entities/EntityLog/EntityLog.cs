﻿using Library.Database.Entities.Base;
using Library.Database.Entities.Identity;
using Library.Database.Enums;
using System;
using System.Collections.Generic;

namespace Library.Database.Entities.EntityLog
{
    /// <summary>
    /// Entity change log
    /// </summary>
    public class EntityLog : Entity
    {
        public long EntityId { get; set; }

        public string EntityTypeName { get; set; }

        public EntityType EntityTypeId { get; set; }

        public EntityLogActionType ActionType { get; set; }

        public DateTimeOffset CreateDate { get; set; }

        public string EntityDescription { get; set; }

        public virtual User User { get; set; }

        public string UserDescription { get; set; }

        public ICollection<EntityPropertyLog> EntityPropertyLogs { get; set; }
    }
}
