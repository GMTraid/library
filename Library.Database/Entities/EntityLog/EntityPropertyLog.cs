﻿using Library.Database.Entities.Base;

namespace Library.Database.Entities.EntityLog
{
    /// <summary>
    /// The log changes property of an entity
    /// </summary>
    public class EntityPropertyLog : Entity
    {
        public virtual EntityLog EntityLog { get; set; }

        public string NewValue { get; set; }

        public string OldValue { get; set; }

        public string Property { get; set; }

        public string PropertyDescription { get; set; }
    }
}
