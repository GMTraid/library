﻿using Library.Database.Entities.Base;
using Library.Database.Enums;
using System;

namespace Library.Database.Entities.Jobs
{
    public class ScheduledJob : Entity
    {
        public string JobName { get; set; }
        public string JobGroup { get; set; }

        public DateTimeOffset CreatedAt { get; set; }
        public DateTimeOffset? StartedAt { get; set; }
        public DateTimeOffset? EndedAt { get; set; }
        public DateTimeOffset StartAt { get; set; }
        public DateTimeOffset OriginStartAt { get; set; }

        public string JsonData { get; set; }
        public string DataType { get; set; }

        public string ServiceType { get; set; }
        public string ServiceMethod { get; set; }

        public string Exception { get; set; }

        public ScheduledJobStatus Status { get; set; }

        public long? BindedEntityId { get; set; }
        public EntityType? BindedEntityType { get; set; }
    }
}
