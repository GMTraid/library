﻿using Library.Database.Entities.Base;
using Library.Database.Entities.Content;
using System;

namespace Library.Database.Entities
{
    public class Hold : Entity
    {
        public virtual Book Book { get; set; }

        public virtual LibraryCard LibraryCard {get ; set;}

        public DateTime HoldPlaced { get; set; }
    }
}
