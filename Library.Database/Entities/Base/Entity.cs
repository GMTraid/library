﻿using Library.Database.Interfaces;

namespace Library.Database.Entities.Base
{
    public class Entity : IEntity
    {
        public long Id { get; set; }
    }
}
