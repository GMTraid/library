﻿using Library.Database.Entities.Base;
using Library.Database.Entities.File;
using System;
using System.Collections.Generic;

namespace Library.Database.Entities
{
    /// <summary>
    /// Higher abstraction over the file
    /// </summary>
    public class FileWrapper : Entity
    {
        public DateTimeOffset CreateDate { get; set; }

        /// <summary>
        /// The file name (without extension)
        /// </summary>
        public string FileName { get; set; }

        /// <summary>
        /// Extension (without dot)
        /// </summary>
        public string Extension { get; set; }

        /// <summary>
        /// The name of the file with the extension under which the file is stored
        /// </summary>
        public string FullNameInStore { get; set; }

        public long Size { get; set; }

        public virtual ICollection<UserProfilePhoto> UserProfilePhotos { get; set; }
    }
}
