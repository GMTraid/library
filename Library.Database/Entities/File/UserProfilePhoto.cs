﻿using Library.Database.Entities.Base;

namespace Library.Database.Entities.File
{
    public class UserProfilePhoto : Entity
    {
        public long PhotoId { get; set; }

        public virtual FileWrapper Photo { get; set; }
    }
}
