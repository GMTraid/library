﻿using System;
using System.Data.Entity;

namespace Library.Database.Domain
{
    public static class ExtendedDbFunctions
    {
        [DbFunction("CodeFirstDatabaseSchema", "sin")]
        public static double Sin(double x)
        {
            throw new NotSupportedException();
        }

        [DbFunction("CodeFirstDatabaseSchema", "cos")]
        public static double Cos(double x)
        {
            throw new NotSupportedException();
        }

        [DbFunction("CodeFirstDatabaseSchema", "tan")]
        public static double Tan(double x)
        {
            throw new NotSupportedException();
        }

        [DbFunction("CodeFirstDatabaseSchema", "cot")]
        public static double Cot(double x)
        {
            throw new NotSupportedException();
        }

        [DbFunction("CodeFirstDatabaseSchema", "acos")]
        public static double Acos(double x)
        {
            throw new NotSupportedException();
        }

        [DbFunction("CodeFirstDatabaseSchema", "atan")]
        public static double Atan(double x)
        {
            throw new NotSupportedException();
        }

        [DbFunction("CodeFirstDatabaseSchema", "atan2")]
        public static double Atan2(double x, double y)
        {
            throw new NotSupportedException();
        }


        [DbFunction("CodeFirstDatabaseSchema", "to_char")]
        public static double To_char(DateTimeOffset x, string mask)
        {
            throw new NotSupportedException();
        }

        [DbFunction("CodeFirstDatabaseSchema", "string_agg")]
        public static string String_agg(string x, string separator)
        {
            throw new NotSupportedException();
        }
    }
}
