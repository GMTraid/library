﻿using CodeFirstStoreFunctions;
using Library.Database.Entities.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Diagnostics;
using System.Reflection;

namespace Library.Database.Domain
{
    public class ApplicationDbContext : IdentityDbContext<User, Role, long, UserLogin, UserRole, UserClaim>
    {
        public ApplicationDbContext() : base("DefaultConnection")
        {
#if DEBUG
            Database.Log = sql => Debug.Write(sql);
#endif
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema("public");
            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();
            base.OnModelCreating(modelBuilder);

            modelBuilder.Configurations.AddFromAssembly(Assembly.GetExecutingAssembly());
            modelBuilder.Conventions.Add(new FunctionsConvention("", typeof(ExtendedDbFunctions)));
        }

        public static ApplicationDbContext Create()
        {
            var dbContext = new ApplicationDbContext();
            return dbContext;
        }
    }
}
