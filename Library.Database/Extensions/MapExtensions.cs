﻿using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure.Annotations;
using System.Data.Entity.ModelConfiguration.Configuration;

namespace Library.Database.Extensions
{
    public static class MapExtensions
    {
        /// <summary>
        /// Добавляет индекс
        /// </summary>
        public static PrimitivePropertyConfiguration AddIndex(this PrimitivePropertyConfiguration configuration, string name, int order = 1)
        {
            return configuration
                .HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new[] { new IndexAttribute(name) { Order = order } }));
        }

        /// <summary>
        /// Добавляет индекс
        /// </summary>
        public static PrimitivePropertyConfiguration AddIndex(this PrimitivePropertyConfiguration configuration, int order = 1)
        {
            return configuration
                .HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new[] { new IndexAttribute() { Order = order } }));
        }

        /// <summary>
        /// Добавляет ограничение на уникальность
        /// </summary>
        public static PrimitivePropertyConfiguration AddUniqueConstrain(this PrimitivePropertyConfiguration configuration, int order = 1)
        {
            return configuration
                .HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new[] { new IndexAttribute() { IsUnique = true, Order = order } }));
        }

        /// <summary>
        /// Добавляет ограничение на уникальность
        /// </summary>
        public static PrimitivePropertyConfiguration AddUniqueConstrain(this PrimitivePropertyConfiguration configuration, string indexName, int order = 1)
        {
            return configuration
                .HasColumnAnnotation(IndexAnnotation.AnnotationName, new IndexAnnotation(new[] { new IndexAttribute(indexName) { IsUnique = true, Order = order } }));
        }

        /// <summary>
        /// Добавляет ограничение на уникальность
        /// </summary>
        public static ForeignKeyAssociationMappingConfiguration AddUniqueConstrain(this ForeignKeyAssociationMappingConfiguration configuration, string columnName, int order = 1)
        {
            return configuration
                .HasColumnAnnotation(columnName, IndexAnnotation.AnnotationName, new IndexAnnotation(new[] { new IndexAttribute() { IsUnique = true, Order = order } }));
        }

        /// <summary>
        /// Добавляет ограничение на уникальность
        /// </summary>
        public static ForeignKeyAssociationMappingConfiguration AddUniqueConstrain(this ForeignKeyAssociationMappingConfiguration configuration, string columnName, string indexName, int order = 1)
        {
            return configuration
                .HasColumnAnnotation(columnName, IndexAnnotation.AnnotationName, new IndexAnnotation(new[] { new IndexAttribute(indexName) { IsUnique = true, Order = order } }));
        }

        /// <summary>
        /// Указывает в какой колонке будет храниться идентификатор связанной сущности 
        /// и добавляет этой колонке ограничение на уникальность
        /// </summary>
        public static ForeignKeyAssociationMappingConfiguration UniqueMapKey(this ForeignKeyAssociationMappingConfiguration configuration, string columnName, string indexName, int order = 1)
        {
            return configuration
                .MapKey(columnName).AddUniqueConstrain(columnName, indexName, order);
        }
    }
}
