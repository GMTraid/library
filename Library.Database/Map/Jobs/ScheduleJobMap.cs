﻿using Library.Database.Entities.Jobs;
using Library.Database.Map.Base;

namespace Library.Database.Map.Jobs
{
    public class ScheduledJobMap : EntityMap<ScheduledJob>
    {
        public ScheduledJobMap()
        {
            Property(x => x.JobGroup).HasMaxLength(64);
            Property(x => x.JobName).HasMaxLength(64);
        }
    }
}
