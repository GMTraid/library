﻿using Library.Database.Interfaces;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace Library.Database.Map.Base
{
    public class EntityMap<T> : EntityTypeConfiguration<T> where T : class, IEntity
    {
        public EntityMap()
        {
            HasKey(x => x.Id);
            SetIdentityKey();
        }

        public virtual void SetIdentityKey()
        {
            Property(x => x.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
        }
    }
}
