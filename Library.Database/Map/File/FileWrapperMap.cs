﻿using Library.Database.Entities;
using Library.Database.Extensions;
using Library.Database.Map.Base;

namespace Library.Database.Map.File
{
    public class FileWrapperMap : EntityMap<FileWrapper>
    {
        public FileWrapperMap()
        {
            Property(x => x.CreateDate).AddIndex();
            Property(x => x.FileName).HasMaxLength(260);
            Property(x => x.Extension).HasMaxLength(260);
            Property(x => x.FullNameInStore).IsRequired().HasMaxLength(260);
        }
    }
}
