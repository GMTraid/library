﻿using Library.Database.Entities.File;
using Library.Database.Extensions;
using Library.Database.Map.Base;

namespace Library.Database.Map.File
{
    public class UserProfilePhotoMap : EntityMap<UserProfilePhoto>
    {
        public UserProfilePhotoMap()
        {
            HasRequired(x => x.Photo)
                .WithMany(x => x.UserProfilePhotos)
                .HasForeignKey(x => x.PhotoId);

            Property(x => x.PhotoId).AddUniqueConstrain("IX_Photo", 1);
        }
    }
}
