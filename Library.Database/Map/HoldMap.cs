﻿using Library.Database.Entities;
using Library.Database.Map.Base;

namespace Library.Database.Map
{
    public class HoldMap : EntityMap<Hold>
    {
        public HoldMap()
        {
            HasRequired(x => x.Book);
            HasRequired(x => x.LibraryCard);
            Property(x => x.HoldPlaced).IsRequired();
        }
    }
}
