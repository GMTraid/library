﻿using Library.Database.Entities.EntityLog;
using Library.Database.Extensions;
using Library.Database.Map.Base;

namespace Library.Database.Map.EntityLog
{
    public class EntityPropertyLogMap : EntityMap<EntityPropertyLog>
    {
        public EntityPropertyLogMap()
        {
            Property(x => x.NewValue);
            Property(x => x.OldValue);
            Property(x => x.Property).IsRequired().AddIndex();
            Property(x => x.PropertyDescription).IsRequired();

            HasRequired(x => x.EntityLog);
        }
    }
}
