﻿using Library.Database.Extensions;
using Library.Database.Map.Base;

namespace Library.Database.Map.EntityLog
{
    public class EntityLogMap : EntityMap<Entities.EntityLog.EntityLog>
    {
        public EntityLogMap()
        {
            Property(x => x.ActionType).IsRequired().AddIndex();
            Property(x => x.CreateDate).IsRequired().AddIndex();
            Property(x => x.EntityId).IsRequired().AddIndex();
            Property(x => x.EntityDescription).IsRequired();
            Property(x => x.EntityTypeId).IsRequired().AddIndex();
            Property(x => x.EntityTypeName).IsRequired();
            Property(x => x.UserDescription).IsRequired();

            HasOptional(x => x.User);
        }
    }
}
