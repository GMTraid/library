﻿using Library.Database.Entities.Content;
using Library.Database.Extensions;
using Library.Database.Map.Base;

namespace Library.Database.Map.Content
{
    public class AuthorMap : EntityMap<Author>
    {
        public AuthorMap()
        {
            Property(x => x.ExternalId).HasMaxLength(50).AddUniqueConstrain();
            Property(x => x.AuthorFIO).HasMaxLength(20).IsRequired();

            HasMany(x => x.Books)
                .WithRequired(x => x.Author);
        }
    }
}
