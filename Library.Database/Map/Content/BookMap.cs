﻿using Library.Database.Entities.Content;
using Library.Database.Extensions;
using Library.Database.Map.Base;

namespace Library.Database.Map.Content
{
    public class BookMap : EntityMap<Book>
    {
        public BookMap()
        {
            Property(x => x.DeweyIndex).IsRequired().AddUniqueConstrain();
            Property(x => x.Year).IsRequired();
            Property(x => x.Number).IsRequired();
            Property(x => x.Name).HasMaxLength(30).IsRequired();
            Property(x => x.ExternalId).HasMaxLength(50).AddUniqueConstrain();
            Property(x => x.Genre).IsRequired();
            Property(x => x.ImageUrl).IsOptional();
            HasRequired(x => x.Author);
            HasRequired(x => x.Cover);
            HasRequired(x => x.Publisher);
            HasRequired(x => x.Status);
            HasRequired(x => x.Location);
        }
    }
}
