﻿using Library.Database.Entities.Content;
using Library.Database.Extensions;
using Library.Database.Map.Base;

namespace Library.Database.Map.Content.Library
{
    public class LibraryBranchMap : EntityMap<LibraryBranch>
    {
        public LibraryBranchMap()
        {
            Property(x => x.Name).IsRequired().HasMaxLength(30);
            Property(x => x.Address).IsRequired();
            Property(x => x.Telephone).IsRequired();
        }
    }
}
