﻿using Library.Database.Entities.Content;
using Library.Database.Map.Base;

namespace Library.Database.Map.Content.Library
{
    public class LibraryCardMap : EntityMap<LibraryCard>
    {
        public LibraryCardMap()
        {
            Property(x => x.Fees);
        }
    }
}
