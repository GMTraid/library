﻿using Library.Database.Entities.Content.Check;
using Library.Database.Map.Base;

namespace Library.Database.Map.Content.Check
{
    public class CheckoutHistoryMap : EntityMap<CheckoutHistory>
    {
        public CheckoutHistoryMap()
        {
            HasRequired(x => x.Book);
            HasRequired(x => x.LibraryCard);
            Property(x => x.CheckedOut).IsRequired();
        }
    }
}
