﻿using Library.Database.Entities.Content;
using Library.Database.Map.Base;

namespace Library.Database.Map.Content
{
    public class CheckoutMap : EntityMap<Checkout>
    {
        public CheckoutMap()
        {
            HasRequired(x => x.Book);
        }
    }
}
