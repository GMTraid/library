﻿using Library.Database.Entities.Content;
using Library.Database.Map.Base;

namespace Library.Database.Map.Content
{
    public class BranchHoursMap : EntityMap<BranchHours>
    {
        public BranchHoursMap()
        {
            HasRequired(x => x.LibraryBranch);
            Property(x => x.DayOfWeek).IsRequired();
            Property(x => x.OpenTime).IsRequired();
            Property(x => x.DayOfWeek).IsRequired();
        }
    }
}
