﻿using Library.Database.Entities.Content;
using Library.Database.Extensions;
using Library.Database.Map.Base;

namespace Library.Database.Map.Content
{
    public class PublisherMap : EntityMap<Publisher>
    {
        public PublisherMap()
        {
            Property(x => x.ExternalId).HasMaxLength(50).AddUniqueConstrain();
            Property(x => x.Organization).IsRequired().HasMaxLength(20);

            HasMany(x => x.Books)
                .WithRequired(x => x.Publisher);
        }
    }
}
