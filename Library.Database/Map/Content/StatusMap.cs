﻿using Library.Database.Entities.Content;
using Library.Database.Map.Base;

namespace Library.Database.Map.Content
{
    public  class StatusMap : EntityMap<Status>
    {
        public StatusMap()
        {
            Property(x => x.Description).IsRequired().HasMaxLength(100);
            Property(x => x.Name).IsRequired();
        }
    }
}
