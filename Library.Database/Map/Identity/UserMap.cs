﻿using Library.Database.Entities.Identity;
using Library.Database.Extensions;
using System.Data.Entity.ModelConfiguration;

namespace Library.Database.Map.Identity
{
    public class UserMap : EntityTypeConfiguration<User>
    {
        public UserMap()
        {
            ToTable("AspNetUsers");
            Property(x => x.UserName).IsRequired().HasMaxLength(0x100).AddUniqueConstrain("UserNameIndex", 0);
            Property(x => x.Email).HasMaxLength(0x100);
            Property(x => x.ExternalId).HasMaxLength(50).AddUniqueConstrain();
            HasRequired(x => x.LibraryBranch);
            HasRequired(x => x.LibraryCard);
        }
    }
}
