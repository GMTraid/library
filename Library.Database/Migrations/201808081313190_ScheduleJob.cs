namespace Library.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ScheduleJob : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "public.ScheduledJobs",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        JobName = c.String(maxLength: 64),
                        JobGroup = c.String(maxLength: 64),
                        CreatedAt = c.DateTimeOffset(nullable: false, precision: 7),
                        StartedAt = c.DateTimeOffset(precision: 7),
                        EndedAt = c.DateTimeOffset(precision: 7),
                        StartAt = c.DateTimeOffset(nullable: false, precision: 7),
                        OriginStartAt = c.DateTimeOffset(nullable: false, precision: 7),
                        JsonData = c.String(),
                        DataType = c.String(),
                        ServiceType = c.String(),
                        ServiceMethod = c.String(),
                        Exception = c.String(),
                        Status = c.Int(nullable: false),
                        BindedEntityId = c.Long(),
                        BindedEntityType = c.Int(),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("public.ScheduledJobs");
        }
    }
}
