namespace Library.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixedEntityBook : DbMigration
    {
        public override void Up()
        {
            DropIndex("public.Books", new[] { "LibraryBranch_Id" });
            RenameColumn(table: "public.Books", name: "LibraryBranch_Id", newName: "Location_Id");
            AlterColumn("public.Books", "Location_Id", c => c.Long(nullable: false));
            CreateIndex("public.Books", "Location_Id");
        }
        
        public override void Down()
        {
            DropIndex("public.Books", new[] { "Location_Id" });
            AlterColumn("public.Books", "Location_Id", c => c.Long());
            RenameColumn(table: "public.Books", name: "Location_Id", newName: "LibraryBranch_Id");
            CreateIndex("public.Books", "LibraryBranch_Id");
        }
    }
}
