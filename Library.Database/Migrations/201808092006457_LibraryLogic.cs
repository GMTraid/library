namespace Library.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class LibraryLogic : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("public.Books", "User_Id", "public.AspNetUsers");
            CreateTable(
                "public.LibraryBranches",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 30),
                        Address = c.String(nullable: false),
                        Telephone = c.String(nullable: false),
                        Description = c.String(),
                        OpenDate = c.DateTime(nullable: false),
                        ImageUrl = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "public.Status",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                        Description = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "public.LibraryCards",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Fees = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Created = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "public.Checkouts",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        Since = c.DateTime(nullable: false),
                        Until = c.DateTime(nullable: false),
                        Book_Id = c.Long(nullable: false),
                        LibraryCard_Id = c.Long(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("public.Books", t => t.Book_Id)
                .ForeignKey("public.LibraryCards", t => t.LibraryCard_Id)
                .Index(t => t.Book_Id)
                .Index(t => t.LibraryCard_Id);
            
            CreateTable(
                "public.Holds",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        HoldPlaced = c.DateTime(nullable: false),
                        Book_Id = c.Long(nullable: false),
                        LibraryCard_Id = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("public.Books", t => t.Book_Id)
                .ForeignKey("public.LibraryCards", t => t.LibraryCard_Id)
                .Index(t => t.Book_Id)
                .Index(t => t.LibraryCard_Id);
            
            CreateTable(
                "public.BranchHours",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        DayOfWeek = c.String(nullable: false, maxLength: 23),
                        OpenTime = c.String(nullable: false, maxLength: 23),
                        CloseTime = c.String(),
                        LibraryBranch_Id = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("public.LibraryBranches", t => t.LibraryBranch_Id)
                .Index(t => t.LibraryBranch_Id);
            
            CreateTable(
                "public.CheckoutHistories",
                c => new
                    {
                        Id = c.Long(nullable: false, identity: true),
                        CheckedOut = c.DateTime(nullable: false),
                        CheckedIn = c.DateTime(),
                        Book_Id = c.Long(nullable: false),
                        LibraryCard_Id = c.Long(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("public.Books", t => t.Book_Id)
                .ForeignKey("public.LibraryCards", t => t.LibraryCard_Id)
                .Index(t => t.Book_Id)
                .Index(t => t.LibraryCard_Id);
            
            AddColumn("public.AspNetUsers", "LibraryBranch_Id", c => c.Long(nullable: false));
            AddColumn("public.AspNetUsers", "LibraryCard_Id", c => c.Long(nullable: false));
            AddColumn("public.Books", "ImageUrl", c => c.String());
            AddColumn("public.Books", "Year", c => c.Int(nullable: false));
            AddColumn("public.Books", "Status_Id", c => c.Long(nullable: false));
            AddColumn("public.Books", "LibraryBranch_Id", c => c.Long());
            AlterColumn("public.Books", "Number", c => c.String(nullable: false, maxLength: 3));
            CreateIndex("public.AspNetUsers", "LibraryBranch_Id");
            CreateIndex("public.AspNetUsers", "LibraryCard_Id");
            CreateIndex("public.Books", "Status_Id");
            CreateIndex("public.Books", "LibraryBranch_Id");
            AddForeignKey("public.Books", "Status_Id", "public.Status", "Id");
            AddForeignKey("public.Books", "LibraryBranch_Id", "public.LibraryBranches", "Id");
            AddForeignKey("public.AspNetUsers", "LibraryBranch_Id", "public.LibraryBranches", "Id");
            AddForeignKey("public.AspNetUsers", "LibraryCard_Id", "public.LibraryCards", "Id");
            DropColumn("public.Books", "IsBooked");
            DropColumn("public.Books", "User_Id");
        }
        
        public override void Down()
        {
            AddColumn("public.Books", "User_Id", c => c.Long());
            AddColumn("public.Books", "IsBooked", c => c.Boolean(nullable: false));
            DropForeignKey("public.CheckoutHistories", "LibraryCard_Id", "public.LibraryCards");
            DropForeignKey("public.CheckoutHistories", "Book_Id", "public.Books");
            DropForeignKey("public.BranchHours", "LibraryBranch_Id", "public.LibraryBranches");
            DropForeignKey("public.Holds", "LibraryCard_Id", "public.LibraryCards");
            DropForeignKey("public.Holds", "Book_Id", "public.Books");
            DropForeignKey("public.AspNetUsers", "LibraryCard_Id", "public.LibraryCards");
            DropForeignKey("public.Checkouts", "LibraryCard_Id", "public.LibraryCards");
            DropForeignKey("public.Checkouts", "Book_Id", "public.Books");
            DropForeignKey("public.AspNetUsers", "LibraryBranch_Id", "public.LibraryBranches");
            DropForeignKey("public.Books", "LibraryBranch_Id", "public.LibraryBranches");
            DropForeignKey("public.Books", "Status_Id", "public.Status");
            DropIndex("public.CheckoutHistories", new[] { "LibraryCard_Id" });
            DropIndex("public.CheckoutHistories", new[] { "Book_Id" });
            DropIndex("public.BranchHours", new[] { "LibraryBranch_Id" });
            DropIndex("public.Holds", new[] { "LibraryCard_Id" });
            DropIndex("public.Holds", new[] { "Book_Id" });
            DropIndex("public.Checkouts", new[] { "LibraryCard_Id" });
            DropIndex("public.Checkouts", new[] { "Book_Id" });
            DropIndex("public.Books", new[] { "LibraryBranch_Id" });
            DropIndex("public.Books", new[] { "Status_Id" });
            DropIndex("public.AspNetUsers", new[] { "LibraryCard_Id" });
            DropIndex("public.AspNetUsers", new[] { "LibraryBranch_Id" });
            AlterColumn("public.Books", "Number", c => c.String(maxLength: 3));
            DropColumn("public.Books", "LibraryBranch_Id");
            DropColumn("public.Books", "Status_Id");
            DropColumn("public.Books", "Year");
            DropColumn("public.Books", "ImageUrl");
            DropColumn("public.AspNetUsers", "LibraryCard_Id");
            DropColumn("public.AspNetUsers", "LibraryBranch_Id");
            DropTable("public.CheckoutHistories");
            DropTable("public.BranchHours");
            DropTable("public.Holds");
            DropTable("public.Checkouts");
            DropTable("public.LibraryCards");
            DropTable("public.Status");
            DropTable("public.LibraryBranches");
            RenameIndex(table: "public.Books", name: "IX_Publisher_Id", newName: "IX_PublisherId");
            RenameIndex(table: "public.Books", name: "IX_Author_Id", newName: "IX_AuthorId");
            RenameColumn(table: "public.Books", name: "Publisher_Id", newName: "BookId");
            RenameColumn(table: "public.Books", name: "Author_Id", newName: "BookId");
            RenameColumn(table: "public.Books", name: "Publisher_Id", newName: "PublisherId");
            RenameColumn(table: "public.Books", name: "Author_Id", newName: "AuthorId");
            CreateIndex("public.Publishers", "BookId");
            CreateIndex("public.Authors", "BookId");
            CreateIndex("public.Books", "User_Id");
            AddForeignKey("public.Books", "User_Id", "public.AspNetUsers", "Id");
        }
    }
}
