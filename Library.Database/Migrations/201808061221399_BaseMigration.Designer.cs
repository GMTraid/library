namespace Library.Database.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.2.0-61023")]
    public sealed partial class BaseMigration : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(BaseMigration));
        
        string IMigrationMetadata.Id
        {
            get { return "201808061221399_BaseMigration"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
