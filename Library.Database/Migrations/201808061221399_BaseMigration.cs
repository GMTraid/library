namespace Library.Database.Migrations
{
    using System.Data.Entity.Migrations;

    public partial class BaseMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "public.AspNetRoles",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    Deleted = c.Boolean(nullable: false),
                    Name = c.String(nullable: false, maxLength: 256),
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");

            CreateTable(
                "public.AspNetUserRoles",
                c => new
                {
                    UserId = c.Long(nullable: false),
                    RoleId = c.Long(nullable: false),
                })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("public.AspNetRoles", t => t.RoleId)
                .ForeignKey("public.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);

            CreateTable(
                "public.AspNetUsers",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    FirstName = c.String(),
                    LastName = c.String(),
                    Patronymic = c.String(),
                    ExternalId = c.String(maxLength: 50),
                    DateOfBirth = c.DateTime(nullable: false),
                    PlaceOfEmployment = c.String(),
                    Deleted = c.Boolean(nullable: false),
                    PasswordHash = c.String(),
                    SecurityStamp = c.String(),
                    Email = c.String(maxLength: 256),
                    EmailConfirmed = c.Boolean(nullable: false),
                    PhoneNumber = c.String(),
                    PhoneNumberConfirmed = c.Boolean(nullable: false),
                    TwoFactorEnabled = c.Boolean(nullable: false),
                    LockoutEndDateUtc = c.DateTime(),
                    LockoutEnabled = c.Boolean(nullable: false),
                    AccessFailedCount = c.Int(nullable: false),
                    UserName = c.String(nullable: false, maxLength: 256),
                    Photo_Id = c.Long(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("public.UserProfilePhotoes", t => t.Photo_Id)
                .Index(t => t.ExternalId, unique: true)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex")
                .Index(t => t.Photo_Id);

            CreateTable(
                "public.Books",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    PublisherId = c.Long(nullable: false),
                    AuthorId = c.Long(nullable: false),
                    Deleted = c.Boolean(nullable: false),
                    ExternalId = c.String(maxLength: 50),
                    IsBooked = c.Boolean(nullable: false),
                    Name = c.String(nullable: false, maxLength: 30),
                    Number = c.String(maxLength: 3),
                    Description = c.String(),
                    Genre = c.Int(nullable: false),
                    Cover_Id = c.Long(nullable: false),
                    User_Id = c.Long(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("public.Authors", t => t.AuthorId)
                .ForeignKey("public.FileWrappers", t => t.Cover_Id)
                .ForeignKey("public.Publishers", t => t.PublisherId)
                .ForeignKey("public.AspNetUsers", t => t.User_Id)
                .Index(t => t.PublisherId)
                .Index(t => t.AuthorId)
                .Index(t => t.ExternalId, unique: true)
                .Index(t => t.Cover_Id)
                .Index(t => t.User_Id);

            CreateTable(
                "public.Authors",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    BookId = c.Long(nullable: false),
                    Deleted = c.Boolean(nullable: false),
                    ExternalId = c.String(maxLength: 50),
                    AuthorFIO = c.String(nullable: false, maxLength: 20),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("public.Books", t => t.BookId)
                .Index(t => t.BookId)
                .Index(t => t.ExternalId, unique: true);

            CreateTable(
                "public.FileWrappers",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    CreateDate = c.DateTimeOffset(nullable: false, precision: 7),
                    FileName = c.String(maxLength: 260),
                    Extension = c.String(maxLength: 260),
                    FullNameInStore = c.String(nullable: false, maxLength: 260),
                    Size = c.Long(nullable: false),
                })
                .PrimaryKey(t => t.Id)
                .Index(t => t.CreateDate);

            CreateTable(
                "public.UserProfilePhotoes",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    PhotoId = c.Long(nullable: false),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("public.FileWrappers", t => t.PhotoId)
                .Index(t => t.PhotoId, unique: true, name: "IX_Photo");

            CreateTable(
                "public.Publishers",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    BookId = c.Long(nullable: false),
                    Deleted = c.Boolean(nullable: false),
                    ExternalId = c.String(maxLength: 50),
                    Organization = c.String(nullable: false, maxLength: 20),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("public.Books", t => t.BookId)
                .Index(t => t.BookId)
                .Index(t => t.ExternalId, unique: true);

            CreateTable(
                "public.AspNetUserClaims",
                c => new
                {
                    Id = c.Int(nullable: false, identity: true),
                    UserId = c.Long(nullable: false),
                    ClaimType = c.String(),
                    ClaimValue = c.String(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("public.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId);

            CreateTable(
                "public.AspNetUserLogins",
                c => new
                {
                    LoginProvider = c.String(nullable: false, maxLength: 128),
                    ProviderKey = c.String(nullable: false, maxLength: 128),
                    UserId = c.Long(nullable: false),
                })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("public.AspNetUsers", t => t.UserId)
                .Index(t => t.UserId);

            CreateTable(
                "public.EntityLogs",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    EntityId = c.Long(nullable: false),
                    EntityTypeName = c.String(nullable: false),
                    EntityTypeId = c.Int(nullable: false),
                    ActionType = c.Int(nullable: false),
                    CreateDate = c.DateTimeOffset(nullable: false, precision: 7),
                    EntityDescription = c.String(nullable: false),
                    UserDescription = c.String(nullable: false),
                    User_Id = c.Long(),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("public.AspNetUsers", t => t.User_Id)
                .Index(t => t.EntityId)
                .Index(t => t.EntityTypeId)
                .Index(t => t.ActionType)
                .Index(t => t.CreateDate)
                .Index(t => t.User_Id);

            CreateTable(
                "public.EntityPropertyLogs",
                c => new
                {
                    Id = c.Long(nullable: false, identity: true),
                    NewValue = c.String(),
                    OldValue = c.String(),
                    Property = c.String(nullable: false),
                    PropertyDescription = c.String(nullable: false),
                    EntityLog_Id = c.Long(nullable: false),
                })
                .PrimaryKey(t => t.Id)
                .ForeignKey("public.EntityLogs", t => t.EntityLog_Id)
                .Index(t => t.Property)
                .Index(t => t.EntityLog_Id);

        }

        public override void Down()
        {
            DropForeignKey("public.EntityLogs", "User_Id", "public.AspNetUsers");
            DropForeignKey("public.EntityPropertyLogs", "EntityLog_Id", "public.EntityLogs");
            DropForeignKey("public.AspNetUserRoles", "UserId", "public.AspNetUsers");
            DropForeignKey("public.AspNetUsers", "Photo_Id", "public.UserProfilePhotoes");
            DropForeignKey("public.AspNetUserLogins", "UserId", "public.AspNetUsers");
            DropForeignKey("public.AspNetUserClaims", "UserId", "public.AspNetUsers");
            DropForeignKey("public.Books", "User_Id", "public.AspNetUsers");
            DropForeignKey("public.Books", "PublisherId", "public.Publishers");
            DropForeignKey("public.Publishers", "BookId", "public.Books");
            DropForeignKey("public.Books", "Cover_Id", "public.FileWrappers");
            DropForeignKey("public.UserProfilePhotoes", "PhotoId", "public.FileWrappers");
            DropForeignKey("public.Books", "AuthorId", "public.Authors");
            DropForeignKey("public.Authors", "BookId", "public.Books");
            DropForeignKey("public.AspNetUserRoles", "RoleId", "public.AspNetRoles");
            DropIndex("public.EntityPropertyLogs", new[] { "EntityLog_Id" });
            DropIndex("public.EntityPropertyLogs", new[] { "Property" });
            DropIndex("public.EntityLogs", new[] { "User_Id" });
            DropIndex("public.EntityLogs", new[] { "CreateDate" });
            DropIndex("public.EntityLogs", new[] { "ActionType" });
            DropIndex("public.EntityLogs", new[] { "EntityTypeId" });
            DropIndex("public.EntityLogs", new[] { "EntityId" });
            DropIndex("public.AspNetUserLogins", new[] { "UserId" });
            DropIndex("public.AspNetUserClaims", new[] { "UserId" });
            DropIndex("public.Publishers", new[] { "ExternalId" });
            DropIndex("public.Publishers", new[] { "BookId" });
            DropIndex("public.UserProfilePhotoes", "IX_Photo");
            DropIndex("public.FileWrappers", new[] { "CreateDate" });
            DropIndex("public.Authors", new[] { "ExternalId" });
            DropIndex("public.Authors", new[] { "BookId" });
            DropIndex("public.Books", new[] { "User_Id" });
            DropIndex("public.Books", new[] { "Cover_Id" });
            DropIndex("public.Books", new[] { "ExternalId" });
            DropIndex("public.Books", new[] { "AuthorId" });
            DropIndex("public.Books", new[] { "PublisherId" });
            DropIndex("public.AspNetUsers", new[] { "Photo_Id" });
            DropIndex("public.AspNetUsers", "UserNameIndex");
            DropIndex("public.AspNetUsers", new[] { "ExternalId" });
            DropIndex("public.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("public.AspNetUserRoles", new[] { "UserId" });
            DropIndex("public.AspNetRoles", "RoleNameIndex");
            DropTable("public.EntityPropertyLogs");
            DropTable("public.EntityLogs");
            DropTable("public.AspNetUserLogins");
            DropTable("public.AspNetUserClaims");
            DropTable("public.Publishers");
            DropTable("public.UserProfilePhotoes");
            DropTable("public.FileWrappers");
            DropTable("public.Authors");
            DropTable("public.Books");
            DropTable("public.AspNetUsers");
            DropTable("public.AspNetUserRoles");
            DropTable("public.AspNetRoles");
        }
    }
}
