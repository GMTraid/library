namespace Library.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddLibraryCard : DbMigration
    {
        public override void Up()
        {
            DropIndex("public.Books", new[] { "DeweyIndex" });
            AddColumn("public.Books", "LibraryCard_Id", c => c.Long(nullable: false));
            AlterColumn("public.Books", "DeweyIndex", c => c.String(nullable: false));
            CreateIndex("public.Books", "DeweyIndex", unique: true);
            CreateIndex("public.Books", "LibraryCard_Id");
            AddForeignKey("public.Books", "LibraryCard_Id", "public.LibraryCards", "Id");
        }
        
        public override void Down()
        {
            DropForeignKey("public.Books", "LibraryCard_Id", "public.LibraryCards");
            DropIndex("public.Books", new[] { "LibraryCard_Id" });
            DropIndex("public.Books", new[] { "DeweyIndex" });
            AlterColumn("public.Books", "DeweyIndex", c => c.Int(nullable: false));
            DropColumn("public.Books", "LibraryCard_Id");
            CreateIndex("public.Books", "DeweyIndex", unique: true);
        }
    }
}
