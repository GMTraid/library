namespace Library.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RemovedUnnecessaryConnectedness : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("public.Books", "LibraryCard_Id", "public.LibraryCards");
            DropIndex("public.Books", new[] { "LibraryCard_Id" });
            DropColumn("public.Books", "LibraryCard_Id");
        }
        
        public override void Down()
        {
            AddColumn("public.Books", "LibraryCard_Id", c => c.Long(nullable: false));
            CreateIndex("public.Books", "LibraryCard_Id");
            AddForeignKey("public.Books", "LibraryCard_Id", "public.LibraryCards", "Id");
        }
    }
}
