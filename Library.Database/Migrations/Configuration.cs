﻿namespace Library.Database.Migrations
{
    using Library.Database.Entities.Identity;
    using System.Data.Entity.Migrations;

    public sealed class Configuration : DbMigrationsConfiguration<Domain.ApplicationDbContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Domain.ApplicationDbContext context)
        {
            context.Set<Role>().AddOrUpdate(x => x.Name, Role.RolesArray);
        }
    }
}
