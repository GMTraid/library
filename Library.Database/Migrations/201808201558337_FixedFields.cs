namespace Library.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixedFields : DbMigration
    {
        public override void Up()
        {
            AlterColumn("public.Books", "Number", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("public.Books", "Number", c => c.String(nullable: false, maxLength: 3));
        }
    }
}
