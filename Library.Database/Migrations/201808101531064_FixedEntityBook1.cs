namespace Library.Database.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class FixedEntityBook1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("public.Books", "DeweyIndex", c => c.Int(nullable: false));
            CreateIndex("public.Books", "DeweyIndex", unique: true);
        }
        
        public override void Down()
        {
            DropIndex("public.Books", new[] { "DeweyIndex" });
            DropColumn("public.Books", "DeweyIndex");
        }
    }
}
