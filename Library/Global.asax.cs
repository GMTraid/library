﻿using Castle.Windsor;
using Castle.Windsor.Installer;
using Library.Database.Domain;
using Library.Extensions.Helpers;
using System;
using System.Data.Entity;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Migrations = Library.Database.Migrations;

namespace Library
{
    public class MvcApplication : System.Web.HttpApplication
    {
        public static IWindsorContainer Container;
        protected void Application_Start()
        {
            System.Data.Entity.Database.SetInitializer(new MigrateDatabaseToLatestVersion<ApplicationDbContext, Migrations.Configuration>());
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(new Action<HttpConfiguration>(WebApiConfig.Register));
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
        }

        public static void InitApplication()
        {
            InitDIContainer();
        }

        public static IWindsorContainer InitDIContainer()
        {
            if (Container == null)
            {
                Container = WindsorHelper.InitDI();
                Container.Install(FromAssembly.InThisApplication());
            }
            return Container;
        }

        protected void Application_End()
        {
            Container.Dispose();
            base.Dispose();
        }
    }
}
