﻿using Castle.MicroKernel.Registration;
using Castle.MicroKernel.SubSystems.Configuration;
using Castle.Windsor;
using Library.App_Start.IdentityConfig;
using Library.Database.Domain;
using Library.Domain.Attributes;
using Library.Domain.Config;
using Library.Domain.CrudFramework;
using Library.Domain.Logging;
using Library.Domain.Logging.Base;
using Library.Domain.Logging.Base.Interfaces;
using Library.Domain.Models;
using Library.Domain.Models.Interfaces;
using Library.Domain.Services;
using Library.Domain.Services.Interfaces;
using Library.Domain.Services.Interfaces.Domain;
using Library.Domain.Services.Interfaces.Domain.Base;
using Library.Extensions;
using Library.Extensions.Transactions;
using Library.Extensions.WindsorContainer;
using Library.Models;
using Library.Models.Repository;
using Library.Models.Repository.Interfaces;
using Library.Services;
using Library.Services.Interfaces;
using Quartz;
using System.Data.Entity;
using System.Security.Principal;
using System.Web;
using System.Web.Http;
using System.Web.Mvc;

namespace Library
{
    public class Module : IWindsorInstaller
    {
        public void Install(IWindsorContainer container, IConfigurationStore store)
        {
            RegisterCore(container);
            RegisterService(container);
            RegisterTask(container);
            IoC.Initialize(container);
        }

        private void RegisterTask(IWindsorContainer container)
        {
            container.Register(Classes.FromThisAssembly().BasedOn<IJob>().LifestyleTransient());
        }

        private static void RegisterService(IWindsorContainer container)
        {
            container.RegisterTransient<ILibraryAssetService, LibraryAssetService>();
            container.RegisterTransient<ICheckoutService, CheckoutService>();
            container.RegisterTransient<IPatronService, PatronService>();
            container.RegisterTransient<ICardService, CardService>();
            container.RegisterTransient<IBranchService, BranchService>();
            container.RegisterTransient<IBookService, BookService>();
            container.RegisterTransient<IStatusService, StatusService>();
            container.RegisterTransient<IMessageService, MessageService>();
        }

        private static void RegisterCore(IWindsorContainer container)
        {
            container.RegisterPerWebRequest<DbContext, ApplicationDbContext>();
            container.Register(Component.For<IWindsorContainer>().Instance(container));
            container.Register(Component.For<LibraryConfiguration>().Instance(LibraryConfiguration.GetConfig()));

            container.Register(Classes.FromThisAssembly().BasedOn<Controller>().LifestylePerWebRequest());
            container.Register(Classes.FromThisAssembly().BasedOn<ApiController>().LifestylePerWebRequest());

            container.RegisterSingleton<BaseEntityLogMetadata, EntityLogMetadata>();
            container.RegisterTransient<EntityLogManager, EntityLogManager>();
            container.RegisterPerWebRequest<DbContext, LogableDbContext>();
            container.RegisterTransient<IDataTransaction, LogableNestedDataTransaction>();
            container.RegisterSingleton<IApiClientManager, ApiClientManager>();

            container.Register(Classes.FromThisAssembly()
                .BasedOn<IEntityLogMap>().WithService
                .FromInterface()
                .LifestyleTransient());

            container.RegisterTransient<IDirtyCheck, DirtyCheck>();
            container.RegisterPerWebRequest<IParamsService, ParamsService>();
            container.RegisterPerWebRequest<IInformService, InformService>();
            container.RegisterTransient<IRepository, Repository>();
            container.RegisterTransient(typeof(IRepository<,>), typeof(LogableRepository<,>));
            container.RegisterTransient(typeof(IRepository<>), typeof(Repository<>));
            container.RegisterTransient(typeof(Domain.Services.Interfaces.IBindingService<>), typeof(BindingService<>));
            container.RegisterTransient(typeof(IBindingService<,>), typeof(BindingService<,>));
            container.RegisterTransient(typeof(IDomainService<,>), typeof(IDomainService<>), typeof(BaseDomainService<>));

            container.Register(Component.For<IPrincipal>().LifeStyle.PerWebRequest.UsingFactoryMethod(() => HttpContext.Current.User));

            container.Register(Component.For<ApplicationUserManager>()
                .UsingFactoryMethod(() => ApplicationUserManager.Create(null, container.Resolve<DbContext>()))
                .PerWebRequestPerScope());
        }
    }
}