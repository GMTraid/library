﻿using Library.Database.Entities.Content;
using Library.Database.Entities.Identity;
using Library.Domain.Models.Interfaces;
using Library.Extensions.Helpers;
using Library.Models.Repository;
using Library.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Library.Services
{
    public class BranchService : IBranchService
    {
        private readonly LibraryContext _context;

        public IRepository<LibraryBranch, long> RsBranches { get; set; }

        public IRepository<BranchHours, long> RsBracnhHours { get; set; }

        public BranchService(LibraryContext context)
        {
            context = _context;
        }
        public async void AddAsync(LibraryBranch newBranch)
        {
            await RsBranches.CreateOrUpdateAsync(newBranch);
            await _context.SaveChangesAsync();
        }

        public IEnumerable<LibraryBranch> GetAll()
        {
            return RsBranches.GetAll().Include(x => x.Patrons).Include(x => x.Books);
        }

        public async Task<int> GetAssetCountAsync(long branchId)
        {
            var assets = await GetAsync(branchId);

            return assets.Books.Count();
        }

        public IEnumerable<Book> GetAssets(long branchId)
        {
            return RsBranches.GetAll().Include(x => x.Books)
               .First(x => x.Id == branchId).Books;
        }

        public decimal GetAssetsValue(long id)
        {
            var assetsValue = GetAssets(id).Select(x => x.Number);
            return assetsValue.Sum();
        }

        public async Task<LibraryBranch> GetAsync(long branchId)
        {
            return await RsBranches.GetAll()
                 .Include(x => x.Patrons)
                 .Include(x => x.Books)
                 .FirstOrDefaultAsync(x => x.Id == branchId);
        }

        public IEnumerable<string> GetBranchHours(long branchId)
        {
            var hours = RsBracnhHours.GetAll().Where(x => x.Id == branchId);

            return DataHelpers.HumanizeBusinessHours(hours);
        }

        public async Task<int> GetPatronCountAsync(long branchId)
        {
            var patron = await GetAsync(branchId);

            return patron.Patrons.Count();
        }

        public async Task<IEnumerable<User>> GetPatronsAsync(long branchId)
        {
            var branches = RsBranches.GetAll();

            var patrons = await branches.Include(x => x.Patrons).FirstAsync(x => x.Id == branchId);

            return patrons.Patrons;
        }

        public bool IsBranchOpen(long branchId)
        {
            var currentTimeHour = DateTime.Now.Hour;

            var currentDayOfWeek = (int)DateTime.Now.DayOfWeek + 1;

            var hours = RsBracnhHours.GetAll().Where(x => x.LibraryBranch.Id == branchId);

            var daysHours = hours.FirstOrDefault(x => Convert.ToInt32(x.DayOfWeek) == currentDayOfWeek);

            return currentTimeHour < Convert.ToInt32(daysHours.CloseTime) && currentTimeHour > Convert.ToInt32(daysHours.OpenTime);
        }
    }
}