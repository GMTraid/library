﻿using Library.Database.Domain;
using Library.Database.Entities;
using Library.Database.Entities.Content;
using Library.Database.Entities.Content.Check;
using Library.Database.Entities.Identity;
using Library.Domain.Models.Interfaces;
using Library.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Library.Services
{
    public class CheckoutService : ICheckoutService
    {
        public IRepository<Book, long> RsBooks { get; set; }
        public IRepository<Hold, long> RsHolds { get; set; }
        public IRepository<Checkout, long> RsCheckouts { get; set; }
        public IRepository<Status, long> RsStatuses { get; set; }
        public IRepository<CheckoutHistory, long> RsCheckoutsHistory { get; set; }
        public IRepository<LibraryCard, long> RsLibraryCards { get; set; }
        public IRepository<User, long> RsUsers { get; set; }

        private ApplicationDbContext _context;
        public CheckoutService(ApplicationDbContext context)
        {
            _context = context;
        }
        public async void AddAsync(Checkout newCheckout)
        {
            await RsCheckouts.CreateOrUpdateAsync(newCheckout);
        }

        public async void CheckInTimeAsync(long assetId)
        {
            var now = DateTime.Now;

            var item = await RsBooks.GetAll()
                .FirstOrDefaultAsync(x => x.Id == assetId);

            RemoveExistingCheckouts(assetId);

            CloseExistingCheckoutHistory(assetId, now);

            var currentHolds = RsHolds.GetAll()
                .Include(x => x.Book)
                .Include(x => x.LibraryCard)
                .Where(x => x.Book.Id == assetId);

            if (currentHolds.Any())
            {
                CheckoutToEarliesHold(assetId, currentHolds);
                return;
            }

            UpdateAssetStatus(assetId, "Available");

            await _context.SaveChangesAsync();
        }

        private async void CheckoutToEarliesHold(long assetId, IQueryable<Hold> currentHolds)
        {
            var earliestHold = await currentHolds
                .OrderBy(holds => holds.HoldPlaced)
                .FirstOrDefaultAsync();

            var card = earliestHold.LibraryCard;

            RsHolds.Delete(earliestHold);

            await _context.SaveChangesAsync();

            CheckOutTimeAsync(assetId, card.Id);
        }

        public async void CheckOutTimeAsync(long assetId, long libraryCardId)
        {
            if (await IsCheckedOutAsync(assetId))
            {
                return;
            }

            var item = await RsBooks.GetAll()
                .FirstOrDefaultAsync(x => x.Id == assetId);

            UpdateAssetStatus(assetId, "Checked Out");

            var libraryCard = await RsLibraryCards.GetAll()
                .Include(x => x.Checkouts)
                .FirstOrDefaultAsync(x => x.Id == libraryCardId);

            var checkout = new Checkout
            {
                Book = item,
                LibraryCard = libraryCard,
                Since = DateTime.Now,
                Until = GetDefaultCheckoutTime(DateTime.Now)
            };

            await RsCheckouts.CreateAsync(checkout);

            var checkoutHistory = new CheckoutHistory
            {
                CheckedOut = DateTime.Now,
                Book = item,
                LibraryCard = libraryCard
            };

            await RsCheckoutsHistory.CreateAsync(checkoutHistory);

            await _context.SaveChangesAsync();
        }

        private DateTime GetDefaultCheckoutTime(DateTime now)
        {
            return now.AddDays(30);
        }

        public IEnumerable<Checkout> GetAll()
        {
            return RsCheckouts.GetAll();
        }

        public Checkout GetById(long checkoutId)
        {
            return GetAll().FirstOrDefault(x => x.Id == checkoutId);
        }

        public IEnumerable<CheckoutHistory> GetCheckoutHistory(long id)
        {
            return RsCheckoutsHistory.GetAll()
                .Include(x => x.Book)
                .Include(x => x.LibraryCard)
                .Where(x => x.Book.Id == id);
        }

        public async Task<string> GetCurrentHoldPatronNameAsync(long id)
        {
            var hold = await RsHolds.GetAll()
                .Include(x => x.Book)
                .Include(x => x.LibraryCard)
                .FirstOrDefaultAsync(x => x.Id == id);

            var cardId = hold.LibraryCard.Id;

            var patron = await RsUsers.GetAll()
                .Include(x => x.LibraryCard)
                .FirstOrDefaultAsync(x => x.LibraryCard.Id == cardId);

            return patron.FullFIO;
        }

        public async Task<string> GetCurrentHoldPlacedAsync(long id)
        {
            var hold = await RsHolds.GetAll()
                .Include(x => x.Book)
                .Include(x => x.LibraryCard)
                .FirstOrDefaultAsync(x => x.Id == id);

            return hold.HoldPlaced.ToString();
        }

        public IEnumerable<Hold> GetCurrentHolds(long id)
        {
            return RsHolds.GetAll()
                 .Include(x => x.Book)
                 .Where(x => x.Book.Id == id);
        }

        public async Task<Checkout> GetLatestCheckout(long assetId)
        {
            return await RsCheckouts.GetAll()
                .Where(x => x.Book.Id == assetId)
                .OrderByDescending(x => x.Since)
                .FirstOrDefaultAsync();
        }

        public async void MarkFound(long id)
        {
            var now = DateTime.Now;

            UpdateAssetStatus(id, "Available");

            RemoveExistingCheckouts(id);

            CloseExistingCheckoutHistory(id, now);

            await _context.SaveChangesAsync();
        }

        private async void UpdateAssetStatus(long id, string status)
        {
            var item = await RsBooks.GetAll()
               .FirstOrDefaultAsync(x => x.Id == id);

            RsBooks.Update(item);

            item.Status = await RsStatuses.GetAll()
                .FirstOrDefaultAsync(x => x.Name == status);
        }

        public async void MarkLost(long id)
        {
            UpdateAssetStatus(id, "Lost");
            await _context.SaveChangesAsync();
        }

        public async void PlaceHoldAsync(long assetId, long libraryCardId)
        {
            var asset = await RsBooks.GetAll()
                .Include(x => x.Status)
                .FirstOrDefaultAsync(x => x.Id == assetId);

            var card = await RsLibraryCards.GetAll().
                FirstOrDefaultAsync(x => x.Id == libraryCardId);

            if (asset.Status.Name == "Available")
            {
                UpdateAssetStatus(assetId, "On Hold");
            }

            var hold = new Hold
            {
                HoldPlaced = DateTime.Now,
                Book = asset,
                LibraryCard = card
            };

            await RsHolds.CreateAsync(hold);

            await _context.SaveChangesAsync();
        }

        private async void CloseExistingCheckoutHistory(long id, DateTime dateTime)
        {
            var history = await RsCheckoutsHistory.GetAll()
               .FirstOrDefaultAsync(x => x.Book.Id == id && x.CheckedIn == null);

            if (history != null)
            {
                RsCheckoutsHistory.Update(history);
                history.CheckedIn = dateTime;
            }
        }

        private async void RemoveExistingCheckouts(long id)
        {
            var checkout = await RsCheckouts.GetAll()
              .FirstOrDefaultAsync(x => x.Book.Id == id);

            if (checkout != null)
            {
                RsCheckouts.Delete(checkout);
            }
        }

        public async Task<string> GetCurrentCheckoutPatronAsync(long assetId)
        {
            var checkout = await GetCheckoutByAssetId(assetId);

            if (checkout == null)
            {
                return "Not checked out.";
            };

            var cardId = checkout.LibraryCard.Id;

            var patron = await RsUsers.GetAll()
                .Include(x => x.LibraryCard)
                .FirstOrDefaultAsync(x => x.LibraryCard.Id == assetId);

            return patron.FullFIO;
        }

        private async Task<Checkout> GetCheckoutByAssetId(long assetId)
        {
            var checkout = await RsCheckouts.GetAll()
                .Include(x => x.Book)
                .Include(x => x.LibraryCard)
                .FirstOrDefaultAsync(x => x.Book.Id == assetId);

            return checkout;
        }

        public async Task<bool> IsCheckedOutAsync(long id)
        {
            var isCheckedOut = await RsCheckouts.GetAll()
                .Where(x => x.Book.Id == id)
                .AnyAsync();

            return isCheckedOut;
        }
    }
}