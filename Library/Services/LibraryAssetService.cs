﻿using Library.Database.Domain;
using Library.Database.Entities.Content;
using Library.Domain.Models.Interfaces;
using Library.Extensions;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Library.Models.Repository
{
    public class LibraryAssetService : Interfaces.ILibraryAssetService
    {
        private ApplicationDbContext _context;
        public IRepository<Book, long> RsBooks { get; set; }

        public IRepository<LibraryBranch, long> RsLibraryBranch { get; set; }

        public LibraryAssetService(ApplicationDbContext context)
        {
            _context = context;
        }

        public async void AddAsync(Book newAsset)
        {
            await RsBooks.CreateOrUpdateAsync(newAsset);
        }

        public IEnumerable<Book> GetAll()
        {
            return RsBooks.GetAll()
                .Include(asset => asset.Status)
                .Include(asset => asset.Location)
                .Include(asset => asset.Publisher)
                .Include(asset => asset.Author)
                .Include(asset => asset.Genre);
        }

        public async Task<string> GetAuthorOrDirectorAsync(long id)
        {
            var isBook = await RsBooks.GetAll()
                .OfType<Book>()
                .Where(assets => assets.Id == id)
                .AnyAsync();

            var author = await RsBooks.GetAll().FirstOrDefaultAsync(x => x.Id == id);

            return isBook ? author.Author.AuthorFIO : "It could be a another Entity";
        }

        public async Task<Book> GetByIdAsync(long id)
        {
            return await RsBooks.GetAll()
                .Include(asset => asset.Status)
                .Include(asset => asset.Location)
                .Include(asset => asset.Publisher)
                .Include(asset => asset.Author)
                .Include(asset => asset.Genre)
                .FirstOrDefaultAsync(asset => asset.Id == id);
        }

        public async Task<LibraryBranch> GetCurrentLocationAsync(long id)
        {
            var result = await GetByIdAsync(id);
            return result.Location;
        }

        public async Task<string> GetDescriptionAsync(long id)
        {
            var isBook = await RsBooks.GetAll()
                .OfType<Book>()
                .Where(assets => assets.Id == id)
                .AnyAsync();

            var description = await RsBooks.GetAll().FirstOrDefaultAsync(x => x.Id == id);

            return isBook ? description.Description?.GetRandomString() : "";
        }

        public async Task<string> GetDeweyIndexAsync(long id)
        {
            if (await RsBooks.GetAll().AnyAsync(book => book.Id == id))
            {
                var result = await RsBooks.GetAll()
                    .FirstOrDefaultAsync(book => book.Id == id);
                return result.DeweyIndex?.GetRandomDeweyIndex();
            }

            else
            {
                var isBook = await RsBooks.GetAll().OfType<Book>()
                                .Where(x => x.Id == id)
                                .AnyAsync();

                return $"It`s a {isBook}";
            }
        }

        public async Task<string> GetTitleAsync(long id)
        {
            var result = await RsBooks.GetAll()
                .FirstOrDefaultAsync(x => x.Id == id);

            return result.Name;
        }

        public async Task<string> GetTypeAsync(long id)
        {
            var book = RsBooks.GetAll().OfType<Book>()
                .Where(x => x.Id == id);

            return await book.AnyAsync() ? "Book" : "Unknown";
        }

        public async Task<string> GetPublisherAsync(long id)
        {
            var isBook = await RsBooks.GetAll()
                .OfType<Book>()
                .Where(assets => assets.Id == id)
                .AnyAsync();

            var publisher = await RsBooks.GetAll().FirstOrDefaultAsync(x => x.Id == id);

            return isBook ? publisher.Publisher.Organization : "";
        }

        /// <summary>
        /// Not an asynchronous version of the method
        /// </summary>
        /// <param book id="id"></param>
        /// <returns></returns>
        public string GetAuthorOrDirector(long id)
        {
            var isBook = RsBooks.GetAllNoTracking()
                .OfType<Book>()
                .Where(assets => assets.Id == id)
                .Any();
            var author = RsBooks.GetAllNoTracking().FirstOrDefault(x => x.Id == id);

            return isBook ? author.Author.AuthorFIO : "It could be a another Entity";
        }

        public LibraryBranch GetCurrentLocation(long id)
        {
            var result = RsBooks.GetAll()
               .Include(asset => asset.Status)
               .Include(asset => asset.Location)
               .Include(asset => asset.Publisher)
               .Include(asset => asset.Author)
               .Include(asset => asset.Genre)
               .FirstOrDefault(asset => asset.Id == id);

            return result.Location;
        }

        public string GetDeweyIndex(long id)
        {
            if (RsBooks.GetAll().Any(book => book.Id == id))
            {
                var result = RsBooks.GetAll()
                    .FirstOrDefault(book => book.Id == id);
                return result.DeweyIndex?.GetRandomDeweyIndex();
            }

            else
            {
                var isBook = RsBooks.GetAll().OfType<Book>()
                                .Where(x => x.Id == id)
                                .Any();

                return $"It`s a {isBook}";
            }
        }
    }
}