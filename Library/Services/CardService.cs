﻿using Library.Database.Entities.Content;
using Library.Domain.Models.Interfaces;
using Library.Models.Repository;
using Library.Services.Interfaces;
using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;

namespace Library.Services
{
    public class CardService : ICardService
    {
        public IRepository<LibraryCard, long> RsCards { get; set; }

        private readonly LibraryContext _context;

        public CardService(LibraryContext context)
        {
            _context = context;
        }

        public async void AddAsync(LibraryCard newCard)
        {
            await RsCards.CreateOrUpdateAsync(newCard);
            await _context.SaveChangesAsync();
        }

        public IEnumerable<LibraryCard> GetAll()
        {
            return RsCards.GetAll();
        }

        public async Task<LibraryCard> GetAsync(long id)
        {
            return await RsCards.GetAll().FirstOrDefaultAsync(x => x.Id == id);
        }
    }
}