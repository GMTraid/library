﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Library.Database.Entities.Content;
using Library.Domain.Models.Interfaces;
using Library.Models.Repository;
using Library.Services.Interfaces;

namespace Library.Services
{
    public class BookService : IBookService
    {
        private readonly LibraryContext _context;
        public IRepository<Book, long> RsBooks { get; set; }

        public BookService(LibraryContext context)
        {
            _context = context;
        }
        public async void AddAsync(Book newBook)
        {
            await RsBooks.CreateOrUpdateAsync(newBook);
            await _context.SaveChangesAsync();
        }

        public IEnumerable<Book> GetAll()
        {
            return RsBooks.GetAll();
        }

        public async Task<Book> GetAsync(int id)
        {
            return await RsBooks.GetAll().FirstOrDefaultAsync(x => x.Id == id);
        }

        public IEnumerable<Book> GetByAuthor(string author)
        {
            return RsBooks.GetAll().Where(x => x.Author.AuthorFIO.Contains(author));
        }
    }
}