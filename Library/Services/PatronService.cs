﻿using Library.Database.Entities;
using Library.Database.Entities.Content;
using Library.Database.Entities.Content.Check;
using Library.Database.Entities.Identity;
using Library.Domain.Models.Interfaces;
using Library.Models.Repository;
using Library.Services.Interfaces;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Library.Services
{
    public class PatronService : IPatronService
    {
        private readonly LibraryContext _context;
        public IRepository<User, long> RsUsers { get; set; }

        public IRepository<CheckoutHistory, long> RsCheckoutHistories { get; set; }

        public IRepository<Checkout, long> RsCheckouts { get; set; }

        public IRepository<Hold, long> RsHolds { get; set; }

        public PatronService(LibraryContext context)
        {
            _context = context;
        }

        public async void Add(User newPatron)
        {
            await RsUsers.CreateOrUpdateAsync(newPatron);
            await _context.SaveChangesAsync();
        }

        public async Task<User> GetAsync(long id)
        {
            return await RsUsers.GetAll()
                .Include(x => x.LibraryCard)
                .Include(x => x.LibraryBranch)
                .FirstOrDefaultAsync(x => x.Id == id);
        }

        public IEnumerable<User> GetAll()
        {
            return RsUsers.GetAll()
                .Include(a => a.LibraryCard)
                .Include(a => a.LibraryBranch);
        }

        public IEnumerable<CheckoutHistory> GetCheckoutHistory(long patronId)
        {
            var cardId = RsUsers.GetAll()
                .Include(a => a.LibraryCard)
                .FirstOrDefault(a => a.Id == patronId)?
                .LibraryCard.Id;


            return RsCheckoutHistories.GetAll()
                .Include(x => x.LibraryCard)
                .Include(x => x.Book)
                .Where(x => x.LibraryCard.Id == cardId)
                .OrderByDescending(x => x.CheckedOut);
        }

        public async Task<IEnumerable<Checkout>> GetCheckoutsAsync(long id)
        {
            var patronCardId = await GetAsync(id);
            return RsCheckouts.GetAll()
                .Include(a => a.LibraryCard)
                .Include(a => a.Book)
                .Where(v => v.LibraryCard.Id == patronCardId.LibraryCard.Id);
        }

        public IEnumerable<Hold> GetHolds(long patronId)
        {
            var cardId = RsUsers.GetAll()
                .Include(a => a.LibraryCard)
                .FirstOrDefault(a => a.Id == patronId)?
                .LibraryCard.Id;

            return RsHolds.GetAll()
                .Include(a => a.LibraryCard)
                .Include(a => a.Book)
                .Where(a => a.LibraryCard.Id == cardId)
                .OrderByDescending(a => a.HoldPlaced);
        }

        public IEnumerable<CheckoutHistory> GetCheckoutHistory(int patronId)
        {
            var cardId = RsUsers.GetAll()
                .Include(a => a.LibraryCard)
                .FirstOrDefault(a => a.Id == patronId)?
                .LibraryCard.Id;

            return RsCheckoutHistories.GetAll()
                .Include(a => a.LibraryCard)
                .Include(a => a.Book)
                .Where(a => a.LibraryCard.Id == cardId)
                .OrderByDescending(x => x.CheckedOut);
        }
    }
}