﻿using Library.Database.Entities.Content;
using Library.Database.Entities.Identity;
using Library.Domain.Models.Interfaces;
using Library.Domain.SMTPClient;
using Library.Domain.SMTPClient.Helpers;
using Library.Domain.SMTPClient.Proxy;
using Library.Services.Interfaces;
using System;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Library.Services
{
    public class MessageService : IMessageService
    {
        public IRepository<LibraryBranch, long> RsLibraryBranchs { get; set; }
        public IRepository<Checkout, long> RsCheckouts { get; set; }
        public IRepository<Book, long> RsBooks { get; set; }
        public IRepository<User, long> RsUsers { get; set; }

        public event Action<string, Exception> OnLog;
        private async Task<bool> IsAvailableForSending(long bookId)
        {
            var asset = await RsBooks.GetAll()
                .Include(x => x.Status)
                .FirstOrDefaultAsync(x => x.Id == bookId);

            return asset.Status.Name == "Available" ? true : false;
        }

        private async Task<Checkout> GetCheckoutByAssetId(long assetId)
        {
            var checkout = await RsCheckouts.GetAll()
                .Include(x => x.Book)
                .Include(x => x.LibraryCard)
                .FirstOrDefaultAsync(x => x.Book.Id == assetId);

            return checkout;
        }

        public async Task<(int SuccessCount, int ErrorCount)> SendMessageAsync()
        {
            var books = RsBooks.GetAll().Where(x => x.Status.Name == "Available").FirstOrDefault();

            var checkout = await GetCheckoutByAssetId(books.Id);

            var successCount = 0;
            var errorCount = 0;

            if (checkout == null)
            {
                errorCount += 1;
                return (successCount, errorCount);
            };

            var cardId = checkout.LibraryCard.Id;

            var patron = await RsUsers.GetAll()
                .Include(x => x.LibraryCard)
                .FirstOrDefaultAsync(x => x.LibraryCard.Id == books.Id);

            var branch = RsLibraryBranchs.GetAll().Where(x => x.Name == "Lake Shore Branch");

            if (await IsAvailableForSending(patron.Id) == true)
            {
                var smtp = new SmtpConnection
                {
                    Proxy = new HttpProxy("91.205.239.120", 8080)
                };

                smtp.Connect("smtp.gmail.com", 587);
                smtp.ExtendedHello("");
                smtp.StartTls("gmail.com");

                smtp.ExtendedHello("mail.ru");
                smtp.AuthPlain("", "");

                smtp.Mail("@gmail.com");

                var text = EmailFormatter.GetText($"Library {branch.FirstOrDefault()}", "Notification", $"{patron.FullFIO} <{patron.Email}>", null , "Your book is available , you can take it on our website!");
                byte[] bytes = Encoding.UTF8.GetBytes(text);
                smtp.Recipient($"<{patron.Email}>");
                try
                {
                    smtp.Data(bytes);
                    successCount += 1;
                }
                catch
                {
                    errorCount += 1;
                }
                smtp.Quit();
            }
            else errorCount += 1;
            return (successCount, errorCount);
        }
    }
}