﻿using System.Collections.Generic;
using System.Data.Entity;
using System.Threading.Tasks;
using Library.Database.Entities.Content;
using Library.Domain.Models.Interfaces;
using Library.Models.Repository;
using Library.Services.Interfaces;

namespace Library.Services
{
    public class StatusService : IStatusService
    {
        private readonly LibraryContext _context;
        public IRepository<Status, long> RsStatus { get; set; }
        public StatusService(LibraryContext context)
        {
            _context = context;
        }
        public async void AddAsync(Status newStatus)
        {
            await RsStatus.CreateOrUpdateAsync(newStatus);
            await _context.SaveChangesAsync();
        }

        public IEnumerable<Status> GetAll()
        {
            return RsStatus.GetAll();
        }

        public async Task<Status> GetAsync(long id)
        {
            return await RsStatus.GetAll().FirstOrDefaultAsync(x => x.Id == id);
        }
    }
}