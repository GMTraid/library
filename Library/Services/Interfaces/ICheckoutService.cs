﻿using Library.Database.Entities;
using Library.Database.Entities.Content;
using Library.Database.Entities.Content.Check;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Library.Services.Interfaces
{
    public interface ICheckoutService
    {
        IEnumerable<Checkout> GetAll();
        Checkout GetById(long checkoutId);
        void AddAsync(Checkout newCheckout);
        void CheckOutTimeAsync(long assetId, long libraryCardId);
        void CheckInTimeAsync(long assetId);
        IEnumerable<CheckoutHistory> GetCheckoutHistory(long id);
        Task<Checkout> GetLatestCheckout(long assetId);
        void PlaceHoldAsync(long assetId, long libraryCardId);
        Task<string> GetCurrentHoldPatronNameAsync(long id);
        Task<string> GetCurrentHoldPlacedAsync(long id);
        Task<string> GetCurrentCheckoutPatronAsync(long assetId);
        IEnumerable<Hold> GetCurrentHolds(long id);
        void MarkLost(long id);
        void MarkFound(long id);
        Task<bool> IsCheckedOutAsync(long id);
    }
}
