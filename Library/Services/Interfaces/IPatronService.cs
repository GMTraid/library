﻿using Library.Database.Entities;
using Library.Database.Entities.Content;
using Library.Database.Entities.Content.Check;
using Library.Database.Entities.Identity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Library.Services.Interfaces
{
    public interface IPatronService
    {
        IEnumerable<User> GetAll();
        Task<User> GetAsync(long id);
        void Add(User newBook);
        IEnumerable<CheckoutHistory> GetCheckoutHistory(long patronId);
        IEnumerable<Hold> GetHolds(long patronId);
        Task<IEnumerable<Checkout>> GetCheckoutsAsync(long id);
    }
}
