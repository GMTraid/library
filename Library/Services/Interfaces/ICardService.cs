﻿using Library.Database.Entities.Content;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Library.Services.Interfaces
{
    public interface ICardService
    {
        IEnumerable<LibraryCard> GetAll();
        Task<LibraryCard> GetAsync(long id);
        void AddAsync(LibraryCard newCard);
    }
}
