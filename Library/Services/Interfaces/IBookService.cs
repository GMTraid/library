﻿using Library.Database.Entities.Content;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Library.Services.Interfaces
{
    public interface IBookService
    {
        IEnumerable<Book> GetAll();
        IEnumerable<Book> GetByAuthor(string author);
        Task<Book> GetAsync(int id);
        void AddAsync(Book newBook);
    }
}
