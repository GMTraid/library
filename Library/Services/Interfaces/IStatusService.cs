﻿using Library.Database.Entities.Content;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Library.Services.Interfaces
{
    public interface IStatusService
    {
        IEnumerable<Status> GetAll();
        Task<Status> GetAsync(long id);
        void AddAsync(Status newStatus);
    }
}
