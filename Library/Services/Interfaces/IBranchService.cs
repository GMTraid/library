﻿using Library.Database.Entities.Content;
using Library.Database.Entities.Identity;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Library.Services.Interfaces
{
    public interface IBranchService
    {
        IEnumerable<LibraryBranch> GetAll();
        Task<IEnumerable<User>> GetPatronsAsync(long branchId);
        IEnumerable<Book> GetAssets(long branchId);
        Task<LibraryBranch> GetAsync(long branchId);
        void AddAsync(LibraryBranch newBranch);
        IEnumerable<string> GetBranchHours(long branchId);
        bool IsBranchOpen(long branchId);
        Task<int> GetAssetCountAsync(long branchId);
        Task<int> GetPatronCountAsync(long branchId);
        decimal GetAssetsValue(long id);
    }
}
