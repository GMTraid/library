﻿using System;
using System.Threading.Tasks;

namespace Library.Services.Interfaces
{
    public interface IMessageService
    {
        event Action<string, Exception> OnLog;
        Task<(int SuccessCount, int ErrorCount)> SendMessageAsync();
    }
}
