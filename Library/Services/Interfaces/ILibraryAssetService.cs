﻿using Library.Database.Entities.Content;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Library.Models.Repository.Interfaces
{
    public interface ILibraryAssetService
    {
        IEnumerable<Book> GetAll();
        Task<Book> GetByIdAsync(long id);
        void AddAsync(Book newAsset);
        Task<string> GetAuthorOrDirectorAsync(long id);
        string GetAuthorOrDirector(long id);
        LibraryBranch GetCurrentLocation(long id);
        string GetDeweyIndex(long id);
        Task<string> GetDeweyIndexAsync(long id);
        Task<string> GetTypeAsync(long id);
        Task<string> GetTitleAsync(long id);
        Task<LibraryBranch> GetCurrentLocationAsync(long id);
        Task<string> GetDescriptionAsync(long id);
        Task<string> GetPublisherAsync(long id);
    }
}
