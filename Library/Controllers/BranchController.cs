﻿using Library.Database.Entities.Identity;
using Library.Domain.Attributes;
using Library.Models.Branch;
using Library.Services.Interfaces;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Library.Controllers
{
    [AuthorizeRoles(Role.AdminRole, Role.LibrarianRole, Role.ReaderRole)]
    public class BranchController : Controller
    {
        public readonly IBranchService _branch;

        public async Task<ActionResult> Index()
        {
            var branchModels = await Task.WhenAll(_branch.GetAll()
                .Select(async x => new BranchDetailModel
                {
                    Id = x.Id,
                    BranchName = x.Name,
                    NumberOfAssets = await _branch.GetAssetCountAsync(x.Id),
                    NumberOfPatrons = await _branch.GetPatronCountAsync(x.Id),
                    IsOpen = _branch.IsBranchOpen(x.Id)
                }).ToList());

            var model = new BranchIndexModel
            {
                Branches = branchModels
            };

            return View(model);
        }

        public async Task<ActionResult> Detail(long id)
        {
            var branch = await _branch.GetAsync(id);
            var model = new BranchDetailModel
            {
                BranchName = branch.Name,
                Description = branch.Description,
                Address = branch.Address,
                Telephone = branch.Telephone,
                BranchOpenedDate = branch.OpenDate.ToString("yyyy-MM-dd"),
                NumberOfPatrons = await _branch.GetPatronCountAsync(id),
                NumberOfAssets = await _branch.GetAssetCountAsync(id),
                TotalAssetValue = _branch.GetAssetsValue(id),
                ImageUrl = branch.ImageUrl,
                HoursOpen = _branch.GetBranchHours(id)
            };

            return View(model);
        }
    }
}