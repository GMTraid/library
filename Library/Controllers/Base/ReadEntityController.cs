﻿using DelegateDecompiler.EntityFramework;
using Library.Database.Interfaces;
using Library.Domain.Models;
using Library.Domain.Params;
using Library.Domain.Services.Interfaces.Domain;
using Library.Extensions;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;

namespace Library.Controllers.Base
{
    public class ReadEntityController<T> : BaseController where T : class, IEntity
    {
        public IDomainService<T> DomainService { get; set; }

        public virtual async Task<IHttpActionResult> List()
        {
            var loadParams = await GetParamsAsync<DataTableLoadParams>();
            var qData = DomainService.GetAll();

            var result = ListSelector(qData, loadParams);
            return Ok(result);
        }

        public virtual IHttpActionResult ListByModelType()
        {
            return ListByModel<T>();
        }

        protected IHttpActionResult ListByModel<TModel>()
        {
            var loadParams = GetParams<DataTableLoadParams>();
            var qData = DomainService.GetAll();

            var result = ListSelector<TModel>(qData, loadParams);
            return Ok(result);
        }

        public virtual IHttpActionResult ListForOwner()
        {
            return ListForOwner(null);
        }

        protected IHttpActionResult ListForOwner(string[] showAllForRoles)
        {
            var loadParams = GetParams<DataTableLoadParams>();
            long userId = GetUserId();
            IQueryable<T> qData;
            if (showAllForRoles != null && showAllForRoles.Any())
            {
                if (showAllForRoles.Any(User.IsInRole))
                {
                    qData = DomainService.GetAll();
                }
                else
                {
                    qData = DomainService.GetAllForOwner(userId);
                }
            }
            else
            {
                qData = DomainService.GetAllForOwner(userId);
            }

            var result = ListSelector(qData, loadParams);
            return Ok(result);
        }

        protected virtual ListResult ListSelector(IQueryable<T> query, DataTableLoadParams loadParams)
        {
            return ListSelector<T>(query, loadParams);
        }

        protected virtual ListResult ListSelector<TModel>(IQueryable<T> query, DataTableLoadParams loadParams)
        {
            return query.GetListResult<T, TModel>(loadParams);
        }

        protected async Task<TModel> GetModel<TModel>(GetParams model)
        {
            return await DomainService.GetAll().Where(x => x.Id == model.Id).ProjectToIf<T, TModel>().DecompileAsync().FirstOrDefaultAsync();
        }

        [HttpPost]
        [HttpGet]
        public virtual async Task<IHttpActionResult> Get(GetParams model)
        {
            var data = await GetModel<T>(model);
            return Ok(Success(data));
        }
    }
}