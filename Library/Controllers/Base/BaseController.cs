﻿using Library.Domain.Models;
using Library.Domain.Params;
using Library.Extensions;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Net.Http;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace Library.Controllers.Base
{
    public abstract class BaseController : ApiController
    {
        private Regex ArrayRegex = new Regex("[A-Za-z]+\\[\\d+\\]");
        private Regex ArrayIndexRegex = new Regex("\\[\\d+\\]");

        public override async Task<HttpResponseMessage> ExecuteAsync(HttpControllerContext controllerContext, CancellationToken cancellationToken)
        {
            await controllerContext.Request.Content.LoadIntoBufferAsync();
            return await base.ExecuteAsync(controllerContext, cancellationToken);
        }

        protected TModel GetParams<TModel>() where TModel : new()
        {
            return GetParamsAsync<TModel>().GetAwaiter().GetResult();
        }

        protected async Task<TModel> GetParamsAsync<TModel>() where TModel : new()
        {
            if (Request.Content.IsMimeMultipartContent())
            {
                var result = new TModel();
                object currentModel = result;
                Type currentModelType = result.GetType();
                var provider = await Request.Content.ReadAsMultipartAsync();
                var contents = provider.Contents
                    .Select(x => ConvertHttpContentToMultipartItem(x, "", ""))
                    .GroupBy(x => $"{x.MainProperty}{x.ArrayKey}")
                    .ToList();
                await ParseFormData(result, result.GetType(), contents, "");

                return result;
            }
            else
            {
                return await Request.Content.ReadAsAsync<TModel>(new[] { new ValidationJsonFormatter(ModelState) });
            }
        }

        private async Task ParseFormData(object parentModel, Type parentModelType, List<IGrouping<string, MultipartItem>> contents, string previousContentKey)
        {
            var currentModelType = parentModelType;
            var currentModel = parentModel;
            foreach (var content in contents)
            {
                if (content.Key.HasValue())
                {
                    var property = parentModelType.GetProperty(content.First().MainProperty);
                    var propertyType = property.PropertyType;

                    var genericPropertyType = propertyType.GetTypeInfo().GetGenericArguments()[0];
                    var genericPropertyValue = Activator.CreateInstance(genericPropertyType);

                    var isListType = IsListType(property);
                    var valueType = GetValueType(property, isListType);

                    if (isListType)
                    {
                        var list = property.GetValue(parentModel);
                        if (list == null)
                        {
                            list = Activator.CreateInstance(propertyType);
                            property.SetValue(parentModel, list);
                        }

                        var addItemMethodInfo = propertyType.GetMethod(nameof(List<object>.Add));
                        addItemMethodInfo.Invoke(list, new[] { genericPropertyValue });

                        currentModel = genericPropertyValue;
                        currentModelType = genericPropertyType;
                    }
                }

                if (content.Key.HasValue())
                {
                    var nestedContents = content
                        .Select(x => ConvertHttpContentToMultipartItem(x.Content, content.Key, previousContentKey))
                        .GroupBy(x => $"{x.MainProperty}{x.ArrayKey}")
                        .ToList();
                    await ParseFormData(currentModel, currentModelType, nestedContents, $"{previousContentKey}{content.Key}");
                }

                foreach (var item in content)
                {
                    var field = item.Content;
                    var name = field.Headers.ContentDisposition.Name.Trim('\"');
                    name = previousContentKey.HasValue() ? name.Replace(previousContentKey, "") : name;
                    name = content.Key.HasValue() ? name.Replace(content.Key, "") : name;
                    var propertyNames = name
                        .Split(new[] { "." }, StringSplitOptions.RemoveEmptyEntries);

                    object lastInstance = currentModel;
                    object lastPropertyValue = null;
                    PropertyInfo lastProperty = null;

                    if (!name.HasValue() && item.PropertyName.HasValue())
                    {
                        if (field.Headers.ContentDisposition.FileName != null)
                        {
                            var filename = field.Headers.ContentDisposition.FileName.Trim('\"');
                            var buffer = await field.ReadAsByteArrayAsync();
                            ((FileInfoParams)lastInstance).FileName = filename;
                            ((FileInfoParams)lastInstance).Buffer = buffer;
                            ((FileInfoParams)lastInstance).Size = buffer.LongLength;
                        }
                        else
                        {
                            var value = await field.ReadAsStringAsync();
                            lastInstance = value;
                        }
                    }

                    foreach (var propertyName in propertyNames)
                    {
                        if (lastPropertyValue != null)
                        {
                            lastInstance = lastPropertyValue;
                        }
                        var property = currentModelType.GetProperty(propertyName);
                        if (property == null)
                        {
                            break;
                        }
                        var propertyType = property.PropertyType;
                        var propertyValue = property.GetValue(lastInstance);
                        if (propertyType.IsClass && propertyType != typeof(string))
                        {
                            if (propertyValue == null)
                            {
                                propertyValue = Activator.CreateInstance(propertyType);
                                property.SetValue(lastInstance, propertyValue);
                            }
                        }
                        lastPropertyValue = propertyValue;
                        lastProperty = property;
                    }

                    if (lastProperty != null)
                    {
                        if (field.Headers.ContentDisposition.FileName != null)
                        {
                            var filename = field.Headers.ContentDisposition.FileName.Trim('\"');
                            var buffer = await field.ReadAsByteArrayAsync();
                            var value = new FileInfoParams
                            {
                                FileName = filename,
                                Buffer = buffer,
                                Size = buffer.LongLength
                            };

                            SetPropertyValueForMime(lastInstance, lastProperty, value);
                        }
                        else
                        {
                            var value = await field.ReadAsStringAsync();
                            SetPropertyValueForMime(lastInstance, lastProperty, value);
                        }
                    }
                }
            }
        }

        private MultipartItem ConvertHttpContentToMultipartItem(HttpContent contentItem, string contentKey, string previousContentKey)
        {
            var matchingValue = contentItem.Headers.ContentDisposition.Name.Trim('\"');
            if (contentKey.HasValue())
            {
                matchingValue = matchingValue.Replace(contentKey, "");
            }
            if (previousContentKey.HasValue())
            {
                matchingValue = matchingValue.Replace(previousContentKey, "");
            }
            var match = ArrayRegex.Match(matchingValue);
            var multipartItem = new MultipartItem { Content = contentItem, ArrayKey = "", MainProperty = "" };
            if (match.Success)
            {
                var arrayKey = ArrayIndexRegex.Match(match.Value).Value;
                multipartItem.ArrayKey = arrayKey;
                multipartItem.MainProperty = match.Value.Replace(arrayKey, "");
                multipartItem.PropertyName = matchingValue;
            }
            return multipartItem;
        }

        private void SetPropertyValueForMime(object lastInstance, PropertyInfo lastProperty, object value)
        {
            if (lastProperty.PropertyType == typeof(FileInfoParams))
            {
                lastProperty.SetValue(lastInstance, value);
            }
            else
            {
                var isListType = IsListType(lastProperty);
                var valueType = GetValueType(lastProperty, isListType);
                var convertedValue = GetConvertedValue(value, valueType);

                if (isListType)
                {
                    var list = lastProperty.GetValue(lastInstance);
                    if (list == null)
                    {
                        list = Activator.CreateInstance(lastProperty.PropertyType);
                        lastProperty.SetValue(lastInstance, list);
                    }
                    var addItemMethodInfo = list.GetType().GetMethod(nameof(List<object>.Add));
                    addItemMethodInfo.Invoke(list, new[] { convertedValue });
                }
                else
                {
                    lastProperty.SetValue(lastInstance, convertedValue);
                }
            }
        }

        private object GetConvertedValue(object value, Type valueType)
        {
            if (value is string stringValue)
            {
                var converter = TypeDescriptor.GetConverter(valueType);
                return converter.ConvertFromInvariantString(stringValue);
            }
            else if (value is FileInfoParams fileInfoParams)
            {
                return fileInfoParams;
            }
            else
            {
                throw new Exception($"No type conversion { value.GetType().Name} in type {valueType.Name}");
            }
        }

        private bool IsListType(PropertyInfo lastProperty)
        {
            var propertyType = lastProperty.PropertyType;
            if (propertyType.IsGenericType)
            {
                var isList = propertyType.GetGenericTypeDefinition().IsAssignableFrom(typeof(List<>));
                return isList;
            }
            return false;
        }

        private Type GetValueType(PropertyInfo lastProperty, bool isListType)
        {
            var propertyType = lastProperty.PropertyType;
            if (isListType)
            {
                return propertyType.GetGenericArguments()[0];
            }
            return lastProperty.PropertyType;
        }

        protected async Task<BaseParams> GetParamsAsync()
        {
            var allParams = new Dictionary<string, object>();
            var allFiles = new Dictionary<string, FileInfoParams>();

            if (Request.Content.IsMimeMultipartContent())
            {
                var provider = await Request.Content.ReadAsMultipartAsync();
                foreach (var field in provider.Contents)
                {
                    var name = field.Headers.ContentDisposition.Name;
                    if (field.Headers.ContentDisposition.FileName != null)
                    {
                        var filename = field.Headers.ContentDisposition.FileName.Trim('\"');
                        var buffer = await field.ReadAsByteArrayAsync();
                        allFiles[name] = new FileInfoParams
                        {
                            FileName = filename,
                            Buffer = buffer,
                            Size = buffer.LongLength
                        };
                    }
                    else
                    {
                        var value = await field.ReadAsStringAsync();
                        allParams[name] = value;
                    }
                }
            }
            return new BaseParams { Params = allParams, Files = allFiles };
        }

        protected long GetUserId()
        {
            return long.Parse(User.Identity.GetUserId());
        }

        protected Result Success(object data)
        {
            return new Result { Success = true, Data = data };
        }

        protected Result Success(string message)
        {
            return new Result { Success = true, Message = message };
        }

        protected Result Success()
        {
            return new Result { Success = true };
        }
    }
}