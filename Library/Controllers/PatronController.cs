﻿using Library.Database.Entities.Identity;
using Library.Domain.Attributes;
using Library.Models.User;
using Library.Services.Interfaces;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Library.Controllers
{
    [AuthorizeRoles(Role.AdminRole, Role.LibrarianRole, Role.ReaderRole)]
    public class PatronController : Controller
    {
        public IPatronService _patron;
        public ActionResult Index()
        {
            var allPatrons = _patron.GetAll();

            var patronModel = allPatrons.Select(x => new PatronDetailModel
            {
                Id = x.Id,
                LastName = x.LastName ?? "No First Name Provided",
                FirstName = x.FirstName ?? "No Last Name Provided",
                LibraryCardId = x.LibraryCard?.Id,
                HomeLibrary = x.LibraryBranch?.Name
            });

            var model = new PatronIndexModel
            {
                Patrons = patronModel
            };

            return View(model);
        }

        public async Task<ActionResult> Detail(int id)
        {
            var patron = await _patron.GetAsync(id);

            var assets = await _patron.GetCheckoutsAsync(id);

            var model = new PatronDetailModel
            {
                Id = patron.Id,
                LastName = patron.LastName ?? "No Last Name Provided",
                FirstName = patron.FirstName ?? "No First Name Provided",
                HomeLibrary = patron.LibraryBranch?.Name ?? "No Home Library",
                MemberSince = patron.LibraryCard?.Created,
                LibraryCardId = patron.LibraryCard?.Id,
                Telephone = string.IsNullOrEmpty(patron.PhoneNumber) ? "No Telephone Number Provided" : patron.PhoneNumber,
                AssetsCheckedOut = assets.ToList(),
                CheckoutHistory = _patron.GetCheckoutHistory(id),
                Holds = _patron.GetHolds(id)
            };
            return View(model);
        }
    }
}