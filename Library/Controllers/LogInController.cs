﻿using Library.App_Start;
using Library.App_Start.IdentityConfig;
using Library.Database.Entities.Identity;
using Library.Domain.Config;
using Library.Domain.Services.Interfaces.Domain;
using Library.Models.Identity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Library.Controllers
{
    public class LogInController : Controller
    {
        [Authorize]
        [SkipAssemblyCheck]
        public class AccountController : Controller
        {
            private ApplicationSignInManager _signInManager;
            private ApplicationUserManager _userManager;

            public IDomainService<User> DsApplicationUser { get; set; }
            public LibraryConfiguration LibraryConfiguration { get; set; }

            protected const string _rootLogin = "root";

            protected string RootPassword => LibraryConfiguration.Security.RootPassword;

            public AccountController()
            {
            }

            public AccountController(ApplicationUserManager userManager, ApplicationSignInManager signInManager)
            {
                UserManager = userManager;
                SignInManager = signInManager;
            }

            public ApplicationSignInManager SignInManager
            {
                get
                {
                    return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
                }
                private set
                {
                    _signInManager = value;
                }
            }

            public ApplicationUserManager UserManager
            {
                get
                {
                    return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
                }
                private set
                {
                    _userManager = value;
                }
            }

            [HttpGet]
            [AllowAnonymous]
            public async Task<ActionResult> Authorize()
            {
                var claims = new ClaimsPrincipal(User).Claims.ToArray();
                var identity = new ClaimsIdentity(claims, "Bearer");
                AuthenticationManager.SignIn(identity);

                var userName = identity.GetUserName();

                if (string.IsNullOrEmpty(userName))
                {
                    return Json(new
                    {
                        success = false,
                        message = ""
                    }, JsonRequestBehavior.AllowGet);
                }

                var user = await UserManager.FindByNameAsync(userName);
                var roles = await UserManager.GetRolesAsync(user.Id);

                return Json(new
                {
                    success = true,
                    message = "",
                    roles = roles,
                    userName = $"{user.LastName} {user.FirstName}"
                }, JsonRequestBehavior.AllowGet);
            }

            [AllowAnonymous]
            public ActionResult Login(string returnUrl)
            {
                ViewBag.ReturnUrl = returnUrl;
                return View();
            }

            [HttpPost]
            [AllowAnonymous]
            public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
            {
                if (model.Login == _rootLogin)
                {
                    var identityResult = await ConfigurationRootUserAsync(model, _rootLogin);
                    if (!identityResult.Succeeded)
                    {
                        ModelState.AddModelError("", "The login attempt failed." + string.Join("; ", identityResult.Errors));
                    }
                }

                if (ModelState.IsValid)
                {
                    var result = await SignInManager.PasswordSignInAsync(model.Login, model.Password, model.RememberMe, shouldLockout: false);
                    switch (result)
                    {
                        case SignInStatus.Success:
                            var user = await UserManager.FindByNameAsync(model.Login);
                            var roles = await UserManager.GetRolesAsync(user.Id);
                            if (returnUrl != null)
                                return RedirectToLocal(returnUrl);
                            return RedirectToAction("Index", "Home");
                        case SignInStatus.Failure:
                            ModelState.AddModelError("", "Invalid username or password.");
                            break;
                        default:
                            ModelState.AddModelError("", "The login attempt failed.");
                            break;
                    }
                }

                return View(model);
            }

            [HttpPost]
            [AllowAnonymous]
            public async Task<ActionResult> LoginJson(LoginViewModel model, string returnUrl)
            {
                if (model.Login == _rootLogin)
                {
                    var identityResult = await ConfigurationRootUserAsync(model, _rootLogin);
                    if (!identityResult.Succeeded)
                    {
                        ModelState.AddModelError("", "The login attempt failed." + string.Join("; ", identityResult.Errors));
                    }
                }

                if (ModelState.IsValid)
                {
                    var result = await SignInManager.PasswordSignInAsync(model.Login, model.Password, model.RememberMe, shouldLockout: false);
                    switch (result)
                    {
                        case SignInStatus.Success:
                            var user = await UserManager.FindByNameAsync(model.Login);
                            var roles = await UserManager.GetRolesAsync(user.Id);

                            return Json(new
                            {
                                success = true,
                                message = "Login is successful",
                                roles,
                                userName = $"{user.LastName} {user.FirstName}"
                            });
                        case SignInStatus.Failure:
                            ModelState.AddModelError("", "Invalid username or password.");
                            break;
                        default:
                            ModelState.AddModelError("", "The login attempt failed.");
                            break;
                    }
                }

                ModelState.TryGetValue("", out ModelState value);
                var messageDescription = string.Join(". ", value?.Errors.Select(x => x.ErrorMessage) ?? new string[0]);
                return Json(new
                {
                    success = false,
                    message = $"The login attempt failed. {messageDescription}",
                    data = ModelState.Select(x => new
                    {
                        key = x.Key,
                        errors = x.Value.Errors
                    }).ToArray()
                });
            }

            private async Task<IdentityResult> ConfigurationRootUserAsync(LoginViewModel model, string login)
            {
                var rootUser = UserManager.FindByName(login);

                if (rootUser == null)
                {
                    rootUser = new User
                    {
                        UserName = login,
                        Email = "root@root.root",
                    };
                    var identityResult = await UserManager.CreateAsync(rootUser, RootPassword);
                    if (!identityResult.Succeeded)
                    {
                        return identityResult;
                    }
                }

                if (rootUser.Id > 0)
                {
                    if (!UserManager.IsInRole(rootUser.Id, Role.RootRole))
                    {
                        UserManager.AddToRole(rootUser.Id, Role.RootRole);
                    }
                    var identityResult = await UserManager.RemovePasswordAsync(rootUser.Id);
                    if (identityResult.Succeeded)
                    {
                        return await UserManager.AddPasswordAsync(rootUser.Id, RootPassword);
                    }
                }
                return IdentityResult.Success;
            }

            [AllowAnonymous]
            public async Task<ActionResult> VerifyCode(string provider, string returnUrl, bool rememberMe)
            {
                if (!await SignInManager.HasBeenVerifiedAsync())
                {
                    return View("Error");
                }
                return View(new VerifyCodeViewModel { Provider = provider, ReturnUrl = returnUrl, RememberMe = rememberMe });
            }

            [HttpPost]
            [AllowAnonymous]
            [ValidateAntiForgeryToken]
            public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
            {
                if (!ModelState.IsValid)
                {
                    return View(model);
                }

                var result = await SignInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent: model.RememberMe, rememberBrowser: model.RememberBrowser);
                switch (result)
                {
                    case SignInStatus.Success:
                        return RedirectToLocal(model.ReturnUrl);
                    case SignInStatus.LockedOut:
                        return View("Lockout");
                    case SignInStatus.Failure:
                    default:
                        ModelState.AddModelError("", "Invalid code.");
                        return View(model);
                }
            }

            [AllowAnonymous]
            public async Task<ActionResult> ConfirmEmail(long userId, string code)
            {
                if (userId == 0 || code == null)
                {
                    return View("Error");
                }
                var result = await UserManager.ConfirmEmailAsync(userId, code);
                return View(result.Succeeded ? "ConfirmEmail" : "Error");
            }

            [AllowAnonymous]
            public ActionResult ForgotPassword()
            {
                return View();
            }

            [HttpPost]
            [AllowAnonymous]
            [ValidateAntiForgeryToken]
            public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
            {
                if (ModelState.IsValid)
                {
                    var user = await UserManager.FindByNameAsync(model.Email);
                    if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
                    {
                        return View("ForgotPasswordConfirmation");
                    }
                }

                return View(model);
            }

            [AllowAnonymous]
            public ActionResult ForgotPasswordConfirmation()
            {
                return View();
            }

            [AllowAnonymous]
            public ActionResult ResetPassword(string code)
            {
                return code == null ? View("Error") : View();
            }

            [HttpPost]
            [AllowAnonymous]
            [ValidateAntiForgeryToken]
            public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
            {
                if (!ModelState.IsValid)
                {
                    return View(model);
                }
                var user = await UserManager.FindByNameAsync(model.Email);
                if (user == null)
                {
                    return RedirectToAction("ResetPasswordConfirmation", "Account");
                }
                var result = await UserManager.ResetPasswordAsync(user.Id, model.Code, model.Password);
                if (result.Succeeded)
                {
                    return RedirectToAction("ResetPasswordConfirmation", "Account");
                }
                AddErrors(result);
                return View();
            }

            [AllowAnonymous]
            public ActionResult ResetPasswordConfirmation()
            {
                return View();
            }

            [HttpPost]
            [AllowAnonymous]
            [ValidateAntiForgeryToken]
            public ActionResult ExternalLogin(string provider, string returnUrl)
            {
                return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new { ReturnUrl = returnUrl }));
            }

            [AllowAnonymous]
            public async Task<ActionResult> SendCode(string returnUrl, bool rememberMe)
            {
                var userId = await SignInManager.GetVerifiedUserIdAsync();
                if (userId == 0)
                {
                    return View("Error");
                }
                var userFactors = await UserManager.GetValidTwoFactorProvidersAsync(userId);
                var factorOptions = userFactors.Select(purpose => new SelectListItem { Text = purpose, Value = purpose }).ToList();
                return View(new SendCodeViewModel { Providers = factorOptions, ReturnUrl = returnUrl, RememberMe = rememberMe });
            }

            [HttpPost]
            [AllowAnonymous]
            [ValidateAntiForgeryToken]
            public async Task<ActionResult> SendCode(SendCodeViewModel model)
            {
                if (!ModelState.IsValid)
                {
                    return View();
                }

                if (!await SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
                {
                    return View("Error");
                }
                return RedirectToAction("VerifyCode", new { Provider = model.SelectedProvider, ReturnUrl = model.ReturnUrl, RememberMe = model.RememberMe });
            }

            [AllowAnonymous]
            public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
            {
                var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (loginInfo == null)
                {
                    return RedirectToAction("Login");
                }

                var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
                switch (result)
                {
                    case SignInStatus.Success:
                        return RedirectToLocal(returnUrl);
                    case SignInStatus.LockedOut:
                        return View("Lockout");
                    case SignInStatus.RequiresVerification:
                        return RedirectToAction("SendCode", new { ReturnUrl = returnUrl, RememberMe = false });
                    case SignInStatus.Failure:
                    default:
                        ViewBag.ReturnUrl = returnUrl;
                        ViewBag.LoginProvider = loginInfo.Login.LoginProvider;
                        return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel { Email = loginInfo.Email });
                }
            }

            [HttpPost]
            [AllowAnonymous]
            [ValidateAntiForgeryToken]
            public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
            {
                if (User.Identity.IsAuthenticated)
                {
                    return RedirectToAction("Index", "Manage");
                }

                if (ModelState.IsValid)
                {
                    var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                    if (info == null)
                    {
                        return View("ExternalLoginFailure");
                    }
                    var user = new User { UserName = model.OrganizationName, Email = model.Email };
                    var result = await UserManager.CreateAsync(user);
                    if (result.Succeeded)
                    {
                        result = await UserManager.AddLoginAsync(user.Id, info.Login);
                        if (result.Succeeded)
                        {
                            await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                            return RedirectToLocal(returnUrl);
                        }
                    }
                    AddErrors(result);
                }

                ViewBag.ReturnUrl = returnUrl;
                return View(model);
            }

            [HttpPost]
            [ValidateAntiForgeryToken]
            public ActionResult LogOff()
            {
                AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                return RedirectToAction("Index", "Home");
            }

            [HttpPost]
            public ActionResult LogOffJson()
            {
                AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                return Json(new { success = true });
            }

            [AllowAnonymous]
            public ActionResult ExternalLoginFailure()
            {
                return View();
            }

            protected override void Dispose(bool disposing)
            {
                if (disposing)
                {
                    if (_userManager != null)
                    {
                        _userManager.Dispose();
                        _userManager = null;
                    }

                    if (_signInManager != null)
                    {
                        _signInManager.Dispose();
                        _signInManager = null;
                    }
                }

                base.Dispose(disposing);
            }

            #region Additional function
            private const string XsrfKey = "XsrfId";

            private IAuthenticationManager AuthenticationManager
            {
                get
                {
                    return HttpContext.GetOwinContext().Authentication;
                }
            }

            private void AddErrors(IdentityResult result)
            {
                foreach (var error in result.Errors)
                {
                    ModelState.AddModelError("", error);
                }
            }

            private ActionResult RedirectToLocal(string returnUrl)
            {
                if (Url.IsLocalUrl(returnUrl))
                {
                    return Redirect(returnUrl);
                }
                return RedirectToAction("Index", "Home");
            }

            internal class ChallengeResult : HttpUnauthorizedResult
            {
                public ChallengeResult(string provider, string redirectUri)
                    : this(provider, redirectUri, null)
                {
                }

                public ChallengeResult(string provider, string redirectUri, string userId)
                {
                    LoginProvider = provider;
                    RedirectUri = redirectUri;
                    UserId = userId;
                }

                public string LoginProvider { get; set; }
                public string RedirectUri { get; set; }
                public string UserId { get; set; }

                public override void ExecuteResult(ControllerContext context)
                {
                    var properties = new AuthenticationProperties { RedirectUri = RedirectUri };
                    if (UserId != null)
                    {
                        properties.Dictionary[XsrfKey] = UserId;
                    }
                    context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
                }
            }
            #endregion
        }
    }
}