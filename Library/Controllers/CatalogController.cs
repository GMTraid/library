﻿using Library.Database.Entities.Identity;
using Library.Domain.Attributes;
using Library.Extensions;
using Library.Models.Catalog;
using Library.Models.Checkout;
using Library.Models.Repository.Interfaces;
using Library.Services.Interfaces;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Library.Controllers
{
    [AuthorizeRoles(Role.AdminRole, Role.LibrarianRole, Role.ReaderRole)]
    public class CatalogController : Controller
    {
        public ILibraryAssetService _asset;
        public ICheckoutService _checkouts;

        [HttpGet]
        [HttpPost]
        public async Task<ActionResult> Index()
        {
            var assetModels = _asset.GetAll();

            var listingResult = await Task.WhenAll(assetModels
                .Select(async x => new AssetIndexListingModel
                {
                    Id = x.Id,
                    ImageUrl = x.ImageUrl,
                    AuthorOrDirector = await _asset.GetAuthorOrDirectorAsync(x.Id),
                    DeweyCallNumber = await _asset.GetDeweyIndexAsync(x.Id),
                    Title = x.Name,
                    Type = await _asset.GetTypeAsync(x.Id),
                    Description = await _asset.GetDescriptionAsync(x.Id),
                    Publisher = await _asset.GetPublisherAsync(x.Id),
                }));

            var model = new AssetIndexModel()
            {
                Assets = listingResult
            };

            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> Detail(long id)
        {
            var asset = await _asset.GetByIdAsync(id);

            var currentHolds = await Task.WhenAll(_checkouts.GetCurrentHolds(id)
                .Select(async x => new AssetHoldModel
                {
                    HoldPlaced = await _checkouts.GetCurrentHoldPlacedAsync(x.Id),
                    PatronName = await _checkouts.GetCurrentHoldPatronNameAsync(x.Id)
                }));

            var model = new AssetDetailModel
            {
                AssetId = id,
                Title = asset.Name,
                Description = asset.Description?.GetRandomString(),
                Year = asset.Year,
                Publisher = await _asset.GetPublisherAsync(id),
                Status = asset.Status.Name,
                ImageUrl = asset.ImageUrl,
                AuthorOrDirector = _asset.GetAuthorOrDirector(id),
                CurentLocation = _asset.GetCurrentLocation(id).Name,
                DeweyCallNumber = _asset.GetDeweyIndex(id),
                CheckoutHistory = _checkouts.GetCheckoutHistory(id),
                LatestCheckout = await _checkouts.GetLatestCheckout(id),
                PatronName = await _checkouts.GetCurrentCheckoutPatronAsync(id),
                CurrentHolds = currentHolds
            };

            return View(model);
        }

        [HttpGet]
        public async Task<ActionResult> Checkout(long id)
        {
            var asset = await _asset.GetByIdAsync(id);

            var model = new CheckoutModel
            {
                AssetId = id,
                ImageUrl = asset.ImageUrl,
                Title = asset.Name,
                LibraryCardId = "",
                IsCheckedOut = await _checkouts.IsCheckedOutAsync(id)
            };

            return View(model);
        }

        [HttpGet]
        public ActionResult CheckIn(long id)
        {
            _checkouts.CheckInTimeAsync(id);
            return RedirectToAction("Detail", new { id });
        }

        [HttpGet]
        public async Task<ActionResult> Hold(long id)
        {
            var asset = await _asset.GetByIdAsync(id);

            var model = new CheckoutModel
            {
                AssetId = id,
                ImageUrl = asset.ImageUrl,
                Title = asset.Name,
                LibraryCardId = "",
                IsCheckedOut = await _checkouts.IsCheckedOutAsync(id),
                HoldCount = _checkouts.GetCurrentHolds(id).Count()
            };

            return View(model);
        }

        public ActionResult MarkFind(long id)
        {
            _checkouts.MarkFound(id);
            return RedirectToAction("DEtail", new { id });
        }

        public ActionResult MarkLost(long id)
        {
            _checkouts.MarkLost(id);
            return RedirectToAction("Detail", new { id });
        }

        [HttpPost]
        public ActionResult PlaceCheckout(long id, long libraryCardId)
        {
            _checkouts.CheckOutTimeAsync(id, libraryCardId);

            return RedirectToAction("Detail", new { id });
        }

        [HttpPost]
        public ActionResult PlaceHold(long id, long libraryCardId)
        {
            _checkouts.PlaceHoldAsync(id, libraryCardId);

            return RedirectToAction("Detail", new { id });
        }
    }
}