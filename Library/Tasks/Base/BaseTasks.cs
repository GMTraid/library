﻿using Castle.MicroKernel.Lifestyle;
using Castle.Windsor;
using Library.Domain.Config;
using NLog;
using Quartz;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Library.Tasks.Base
{
    public abstract class BaseTask<TService> : IJob
    {
        public IWindsorContainer Container { get; set; }
        public LibraryConfiguration Config { get; set; }

        protected abstract SemaphoreSlim GetSyncObject();
        protected abstract Task ExecuteInScopeAsync(IJobExecutionContext context, TService service, Logger log);
        protected virtual void AdditionalLogError(Logger log, Exception ex) { }

        public async Task Execute(IJobExecutionContext context)
        {
            var syncObject = GetSyncObject();
            if (syncObject.CurrentCount == 0) return;
            await syncObject.WaitAsync();

            var log = LogManager.GetLogger(this.GetType().FullName);
            try
            {
                using (Container.BeginScope())
                {
                    Exception exception = null;
                    Guid? exceptionGuid = null;

                    var service = Container.Resolve<TService>();
                    log.Info($"Start.");

                    try
                    {
                        await ExecuteInScopeAsync(context, service, log);
                    }
                    catch (Exception ex)
                    {
                        exception = ex;
                        exceptionGuid = Guid.NewGuid();
                        AdditionalLogError(log, ex);
                        log.Error(exceptionGuid?.ToString("B"));
                        log.Error(ex);
                    }
                    finally
                    {
                        Container.Release(service);
                        if (exception != null)
                        {
                            log.Info($"Exception: {exceptionGuid:B} {exception.Message}");
                        }
                        log.Info($"Finish.");
                    }
                }
            }
            catch (Exception ex)
            {
                log.Error(ex);
            }
            finally
            {
                syncObject.Release();
            }
        }
    }
}