﻿using Library.Tasks.Base;
using Library.Tasks.Intefaces;
using NLog;
using Quartz;
using System.Threading;
using System.Threading.Tasks;

namespace Library.Tasks
{
    public class ExecuteScheduledJobTask : BaseTask<IJobDispatcher>
    {
        protected readonly SemaphoreSlim _syncObject = new SemaphoreSlim(1, 1);

        protected override async Task ExecuteInScopeAsync(IJobExecutionContext context, IJobDispatcher service, Logger log)
        {
            await service.StartJobAsync(context);
        }

        protected override SemaphoreSlim GetSyncObject()
        {
            return _syncObject;
        }
    }
}