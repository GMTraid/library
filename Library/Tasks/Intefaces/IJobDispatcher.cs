﻿using Castle.Windsor;
using Library.Database.Entities.Base;
using Library.Database.Entities.Jobs;
using Library.Database.Enums;
using Library.Domain.Models.Interfaces;
using Library.Domain.Services.Exceptions;
using Library.Extensions;
using Newtonsoft.Json;
using Quartz;
using Quartz.Impl;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Library.Tasks.Intefaces
{
    public interface IJobDispatcher
    {
        Task InitJobs();
        Task ScheduleNewAsync(DateTimeOffset startAt, object data, string serviceType, string serviceMethod, long entityId, EntityType entityType);
        Task StartJobAsync(IJobExecutionContext context);
        Task UnscheduleAsync(long scheduledJobId);
        Task UnscheduleAsync(long entityId, EntityType entityType);
    }

    public class JobDispatcher : IJobDispatcher
    {
        private static IEnumerable<Type> Types = typeof(Entity).Assembly.GetTypes().Concat(typeof(JobDispatcher).Assembly.GetTypes());

        public IWindsorContainer Container { get; set; }
        public IRepository<ScheduledJob, long> RsScheduledJob { get; set; }

        public async Task InitJobs()
        {
            var jobs = await RsScheduledJob.GetAll()
                .Where(x => x.Status != ScheduledJobStatus.Completed)
                .Where(x => x.Status != ScheduledJobStatus.Failed)
                .Where(x => x.Status != ScheduledJobStatus.Unscheduled)
                .OrderBy(x => x.OriginStartAt)
                .ToListAsync();
            foreach (var job in jobs)
            {
                await RescheduleAsync(job);
            }
        }

        public async Task RescheduleAsync(ScheduledJob scheduledJob)
        {
            var dataType = Types.Where(x => x.FullName == scheduledJob.DataType).FirstOrDefault();
            var jobData = JsonConvert.DeserializeObject(scheduledJob.JsonData, dataType);

            var now = DateTimeOffset.Now.AddMinutes(1);
            var startAt = scheduledJob.StartAt < now ? now : scheduledJob.StartAt;
            var jobDetail = await ScheduleAsync(scheduledJob.StartAt, jobData, scheduledJob.ServiceType, scheduledJob.ServiceMethod);
            var jobKey = jobDetail.Key;

            scheduledJob.Status = ScheduledJobStatus.Rescheduled;
            scheduledJob.StartAt = startAt;
            scheduledJob.JobName = jobKey.Name;
            scheduledJob.JobGroup = jobKey.Group;

            await RsScheduledJob.UpdateAsync(scheduledJob);
        }

        public async Task ScheduleNewAsync(DateTimeOffset startAt, object data, string serviceType, string serviceMethod, long entityId, EntityType entityType)
        {
            if (startAt <= DateTimeOffset.Now) throw new ApplicationValidationException("StartAt must not be less than the current date");

            var jobDetail = await ScheduleAsync(startAt, data, serviceType, serviceMethod);

            await CreateScheduledJob(startAt, data, serviceType, serviceMethod, jobDetail.Key, entityId, entityType);
        }

        public async Task<IJobDetail> ScheduleAsync(DateTimeOffset startAt, object data, string serviceType, string serviceMethod)
        {
            var jsonData = JsonConvert.SerializeObject(data);
            var dataType = data.GetType().ToString();

            var scheduler = await StdSchedulerFactory.GetDefaultScheduler();

            var jobBuilder = JobBuilder.Create<ExecuteScheduledJobTask>();
            var jobDetail = jobBuilder.Build();
            var trigger = TriggerBuilder.Create().StartAt(startAt.ToUniversalTime()).Build();

            var jobWillStartAt = await scheduler.ScheduleJob(jobDetail, trigger);

            return jobDetail;
        }

        public async Task CreateScheduledJob(DateTimeOffset startAt, object data, string serviceType, string serviceMethod, JobKey jobKey, long entityId, EntityType entityType)
        {
            var jsonData = JsonConvert.SerializeObject(data);
            var dataType = data.GetType().ToString();

            var scheduledJob = new ScheduledJob
            {
                StartAt = startAt,
                DataType = dataType,
                JsonData = jsonData,
                JobName = jobKey.Name,
                JobGroup = jobKey.Group,
                OriginStartAt = startAt,
                ServiceType = serviceType,
                BindedEntityId = entityId,
                ServiceMethod = serviceMethod,
                BindedEntityType = entityType,
                CreatedAt = DateTimeOffset.Now,
            };
            await RsScheduledJob.CreateAsync(scheduledJob);
        }

        public async Task StartJobAsync(IJobExecutionContext context)
        {
            var key = context.JobDetail.Key;
            var scheduledJob = await RsScheduledJob.GetAll()
                .Where(x => x.JobName == key.Name)
                .Where(x => x.JobGroup == key.Group)
                .FirstOrDefaultAsync();

            scheduledJob.Status = ScheduledJobStatus.Executing;
            scheduledJob.StartedAt = DateTimeOffset.Now;

            await RsScheduledJob.UpdateAsync(scheduledJob);

            var dataType = Types.Where(x => x.FullName == scheduledJob.DataType).FirstOrDefault();
            var jobData = JsonConvert.DeserializeObject(scheduledJob.JsonData, dataType);

            var serviceType = Types.Where(x => x.FullName == scheduledJob.ServiceType).FirstOrDefault();
            var service = Container.Resolve(serviceType);
            var serviceMethod = serviceType.GetMethod(scheduledJob.ServiceMethod);
            try
            {
                if (serviceMethod.IsAsync())
                {
                    await (Task)serviceMethod.Invoke(service, new[] { jobData });
                }
                else
                {
                    serviceMethod.Invoke(service, new[] { jobData });
                }
                scheduledJob.Status = ScheduledJobStatus.Completed;
            }
            catch (Exception ex)
            {
                scheduledJob.Status = ScheduledJobStatus.Failed;
                scheduledJob.Exception = ex.ToString();
                throw;
            }
            finally
            {
                scheduledJob.EndedAt = DateTimeOffset.Now;
                await RsScheduledJob.UpdateAsync(scheduledJob);
                Container.Release(service);
            }
        }

        public async Task UnscheduleAsync(long scheduledJobId)
        {
            var scheduledJob = await RsScheduledJob.GetAsync(scheduledJobId);
            await UnscheduleAsync(scheduledJob);
            await RsScheduledJob.UpdateAsync(scheduledJob);
        }

        public async Task UnscheduleAsync(long entityId, EntityType entityType)
        {
            var scheduledJobs = await RsScheduledJob.GetAll()
                .Where(x => x.BindedEntityId == entityId)
                .Where(x => x.BindedEntityType == entityType)
                .Where(x => x.Status != ScheduledJobStatus.Completed)
                .Where(x => x.Status != ScheduledJobStatus.Failed)
                .Where(x => x.Status != ScheduledJobStatus.Unscheduled)
                .ToListAsync();
            foreach (var scheduledJob in scheduledJobs)
            {
                await UnscheduleAsync(scheduledJob);
            }
            await RsScheduledJob.UpdateRangeAsync(scheduledJobs);
        }

        private async Task UnscheduleAsync(ScheduledJob scheduledJob)
        {
            var scheduler = await StdSchedulerFactory.GetDefaultScheduler();
            var result = await scheduler.DeleteJob(new JobKey(scheduledJob.JobName, scheduledJob.JobGroup));
            scheduledJob.Status = ScheduledJobStatus.Unscheduled;
            scheduledJob.EndedAt = DateTimeOffset.Now;
        }
    }
}
