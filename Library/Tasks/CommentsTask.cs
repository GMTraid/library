﻿using Microsoft.AspNet.SignalR;
using System.Threading.Tasks;

namespace Library.Tasks
{
    public class CommentsTask : Hub
    {
        public override Task OnConnected()
        {
            Clients.All.User(Context.User.Identity.Name);
            return base.OnConnected();
        }
        public void Send(string message)
        {
            Clients.Caller.message("You" + message);
            Clients.Others.message($"{Context.User.Identity.Name}:{message}");
        }
    }
}