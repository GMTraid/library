﻿using Library.Services.Interfaces;
using Library.Tasks.Base;
using NLog;
using Quartz;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace Library.Tasks
{
    public class SendMessageTask : BaseTask<IMessageService>
    {
        protected static readonly SemaphoreSlim _syncObject = new SemaphoreSlim(1, 1);

        protected override async Task ExecuteInScopeAsync(IJobExecutionContext context, IMessageService service, Logger log)
        {
            service.OnLog += (string message, Exception exception) =>
            {
                if (exception == null)
                {
                    log.Info(message);
                }
                else
                {
                    log.Info($"{message} Exception:{exception.ToString()}");
                    log.Error(exception, message);
                }
            };

            var (SuccessCount, ErrorCount) = await service.SendMessageAsync();

            log.Info($"Processed successfully:{SuccessCount} with exception:{ErrorCount}");
        }

        protected override SemaphoreSlim GetSyncObject()
        {
            return _syncObject;
        }
    }
}