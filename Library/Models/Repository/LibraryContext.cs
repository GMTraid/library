﻿using Library.Database.Entities;
using Library.Database.Entities.Content;
using Library.Database.Entities.Content.Check;
using System.Data.Entity;

namespace Library.Models.Repository
{
    public class LibraryContext : DbContext
    {
        public LibraryContext() : base("DefaultConnection")  { }

        public DbSet<Book> Books { get; set; }

        public DbSet<Database.Entities.Content.Checkout> Checkouts { get; set; }

        public DbSet<CheckoutHistory> CheckoutHistories { get; set; }

        public DbSet<LibraryBranch> LibraryBranches { get; set; }

        public DbSet<BranchHours> BranchHours { get; set; }

        public DbSet<LibraryCard> LibraryCards { get; set; }

        public DbSet<Database.Entities.Identity.User> Patrons { get; set; }

        public DbSet<Status> Statuses { get; set; }

        public DbSet<Hold> Holds { get; set; }
    }
}