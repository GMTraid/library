﻿using System.Collections.Generic;

namespace Library.Models.User
{
    public class PatronIndexModel
    {
        public IEnumerable<PatronDetailModel> Patrons { get; set; }
    }
}