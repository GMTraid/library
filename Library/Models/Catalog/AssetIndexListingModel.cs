﻿namespace Library.Models.Catalog
{
    public class AssetIndexListingModel
    {
        public long Id { get; set; }

        public string ImageUrl { get; set; }

        public string Title { get; set; }

        public string Type { get; set; }

        public string AuthorOrDirector { get; set; }

        public string DeweyCallNumber { get; set; }

        public string NumberOfCopies { get; set; }

        public string Description { get; set; }

        public string Publisher { get; set; }

        public string Genre { get; set; }
    }
}