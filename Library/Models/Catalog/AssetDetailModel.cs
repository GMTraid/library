﻿using Library.Database.Entities.Content.Check;
using System.Collections.Generic;

namespace Library.Models.Catalog
{
    public class AssetDetailModel
    {
        public long AssetId { get; set; }

        public string Title { get; set; }
        public string Description { get; set; }

        public string Genre { get; set; }

        public string Publisher { get; set; }

        public string AuthorOrDirector { get; set; }

        public int Year { get; set; }

        public string DeweyCallNumber { get; set; }

        public string Status { get; set; }

        public string CurentLocation { get; set; }

        public string ImageUrl { get; set; }

        public string PatronName { get; set; }

        public Database.Entities.Content.Checkout LatestCheckout { get; set; }

        public IEnumerable<CheckoutHistory> CheckoutHistory { get; set; }

        public IEnumerable<AssetHoldModel> CurrentHolds { get; set; }
    }
}