﻿using Owin;
using Microsoft.Owin;

[assembly: OwinStartup(typeof(Library.Startup))]

namespace Library
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            app.MapSignalR();
        }
    }
}