﻿using System;
using System.Reflection;
using System.Threading.Tasks;

namespace Library.Extensions
{
    public static class TypeExtensions
    {
        public static bool IsNullable(this Type type)
        {
            return type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>);
        }

        public static bool IsAsync(this MethodInfo method)
        {
            return method.ReturnType == typeof(Task) || method.ReturnType.BaseType == typeof(Task);
        }
    }
}