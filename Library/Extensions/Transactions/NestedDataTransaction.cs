﻿using System;
using System.Data.Entity;

namespace Library.Extensions.Transactions
{
    public class NestedDataTransaction : IDataTransaction
    {
        protected DbContext DbContext { get; set; }
        protected DbContextTransaction CurrentTransaction { get; set; }

        protected bool IsCommited { get; set; }
        protected bool IsRollback { get; set; }
        protected bool IsMainTransaction { get; set; }

        public NestedDataTransaction(DbContext dbContext)
        {
            DbContext = dbContext;
            CurrentTransaction = DbContext.Database.CurrentTransaction;
            IsMainTransaction = CurrentTransaction == null;
            if (IsMainTransaction)
            {
                CurrentTransaction = DbContext.Database.BeginTransaction();
            }
        }

        public virtual void Commit()
        {
            if (IsMainTransaction)
            {
                CommitImplementation();
            }
        }

        protected virtual void CommitImplementation()
        {
            CurrentTransaction.Commit();
            IsCommited = true;
        }

        public virtual void Rollback()
        {
            try
            {
                if (CurrentTransaction.UnderlyingTransaction.Connection != null)
                {
                    CurrentTransaction.Rollback();
                }
                IsRollback = true;
            }
            catch (Exception ex)
            {
                throw new Exception("An error occurred while canceling the transaction", ex);
            }
        }

        public void Dispose()
        {
            if (IsMainTransaction)
            {
                if (!IsCommited && !IsRollback)
                {
                    Rollback();
                }
                CurrentTransaction.Dispose();
            }
        }
    }
}