﻿using Library.Domain.Models;
using System.Data.Entity;

namespace Library.Extensions.Transactions
{
    public class LogableNestedDataTransaction : NestedDataTransaction
    {
        protected LogableDbContext LogableDbContext { get; set; }

        public LogableNestedDataTransaction(DbContext dbContext) : base(dbContext)
        {
            if (dbContext is LogableDbContext loagableDbContext)
            {
                LogableDbContext = loagableDbContext;
            }
        }

        protected override void CommitImplementation()
        {
            LogableDbContext?.SaveLogsImplementation();
            base.CommitImplementation();
        }

        public override void Rollback()
        {
            LogableDbContext?.ClearLogs();

            base.Rollback();
        }
    }
}