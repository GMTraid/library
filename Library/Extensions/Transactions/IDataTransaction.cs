﻿using System;

namespace Library.Extensions.Transactions
{
    public interface IDataTransaction : IDisposable
    {
        void Commit();
        void Rollback();
    }
}
