﻿using Library.Domain.Models.Filters;
using Library.Domain.Params;
using Library.Domain.Params.RepresentInfo;
using Library.Provider;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;

namespace Library.Extensions
{
    public static class QueryableGenericExtensions
    {
        public static IQueryable<T> WhereIf<T>(this IQueryable<T> query, bool condition, Expression<Func<T, bool>> predicate)
        {
            if (condition)
            {
                query = query.Where(predicate);
            }
            return query;
        }

        public static IQueryable<T> WhereIf<T>(this IQueryable<T> query, bool condition, Expression<Func<T, bool>> predicate, Expression<Func<T, bool>> elsePredicate)
        {
            if (condition)
            {
                query = query.Where(predicate);
            }
            else
            {
                query = query.Where(elsePredicate);
            }
            return query;
        }

        public static IQueryable<T> FilterOrderPaging<T>(this IQueryable<T> query, LoadParams loadParams)
        {
            return query.Filter(loadParams).Order(loadParams).Paging(loadParams);
        }

        public static IQueryable<T> FilterOrderPaging<T>(this IQueryable<T> query, LoadParams loadParams, out int totalCount)
        {
            var qData = query.Filter(loadParams);
            totalCount = qData.Count();
            return qData.Order(loadParams).Paging(loadParams);
        }

        public static IQueryable<T> Filter<T>(this IQueryable<T> query, LoadParams loadParams)
        {
            if (loadParams == null)
            {
                return query;
            }

            return Filter(query, loadParams.GetFiltersInfo());
        }

        public static IQueryable<T> Filter<T>(this IQueryable<T> query, FilterInfo[] filtersInfo)
        {
            Expression resultExpression = null;
            var param = Expression.Parameter(typeof(T), "x");


            foreach (var filterInfo in filtersInfo)
            {
                var filterExpression = GetFilterExpression(param, filterInfo);

                if (filterExpression != null)
                {
                    if (resultExpression == null)
                    {
                        resultExpression = filterExpression;
                    }
                    else
                    {
                        resultExpression = Expression.And(resultExpression, filterExpression);
                    }
                }
            }
            if (resultExpression != null)
            {
                var lambda = Expression.Lambda<Func<T, bool>>(resultExpression, param);
                return query.Where(lambda);
            }
            return query;
        }

        private static Expression GetFilterExpression(ParameterExpression param, FilterInfo filterInfo)
        {
            var isSimpleFilter = IsSimpleFilter(filterInfo);

            if (isSimpleFilter)
            {
                return GetSimpleFilterExpression(param, filterInfo);
            }
            else
            {
                return GetComplexFilterExpression(param, filterInfo);
            }
        }

        private static Expression GetComplexFilterExpression(ParameterExpression param, FilterInfo filterInfo)
        {
            var leftExpression = GetFilterExpression(param, filterInfo.Left);
            var rightExpression = GetFilterExpression(param, filterInfo.Right);

            if (leftExpression == null || rightExpression == null)
            {
                return leftExpression ?? rightExpression;
            }

            switch (filterInfo.Action)
            {
                case FilterActions.And:
                    return Expression.And(leftExpression, rightExpression);
                case FilterActions.Or:
                    return Expression.Or(leftExpression, rightExpression);
                default:
                    throw new Exception("A complex filter must have an explicit filter action type");
            }
        }

        private static Expression GetSimpleFilterExpression(ParameterExpression param, FilterInfo filterInfo)
        {
            if (!string.IsNullOrWhiteSpace(filterInfo.DataIndex))
            {
                if (!string.IsNullOrEmpty(filterInfo.Value))
                {
                    var propertyExpression = GetPropertyAccessExpression(param, filterInfo.DataIndex);
                    var propertyType = propertyExpression.Type;
                    return GetFilterPropertyExpression(filterInfo, propertyExpression, propertyType);
                }
                return null;
            }
            throw new Exception("A simple filter must have a property that will be used for filtering");
        }

        private static bool IsSimpleFilter(FilterInfo filterInfo)
        {
            var complexFilterActions = new[] { FilterActions.Or, FilterActions.And };
            if (string.IsNullOrWhiteSpace(filterInfo.Action))
            {
                filterInfo.Action = FilterActions.Default;
            }

            return !complexFilterActions.Contains(filterInfo.Action);
        }

        public static IQueryable<T> OldFilter<T>(this IQueryable<T> query, LoadParams loadParams)
        {
            if (loadParams == null)
            {
                return query;
            }

            var filtersInfo = loadParams.GetFiltersInfo();
            var queryType = query.GetType();
            var type = queryType.GetGenericArguments().Single();

            Expression resultExpression = null;
            var param = Expression.Parameter(query.ElementType, "x");

            foreach (var filterInfo in filtersInfo)
            {
                if (!string.IsNullOrWhiteSpace(filterInfo.DataIndex) && !string.IsNullOrEmpty(filterInfo.Value))
                {
                    var propertyExpression = GetPropertyAccessExpression(param, filterInfo.DataIndex);
                    var propertyType = propertyExpression.Type;
                    var filterPropertyExpression = GetFilterPropertyExpression(filterInfo, propertyExpression, propertyType);

                    if (resultExpression == null)
                    {
                        resultExpression = filterPropertyExpression;
                    }
                    else
                    {
                        resultExpression = Expression.And(resultExpression, filterPropertyExpression);
                    }
                }
            }
            if (resultExpression != null)
            {
                var lambda = Expression.Lambda<Func<T, bool>>(resultExpression, param);
                return query.Where(lambda);
            }
            return query;
        }

        private static Expression GetFilterPropertyExpression(FilterInfo filterInfo, Expression propertyExpression, Type propertyType)
        {
            if (propertyType == typeof(string))
            {
                var constExpression = Expression.Constant(filterInfo.Value.ToUpper(), typeof(string));
                var toUpperMethod = propertyType.GetMethod("ToUpper", new Type[0]);
                var containsMethod = propertyType.GetMethod("Contains");

                var toUpperMethodExpression = Expression.Call(propertyExpression, toUpperMethod);
                return Expression.Call(toUpperMethodExpression, containsMethod, constExpression);
            }
            else if (propertyType == typeof(DateTime) || propertyType == typeof(DateTime?))
            {
                var dateValue = (DateTime?)DateTime.Parse(filterInfo.Value).Date;
                var constExpression = Expression.Constant(dateValue, typeof(DateTime?));
                var truncateMethod = GetMethodInfo<DateTime?>(DbFunctions.TruncateTime);
                var convertedProperyExpression = Expression.Convert(propertyExpression, typeof(DateTime?));
                var leftExpression = Expression.Call(truncateMethod, convertedProperyExpression);

                return Expression.Equal(leftExpression, constExpression);
            }
            else if (propertyType == typeof(DateTimeOffset) || propertyType == typeof(DateTimeOffset?))
            {
                var dateValue = (DateTimeOffset?)DateTimeOffset.Parse(filterInfo.Value).Date;
                var constExpression = Expression.Constant(dateValue, typeof(DateTimeOffset?));
                var truncateMethod = GetMethodInfo<DateTimeOffset?>(DbFunctions.TruncateTime);
                var convertedProperyExpression = Expression.Convert(propertyExpression, typeof(DateTimeOffset?));
                var leftExpression = Expression.Call(truncateMethod, convertedProperyExpression);

                return Expression.Equal(leftExpression, constExpression);
            }
            else
            {
                var converter = TypeDescriptor.GetConverter(propertyType);

                object typedValue;
                if (propertyType.IsEnum || propertyType.IsNullableEnum())
                {
                    var type = propertyType.IsNullableEnum() ? Nullable.GetUnderlyingType(propertyType) : propertyType;
                    typedValue = filterInfo.Value.HasValue() ? Enum.Parse(type, filterInfo.Value) : null;
                }
                else
                {
                    typedValue = converter.ConvertFromString(null, CultureInfo.InvariantCulture, filterInfo.Value);
                }

                if (filterInfo.CheckValueContains && !propertyType.IsEnum && !propertyType.IsNullableEnum() && propertyType != typeof(Boolean))
                {
                    if (typedValue == null)
                    {
                        var nullExpression = Expression.Constant(typedValue, propertyType);
                        return Expression.Equal(propertyExpression, nullExpression);
                    }
                    else
                    {
                        var underlyingType = Nullable.GetUnderlyingType(propertyType);

                        var value = typedValue.ToString().ToUpper();
                        var constExpression = Expression.Constant(value, typeof(string));

                        var numberTypes = new[] { typeof(double), typeof(decimal) };

                        if (numberTypes.Contains(typedValue) || numberTypes.Contains(underlyingType))
                        {
                            value = value.Replace(',', '.');
                            constExpression = Expression.Constant(value, typeof(string));
                        }

                        var containsMethod = typeof(string).GetMethod("Contains");

                        if (underlyingType != null)
                        {
                            var toStringMethod = underlyingType.GetMethod("ToString", new Type[0]);
                            var hasValueExpression = Expression.Property(propertyExpression, "HasValue");
                            var valueExpression = Expression.Property(propertyExpression, "Value");
                            var toStringMethodExpression = Expression.Call(valueExpression, toStringMethod);
                            var containsExpression = Expression.Call(toStringMethodExpression, containsMethod, constExpression);

                            return Expression.AndAlso(hasValueExpression, containsExpression);
                        }
                        else
                        {
                            var toStringMethod = propertyType.GetMethod("ToString", new Type[0]);
                            var toStringMethodExpression = Expression.Call(propertyExpression, toStringMethod);
                            return Expression.Call(toStringMethodExpression, containsMethod, constExpression);
                        }
                    }
                }

                var rightExpression = Expression.Constant(typedValue, propertyType);
                return Expression.Equal(propertyExpression, rightExpression);
            }
        }

        public static IQueryable<T> Paging<T>(this IQueryable<T> query, LoadParams loadParams)
        {
            if (loadParams == null)
            {
                return query;
            }

            return Paging(query, loadParams.Start, loadParams.Length);
        }

        public static IQueryable<T> Paging<T>(this IQueryable<T> query, int start, int length)
        {
            var result = query.Skip(start);
            if (length > 0)
            {
                result = result.Take(length);
            }

            return result;
        }

        public static IQueryable<T> Order<T>(this IQueryable<T> query, LoadParams loadParams)
        {
            if (loadParams == null)
            {
                return query;
            }

            return Order(query, loadParams.GetOrdersInfo());
        }

        public static IQueryable<T> Order<T>(this IQueryable<T> query, OrderInfo[] ordersInfo)
        {
            var result = query;
            var queryType = typeof(T);

            if (ordersInfo.Any())
            {
                var isFirst = true;
                foreach (var order in ordersInfo)
                {
                    var keySelector = GetOrderExpression(order, queryType);
                    var nullsKeySelector = GetNullOrderExpression(order, queryType);

                    Type selectorType = order.NullsInEnd ? nullsKeySelector.Body.Type : keySelector.Body.Type;
                    MethodInfo orderMethod = isFirst
                        ? GetOrderMethodInfo(typeof(Queryable), queryType, selectorType, !order.IsDesc)
                        : GetThenMethodInfo(typeof(Queryable), queryType, selectorType, !order.IsDesc);

                    if (order.NullsInEnd)
                    {
                        result = (IQueryable<T>)orderMethod.Invoke(null, new object[] { result, nullsKeySelector });
                        orderMethod = GetThenMethodInfo(typeof(Queryable), queryType, keySelector.Body.Type, !order.IsDesc);
                    }

                    result = (IQueryable<T>)orderMethod.Invoke(null, new object[] { result, keySelector });

                    isFirst = false;
                }
            }
            else
            {
                result = result.OrderBy(x => true);
            }
            return result;
        }

        private static LambdaExpression GetOrderExpression(OrderInfo orderInfo, Type type)
        {
            var p = Expression.Parameter(type, "x");
            var property = GetPropertyAccessExpression(p, orderInfo.DataIndex);
            var delegateType = typeof(Func<,>).MakeGenericType(type, property.Type);
            var expression = Expression.Lambda(delegateType, property, p);
            return expression;
        }

        private static LambdaExpression GetNullOrderExpression(OrderInfo orderInfo, Type type)
        {
            var p = Expression.Parameter(type, "x");
            var property = GetPropertyAccessExpression(p, orderInfo.DataIndex);

            var constExpression = Expression.Constant(null);
            Expression resultExpression = property.Type.IsNullable()
                ? (orderInfo.IsDesc ? Expression.NotEqual(property, constExpression) : Expression.Equal(property, constExpression))
                : property;

            var delegateType = typeof(Func<,>).MakeGenericType(type, resultExpression.Type);
            var expression = Expression.Lambda(delegateType, resultExpression, p);
            return expression;
        }

        private static Expression GetPropertyAccessExpression(Expression source, string propertyMap)
        {
            var propertiesNames = propertyMap.Split(new[] { '.' }, StringSplitOptions.RemoveEmptyEntries);
            var result = source;
            foreach (var propertyName in propertiesNames)
            {
                result = Expression.Property(result, propertyName);
            }
            return result;
        }

        private static MethodInfo GetOrderMethodInfo(Type typeFromMethod, Type dateType, Type keyType, bool isAsc)
        {
            var methodName = isAsc ? "OrderBy" : "OrderByDescending";
            var method = typeFromMethod.GetMethods().First(item => item.Name == methodName && item.GetParameters().Count() == 2);

            return method.MakeGenericMethod(dateType, keyType);
        }

        private static MethodInfo GetThenMethodInfo(Type typeFromMethod, Type dateType, Type keyType, bool isAsc)
        {
            var methodName = isAsc ? "ThenBy" : "ThenByDescending";
            var method = typeFromMethod.GetMethods().First(item => item.Name == methodName && item.GetParameters().Count() == 2);

            return method.MakeGenericMethod(dateType, keyType);
        }

        private static MethodInfo GetMethodInfo<T>(Func<T, T> m)
        {
            return m.GetMethodInfo();
        }

        public static IQueryable<T> AsAsyncQueryable<T>(this IQueryable<T> query)
        {
            return new DbAsyncEnumerable<T>(query);
        }

        public static IQueryable<T> AsAsyncQueryable<T>(this IEnumerable<T> query)
        {
            return new DbAsyncEnumerable<T>(query);
        }
    }
}