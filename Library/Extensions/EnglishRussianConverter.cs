﻿using System;
using System.Linq;

namespace Library.Extensions
{
    public static class EnglishRussianConverter
    {
        public enum Language
        {
            Russian,
            English,
            Mixed,
            Undefined
        }
        private const string russian = "йцукенгшщзхъфывапролджэячсмитьбюёЙЦУКЕНГШЩЗХЪФЫВАПРОЛДЖЭЯЧСМИТЬБЮЁ";
        private const string english = "qwertyuiop[]asdfghjkl;'zxcvbnm,.`QWERTYUIOP{}ASDFGHJKL:\"ZXCVBNM<>~";
        private const string symRussian = "укехаросмтУКЕХВАНРОСМТ";
        private const string symEnglish = "ykexapocmtYKEXBAHPOCMT";

        private const string translitRussian = "а|б|в|г|д|е|ё|ж|з|и|й|к|л|м|н|о|п|р|с|т|у|ф|х|ц|ч|ш|щ|ъ|ы|ь|э|ю|я|А|Б|В|Г|Д|Е|Ё|Ж|З|И|Й|К|Л|М|Н|О|П|Р|С|Т|У|Ф|Х|Ц|Ч|Ш|Щ|Ъ|Ы|Ь|Э|Ю|Я";
        private const string translitEnglish = "a|b|v|g|d|e|yo|j|z|i|y|k|l|m|n|o|p|r|s|t|u|f|h|c|ch|sh|sch||y||e|yu|ya|A|B|V|G|D|E|Yo|J|Z|I|Y|K|L|M|N|O|P|R|S|T|U|F|H|C|Ch|Sh|Sch||Y||E|Yu|Ya";

        public static string InvertSymbols(string source, Language from, Language to)
        {
            string result = string.Empty;
            string fromStr = symRussian;
            string toStr = symEnglish;
            if (from == Language.English)
            {
                fromStr = symEnglish;
                toStr = symRussian;
            }
            foreach (var ch in source)
            {
                int ind = fromStr.IndexOf(ch);
                if (ind != -1)
                {
                    result += toStr[ind];
                }
                else
                {
                    result += ch;
                }
            }
            return result;
        }

        public static Language DetectLanguage(string source)
        {
            Language result = Language.Undefined;
            if (russian.IndexOf(source.Substring(0, 1)) != -1)
            {
                result = Language.Russian;
            }
            else if (english.IndexOf(source.Substring(0, 1)) != -1)
            {
                result = Language.English;
            }
            if (result != Language.Undefined)
            {
                foreach (var ch in source)
                {
                    bool isRussian = russian.IndexOf(ch) != -1;
                    bool isEnglish = english.IndexOf(ch) != -1;
                    if ((isRussian && result == Language.English) || (isEnglish && result == Language.Russian))
                    {
                        result = Language.Mixed;
                        break;
                    }
                }
            }
            return result;
        }

        public static string InvertToRussian(string source)
        {
            return Invert(source, english, russian);
        }

        public static string InvertToEnglish(string source)
        {
            return Invert(source, russian, english);
        }

        private static string Invert(string src, string from, string to)
        {
            string result = string.Empty;
            foreach (var ch in src)
            {
                int ind = from.IndexOf(ch);
                if (ind != -1)
                {
                    result += to[ind];
                }
                else
                {
                    result += ch;
                }
            }
            return result;
        }


        public static string Translit(string src, string from, string to)
        {
            var fromSimbols = from.Split('|');
            var toSimbols = to.Split('|');
            if (fromSimbols.Length != toSimbols.Length)
                throw new ArgumentException();

            var result = "";
            for (int i = 0; i < src.Length; i++)
            {
                for (int k = 3; k >= 0; k--)
                {
                    if (k == 0)
                    {
                        result += src[i];
                        break;
                    }
                    var currentSimbol = new string(src.Skip(i).Take(k).ToArray());
                    var fromIndex = Array.IndexOf(fromSimbols, currentSimbol);
                    if (fromIndex != -1)
                    {
                        result += toSimbols[fromIndex];
                        i += k - 1;
                        break;
                    }
                }
            }
            return result;
        }
        public static string TranslitToEnglish(string src)
        {
            return Translit(src, translitRussian, translitEnglish);
        }
        public static string TranslitToEnglish(char simbol)
        {
            return TranslitToEnglish(simbol.ToString());
        }
    }
}