﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace Library.Extensions
{
    public static class ExpressionExtensions
    {
        public static Expression<Func<T, bool>> Equal<T, TCompare>(this Expression<Func<T, TCompare>> selector, TCompare toCompare)
        {
            Expression<Func<TCompare>> closure = () => toCompare;
            return Expression.Lambda<Func<T, bool>>(
                Expression.Equal(selector.Body, closure.Body),
                selector.Parameters);
        }

        public static IEnumerable<MemberExpression> GetAllMembers<T, TCompare>(this Expression<Func<T, TCompare>> funcExpression)
        {
            var expression = funcExpression.Body;
            return GetAllMembers(expression);
        }

        private static IEnumerable<MemberExpression> GetAllMembers(Expression expression)
        {
            if (expression.NodeType == ExpressionType.Parameter)
            {
                return null;
            }
            if (expression.NodeType != ExpressionType.MemberAccess)
            {
                throw new Exception("The expression must contain only nodes of the type MemberAccess");
            }

            var memberExpression = expression as MemberExpression;
            var memberExpressions = new[] { memberExpression };

            return GetAllMembers(memberExpression.Expression)?.Concat(memberExpressions) ?? memberExpressions;
        }
    }
}