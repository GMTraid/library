﻿using Castle.MicroKernel.Context;
using Castle.MicroKernel.Lifestyle;
using Castle.MicroKernel.Lifestyle.Scoped;
using System.Web;

namespace Library.Extensions.WindsorContainer
{
    public class PerWebRequestScopeAccessor : IScopeAccessor
    {
        private readonly IScopeAccessor webRequestScopeAccessor = new WebRequestScopeAccessor();
        private readonly IScopeAccessor secondaryScopeAccessor;

        public PerWebRequestScopeAccessor(IScopeAccessor secondaryScopeAccessor)
        {
            this.secondaryScopeAccessor = secondaryScopeAccessor;
        }

        public ILifetimeScope GetScope(CreationContext context)
        {
            if (HttpContext.Current?.CurrentHandler != null)
            {
                return webRequestScopeAccessor.GetScope(context);
            }
            return secondaryScopeAccessor.GetScope(context);
        }

        public void Dispose()
        {
            webRequestScopeAccessor.Dispose();
            secondaryScopeAccessor.Dispose();
        }
    }
}