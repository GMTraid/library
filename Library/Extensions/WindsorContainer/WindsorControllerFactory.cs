﻿using Castle.MicroKernel;
using Castle.Windsor;
using System;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace Library.Extensions.WindsorContainer
{
    public class WindsorControllerFactory : DefaultControllerFactory
    {
        private readonly IKernel _kernel;
        private readonly IWindsorContainer _container;

        public WindsorControllerFactory(IWindsorContainer container)
        {
            _container = container;
            _kernel = _container.Kernel;
        }
        public override void ReleaseController(IController controller)
        {
            _kernel.ReleaseComponent(controller);
        }

        protected override IController GetControllerInstance(RequestContext requestContext, Type controllerType)
        {
            if (controllerType == null)
            {
                throw new HttpException(404, $"No controller found for '{requestContext.HttpContext.Request.Path}'");
            }
            return (IController)_kernel.Resolve(controllerType);
        }
    }
}