﻿using Castle.MicroKernel.Registration;
using Castle.Windsor;
using System;

namespace Library.Extensions.WindsorContainer
{
    public static class WindsorContainerExtension
    {
        public static void RegisterTransient<T, U>(this IWindsorContainer container) where U : T where T : class
        {
            container.Register(Component.For<T>().ImplementedBy<U>().LifestyleTransient());
        }

        public static void RegisterTransient<T, U>(this IWindsorContainer container, string name) where U : T where T : class
        {
            container.Register(Component.For<T>().ImplementedBy<U>().LifestyleTransient().Named(name));
        }

        public static void RegisterTransient(this IWindsorContainer container, Type serviceType, Type type)
        {
            container.Register(Component.For(serviceType).ImplementedBy(type).LifestyleTransient());
        }

        public static void RegisterTransient(this IWindsorContainer container, Type serviceType1, Type serviceType2, Type type)
        {
            container.Register(Component.For(serviceType1, serviceType2).ImplementedBy(type).LifestyleTransient());
        }

        public static void RegisterTransient<T1, T2, U>(this IWindsorContainer container) where U : T1, T2 where T1 : class
        {
            container.Register(Component.For<T1, T2>().ImplementedBy<U>().LifestyleTransient());
        }

        public static void RegisterPerWebRequest<T, U>(this IWindsorContainer container) where U : T where T : class
        {
            container.Register(Component.For<T>().ImplementedBy<U>().PerWebRequestPerScope());
        }

        public static void RegisterSingleton<T, U>(this IWindsorContainer container) where U : T where T : class
        {
            container.Register(Component.For<T>().ImplementedBy<U>().LifestyleSingleton());
        }

        public static T ResolveAndRelease<T>(this IWindsorContainer container)
        {
            var result = container.Resolve<T>();
            container.Release(result);
            return result;
        }

        public static object ResolveAndRelease(this IWindsorContainer container, Type type)
        {
            var result = container.Resolve(type);
            container.Release(result);
            return result;
        }

        public static ComponentRegistration<T> PerWebRequestPerScope<T>(this ComponentRegistration<T> componentRegistration) where T : class
        {
            return componentRegistration.LifeStyle.Scoped<PerWebRequestScopedScopeAccessor>();
        }
    }
}