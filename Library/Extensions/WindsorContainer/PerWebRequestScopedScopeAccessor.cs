﻿using Castle.MicroKernel.Lifestyle;

namespace Library.Extensions.WindsorContainer
{
    public class PerWebRequestScopedScopeAccessor : PerWebRequestScopeAccessor
    {
        public PerWebRequestScopedScopeAccessor() : base(new LifetimeScopeAccessor())
        {
        }
    }
}