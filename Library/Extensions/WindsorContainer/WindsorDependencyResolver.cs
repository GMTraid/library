﻿using Castle.MicroKernel.Lifestyle;
using Castle.Windsor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http.Dependencies;

namespace Library.Extensions.WindsorContainer
{
    public class WindsorDependencyResolver : IDependencyResolver
    {
        private readonly IWindsorContainer _container;

        public WindsorDependencyResolver(IWindsorContainer container)
        {
            _container = container;
        }

        public IDependencyScope BeginScope()
        {
            return new WindsorDependencyScope(_container);
        }

        public void Dispose()
        {
            _container.Dispose();
        }

        public object GetService(Type serviceType)
        {
            return _container.Kernel.HasComponent(serviceType) ? _container.Resolve(serviceType) : null;
        }

        public IEnumerable<object> GetServices(Type serviceType)
        {
            if (!_container.Kernel.HasComponent(serviceType))
            {
                return new object[0];
            }

            return _container.ResolveAll(serviceType).Cast<object>();
        }

        private class WindsorDependencyScope : IDependencyScope
        {
            private readonly IWindsorContainer _container;
            private readonly IDisposable _scope;

            public WindsorDependencyScope(IWindsorContainer container)
            {
                _container = container;
                _scope = _container.BeginScope();
            }

            public void Dispose()
            {
                _scope.Dispose();
            }

            public object GetService(Type serviceType)
            {
                if (_container.Kernel.HasComponent(serviceType))
                {
                    return _container.Resolve(serviceType);
                }
                else
                {
                    return null;
                }
            }

            public IEnumerable<object> GetServices(Type serviceType)
            {
                if (!_container.Kernel.HasComponent(serviceType))
                {
                    return new object[0];
                }

                return _container.ResolveAll(serviceType).Cast<object>();
            }
        }
    }
}