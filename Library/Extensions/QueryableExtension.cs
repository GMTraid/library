﻿using AutoMapper.QueryableExtensions;
using DelegateDecompiler.EntityFramework;
using Library.Domain.Models;
using Library.Domain.Params;
using Library.Extensions.Lambda;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Library.Extensions
{
    public static class QueryableExtension
    {
        public static ListResult GetListResult<T>(this IQueryable<T> query, DataTableLoadParams loadParams, object metaData = null)
        {
            var data = query
                .FilterOrderPaging(loadParams, out int totalCount)
                .ToArray();

            return new ListResult(data, totalCount, loadParams?.Draw ?? 0, metaData);
        }

        public static ListResult GetListResult<T, TModel>(this IQueryable<T> query, DataTableLoadParams loadParams)
        {
            var data = query
                .ProjectToIf<T, TModel>(typeof(T) != typeof(TModel))
                .FilterOrderPaging(loadParams, out int totalCount)
                .ToArray();

            return new ListResult(data, totalCount, loadParams?.Draw ?? 0);
        }

        public static async Task<ListResult> GetListResultAsync<T>(this IQueryable<T> query, DataTableLoadParams loadParams, T emptyItem = null)
            where T : class
        {
            var data = await query
                .FilterOrderPaging(loadParams, out int totalCount)
                .ToListAsync();
            return GenerateListResult(data, loadParams, totalCount, emptyItem);
        }

        public static async Task<ListResult> GetListResultAsync<T, TModel>(this IQueryable<T> query, DataTableLoadParams loadParams, TModel emptyItem = null)
            where T : class
            where TModel : class
        {
            var data = await query
                .ProjectToIf<T, TModel>()
                .FilterOrderPaging(loadParams, out int totalCount)
                .ToListAsync();
            return GenerateListResult(data, loadParams, totalCount, emptyItem);
        }
        private static ListResult GenerateListResult<T>(List<T> data, DataTableLoadParams loadParams, int totalCount, T emptyItem = null)
            where T : class
        {
            if (emptyItem != null)
            {
                if (loadParams.Start == 0)
                {
                    data.Insert(0, emptyItem);
                }
                totalCount++;
            }
            return new ListResult(data, totalCount, loadParams?.Draw ?? 0);
        }

        public static IQueryable<TDestination> ProjectToIf<T, TDestination>(this IQueryable<T> query, bool condition)
        {
            if (condition)
            {
                return query.ProjectTo<TDestination>().DecompileAsync();
            }
            return query.Cast<TDestination>().DecompileAsync();
        }

        public static IQueryable<TDestination> ProjectToIf<T, TDestination>(this IQueryable<T> query)
        {
            return query.ProjectToIf<T, TDestination>(typeof(T) != typeof(TDestination));
        }

        public static ListResult GetListResult(this IQueryable query, DataTableLoadParams loadParams)
        {
            var data = query.FilterOrderPaging(loadParams, out int totalCount);
            return new ListResult(data, totalCount, loadParams?.Draw ?? 0);
        }
    }
}