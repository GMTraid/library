﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Reflection;

namespace Library.Extensions
{
    public static class EnumExtensions
    {
        public static string GetDescription(this Enum value)
        {
            return value.GetAttribute<DescriptionAttribute>()?.Description;
        }

        public static string GetName(this Enum value)
        {
            return value.GetAttribute<DisplayAttribute>()?.GetName();
        }

        public static TAttribute GetAttribute<TAttribute>(this Enum enumValue)
            where TAttribute : Attribute
        {
            return enumValue.GetType()
                .GetMember(enumValue.ToString())
                .FirstOrDefault()
                ?.GetCustomAttribute<TAttribute>();
        }

        public static bool IsNullableEnum(this Type enumType)
        {
            return Nullable.GetUnderlyingType(enumType)?.IsEnum ?? false;
        }
    }
}