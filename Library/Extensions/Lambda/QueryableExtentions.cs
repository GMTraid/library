﻿using Library.Domain.Params;
using Library.Domain.Params.RepresentInfo;
using System;
using System.Linq;
using System.Reflection;

namespace Library.Extensions.Lambda
{
    public static class QueryableExtentions
    {
        private static MethodInfo GetGenericMethod(string methodName, Type parameterType, int lengthParams)
        {
            return typeof(QueryableGenericExtensions)
                .GetMethods()
                .Where(x => x.Name == methodName)
                .Where(x => x.GetParameters()
                    .Where(y => y.Position == 1 && y.ParameterType == parameterType).Any() &&
                    x.GetParameters().Count() == lengthParams)
                .Single();
        }

        public static IQueryable FilterOrderPaging(this IQueryable query, LoadParams loadParams, out int totalCount)
        {
            var genericType = query.ElementType;
            var methodInfo = GetGenericMethod(nameof(FilterOrderPaging), typeof(LoadParams), 3);
            var genericMethod = methodInfo.MakeGenericMethod(genericType);
            var parameters = new object[] { query, loadParams, null };
            var result = (IQueryable)genericMethod.Invoke(null, parameters);
            totalCount = (int)parameters[2];

            return result;
        }

        public static IQueryable Filter(this IQueryable query, FilterInfo[] filtersInfo)
        {
            var genericType = query.ElementType;
            var methodInfo = GetGenericMethod(nameof(Filter), typeof(FilterInfo[]), 2);
            var genericMethod = methodInfo.MakeGenericMethod(genericType);

            return (IQueryable)genericMethod.Invoke(null, new object[] { query, filtersInfo });
        }
    }
}