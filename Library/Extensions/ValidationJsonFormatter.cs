﻿using Library.Domain.Services.Exceptions;
using Newtonsoft.Json;
using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web.Http.ModelBinding;

namespace Library.Extensions
{
    public class ValidationJsonFormatter : JsonMediaTypeFormatter
    {
        private readonly ModelStateDictionary modeState;

        public ValidationJsonFormatter(ModelStateDictionary modeState)
        {
            this.modeState = modeState;
        }

        public override async Task<object> ReadFromStreamAsync(Type type, Stream readStream, HttpContent content, IFormatterLogger formatterLogger)
        {
            try
            {
                return await base.ReadFromStreamAsync(type, readStream, content, formatterLogger);
            }
            catch (JsonReaderException ex)
            {
                string pattern = @"\[[0-9]\]";
                Regex rgx = new Regex(pattern);
                string result = rgx.Replace(ex.Path, "");

                Type resultType = type;
                PropertyInfo property = null;
                foreach (var part in result.Split('.'))
                {
                    if (resultType.IsArray)
                    {
                        property = resultType.GetElementType().GetProperty(part);
                    }
                    else if (resultType.IsGenericType)
                    {
                        property = resultType.GetGenericArguments().Single().GetProperty(part);
                    }
                    else
                    {
                        property = resultType.GetProperty(part);
                    }
                    resultType = property.PropertyType;
                }
                if (property != null)
                {
                    var makeSureText = "Make sure that the value entered in the field is correct";

                    var displayAttribute = property.GetCustomAttribute<DisplayAttribute>();
                    if (displayAttribute != null)
                    {
                        modeState.AddModelError(ex.Path, new ApplicationValidationException($"{makeSureText} {displayAttribute.GetName()}"));
                        return null;
                    }

                    var descriptionAttribute = property.GetCustomAttribute<DescriptionAttribute>();
                    if (descriptionAttribute != null)
                    {
                        modeState.AddModelError(ex.Path, new ApplicationValidationException($"{makeSureText} {descriptionAttribute.Description}"));
                        return null;
                    }
                }

                throw;
            }
        }
    }
}