﻿using Library.Domain.Attributes;
using System.Collections.Generic;
using System.Linq;
using System.Web.Http.Controllers;

namespace Library.Extensions
{
    public static class HttpActionContextExtensions
    {
        public static bool IsInternalRequest(this HttpActionContext actionContext)
        {
            actionContext.Request.Headers.TryGetValues("internalrequest", out IEnumerable<string> isInternalRequestValues);
            bool.TryParse(isInternalRequestValues?.FirstOrDefault(), out bool isInternalRequest);
            return isInternalRequest &&
                (actionContext.ActionDescriptor.GetCustomAttributes<DigestAuthorizeAttribute>().Any()
              || actionContext.ControllerContext.ControllerDescriptor.GetCustomAttributes<DigestAuthorizeAttribute>().Any());
        }
    }
}