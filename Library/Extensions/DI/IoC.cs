﻿using Castle.Windsor;

namespace Library.Extensions
{
    public static class IoC
    {
        public static void Initialize(IWindsorContainer windsorContainer)
        {
            GlobalContainer = windsorContainer;
        }

        public static IWindsorContainer GlobalContainer { get; set; }
    }
}