﻿using Npgsql;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity.Core.Objects;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace Library.Extensions.EF
{
    public class NpgSqlQueryProvider : IQueryProvider
    {
        public bool CanDelete { get { return true; } }
        public bool CanUpdate { get { return true; } }
        public bool CanInsert { get { return true; } }
        public bool CanBulkUpdate { get { return true; } }

        public async Task InsertItemsAsync<T>(IEnumerable<T> items, string schema, string tableName, IList<ColumnMapping> properties, DbConnection storeConnection, int? batchSize)
        {
            int index = 0;
            int batchSizeValue = batchSize ?? 5000;
            int itemsCount = items.Count();
            bool itemsExists = itemsCount > 0;
            while (itemsExists)
            {
                var itemsPart = items.Skip(index * batchSizeValue).Take(batchSizeValue);

                using (var reader = new EFDataReader<T>(itemsPart, properties))
                {
                    if (!reader.HasRows) return;

                    var con = storeConnection as NpgsqlConnection;

                    if (con.State != System.Data.ConnectionState.Open)
                    {
                        await con.OpenAsync();
                    }
                    List<ColumnMapping> actualProperties = properties.Where(x => !x.Autoincrement).ToList();
                    string columns = string.Join(",", actualProperties.Select(x => $"\"{x.NameInDatabase}\""));
                    string tableWithSchema = schema.HasValue() ? $"{schema}.\"{tableName}\"" : tableName;
                    using (var writer = con.BeginBinaryImport($"COPY {tableWithSchema} ({columns}) FROM STDIN (FORMAT BINARY)"))
                    {
                        while (await reader.ReadAsync())
                        {
                            writer.StartRow();
                            for (int i = 0; i < actualProperties.Count; i++)
                            {
                                var property = actualProperties[i];
                                var propertyValue = reader.GetPropertyValue(property.NameOnObject);
                                writer.Write(propertyValue);
                            }
                        }

                        //await writer.CommitAsync();
                    }
                }
                index++;
                itemsExists = index * batchSizeValue < itemsCount;
            }
        }

        private async Task CommitBinaryImportAsync(NpgsqlBinaryImporter writer)
        {
            var writerType = writer.GetType();
            var writerTypeAssembly = Assembly.GetAssembly(writerType);

            var nonPublicInstance = BindingFlags.NonPublic | BindingFlags.Instance;
            var nonPublicInstanceDeclaredOnly = nonPublicInstance | BindingFlags.DeclaredOnly;

            var methodWriteTrailer = writerType.GetMethod("WriteTrailer", nonPublicInstance);

            var propertyNumColumns = writerType.GetProperty("NumColumns", nonPublicInstance);
            var fieldColumn = writerType.GetField("_column", nonPublicInstance);
            var fieldBuf = writerType.GetField("_buf", nonPublicInstance);
            var fieldConnector = writerType.GetField("_connector", nonPublicInstance);

            var typeNpgsqlWriteBuffer = writerTypeAssembly.GetType("Npgsql.WriteBuffer");
            var buf = fieldBuf.GetValue(writer);

            var column = (short)fieldColumn.GetValue(writer);
            if (column != -1 && column != (int)propertyNumColumns.GetValue(writer))
            {
                var methodBufClear = typeNpgsqlWriteBuffer.GetMethod("Clear", nonPublicInstanceDeclaredOnly, null, new Type[] { }, null);
                methodBufClear.Invoke(buf, new object[] { });
                //writer.Cancel();
                throw new InvalidOperationException("Binary importer closed in the middle of a row, cancelling import.");
            }

            methodWriteTrailer.Invoke(writer, new object[] { });

            var methodBufFlush = typeNpgsqlWriteBuffer.GetMethod("Flush", nonPublicInstanceDeclaredOnly, null, new Type[] { }, null);
            var methodBufEndCopyMode = typeNpgsqlWriteBuffer.GetMethod("EndCopyMode", nonPublicInstance);

            methodBufFlush.Invoke(buf, new object[] { });
            methodBufEndCopyMode.Invoke(buf, new object[] { });

            var connector = fieldConnector.GetValue(writer);
            var connectorType = connector.GetType();

            var typeCopyDoneMessage = writerTypeAssembly.GetType("Npgsql.BackendMessages.CopyDoneMessage");
            var fieldCopyDoneMessageInstance = typeCopyDoneMessage.GetField("Instance", BindingFlags.NonPublic | BindingFlags.Static);

            var methodConnectorSendMessage = connectorType.GetMethod("SendMessage", nonPublicInstance);
            methodConnectorSendMessage.Invoke(connector, new object[] { fieldCopyDoneMessageInstance.GetValue(null) });

            try
            {
                var methodConnectorReadExpecting = connectorType.GetMethod("ReadExpecting", nonPublicInstanceDeclaredOnly, null, new Type[] { typeof(bool) }, null);

                var typeCommandCompleteMessage = writerTypeAssembly.GetType("Npgsql.BackendMessages.CommandCompleteMessage");
                var methodConnectorReadExpectingCommandCompleteMessage = methodConnectorReadExpecting.MakeGenericMethod(typeCommandCompleteMessage);

                var typeReadyForQueryMessage = writerTypeAssembly.GetType("Npgsql.BackendMessages.ReadyForQueryMessage");
                var methodConnectorReadExpectingReadyForQueryMessage = methodConnectorReadExpecting.MakeGenericMethod(typeReadyForQueryMessage);

                var task1 = (dynamic)methodConnectorReadExpectingCommandCompleteMessage.Invoke(connector, new object[] { true });
                var task2 = (dynamic)methodConnectorReadExpectingReadyForQueryMessage.Invoke(connector, new object[] { true });
            }
            finally
            {
                var methodEndUserAction = connectorType.GetMethod("EndUserAction", nonPublicInstanceDeclaredOnly);
                methodEndUserAction.Invoke(connector, new object[] { });
            }
        }

        #region NotImplementended
        public bool CanHandle(DbConnection storeConnection)
        {
            return storeConnection is NpgsqlConnection;
        }

        public string GetDeleteQuery(QueryInformation queryInformation)
        {
            throw new NotImplementedException();
        }

        public string GetUpdateQuery(QueryInformation predicateQueryInfo, QueryInformation modificationQueryInfo)
        {
            throw new NotImplementedException();
        }

        public void UpdateItems<T>(IEnumerable<T> items, string schema, string tableName, IList<ColumnMapping> properties, DbConnection storeConnection, int? batchSize, UpdateSpecification<T> updateSpecification)
        {
            throw new NotImplementedException();
        }

        public QueryInformation GetQueryInformation<T>(ObjectQuery<T> query)
        {
            throw new NotImplementedException();
        }

        #endregion
    }
}