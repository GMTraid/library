﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;

namespace Library.Extensions
{
    public static class StringExtensions
    {
        public static bool HasValue(this string str)
        {
            return !string.IsNullOrEmpty(str);
        }

        public static List<long> ToLongList(this string str, char separator = ',')
        {
            var result = new List<long>();
            if (str.HasValue())
            {
                result = str.Split(new[] { separator }, StringSplitOptions.RemoveEmptyEntries)
                    .Select(long.Parse)
                    .ToList();
            }
            return result;
        }

        public static double ParseDouble(this string str)
        {
            if (!str.HasValue()) return 0;

            double.TryParse(str.Replace(',', '.'), NumberStyles.Any, CultureInfo.InvariantCulture, out double result);

            return result;
        }

        public static string ReplaceSameLatinLettersToCyrillic(this string cellValue)
        {
            var strBuilder = new StringBuilder(cellValue);

            return strBuilder
                .Replace('a', 'а')
                .Replace('e', 'е')
                .Replace('k', 'к')
                .Replace('o', 'о')
                .Replace('p', 'р')
                .Replace('c', 'с')
                .Replace('y', 'у')
                .Replace('x', 'х')
                .ToString();
        }

        public static string GetRandomString(this string builder)
        {
            StringBuilder stringBuilder = new StringBuilder(builder);
            Random rnd = new Random();
            char chars;
            for (int i = 0; i < rnd.Next(rnd.Next(0, 25), 50); i++)
            {
                chars = Convert.ToChar(Convert.ToInt32(Math.Floor(26 * rnd.NextDouble() + 65)));
                stringBuilder.Append(chars);
            }
            return stringBuilder.ToString();
        }
    }
}
