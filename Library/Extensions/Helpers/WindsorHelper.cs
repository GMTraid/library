﻿using Castle.MicroKernel.Resolvers.SpecializedResolvers;
using Castle.Windsor;

namespace Library.Extensions.Helpers
{
    public static class WindsorHelper
    {
        public static IWindsorContainer InitContainer()
        {
            var container = new Castle.Windsor.WindsorContainer();
            container.Kernel.Resolver.AddSubResolver(new CollectionResolver(container.Kernel, true));
            return container;
        }

        public static IWindsorContainer InitDI()
        {
            var container = InitContainer();
            WindsorMvcHelper.InitDI(container);
            WindsorWebApiHelper.InitDI(container);
            return container;
        }
    }
}