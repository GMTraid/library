﻿using Library.Domain.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Security.Principal;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace Library.Extensions.Helpers
{
    /// <summary>
    /// Хелпер для оповещений о деплое и ошибках на сервере
    /// </summary>
    public class DeployNotificationHelper
    {
        public virtual string Site
        {
            get { return HttpContext.Current?.Request?.Url?.Host; }
        }

        /// <summary>
        /// Уведомление о деплое
        /// </summary>
        /// <returns></returns>
        public bool Deploy()
        {
            return MakeRequest(null, null, "deploy");
        }

        /// <summary>
        /// Уведомление об ошибке
        /// </summary>
        /// <param name="text">Текст ошибки</param>
        /// <returns></returns>
        public bool Exception(Exception ex)
        {
            return Exception(ex?.Message, ex?.ToString());
        }

        /// <summary>
        /// Уведомление об ошибке
        /// </summary>
        /// <param name="text">Текст ошибки</param>
        /// <returns></returns>
        public bool Exception(Exception ex, HttpRequest request)
        {
            return Exception(ex, new HttpRequestWrapper(request));
        }

        /// <summary>
        /// Уведомление об ошибке
        /// </summary>
        /// <param name="text">Текст ошибки</param>
        /// <returns></returns>
        public bool Exception(Exception ex, HttpRequestBase request)
        {
            if (request == null)
                return Exception(ex);

            var userName = request.RequestContext?.HttpContext?.User?.Identity?.Name;

            var sb = new StringBuilder();

            sb.AppendLine($"User = {userName}");
            sb.AppendLine($"RawUrl = {request.RawUrl}");
            sb.AppendLine($"Url = {request.Url}");
            sb.AppendLine($"UrlReferrer = {request.UrlReferrer}");

            sb.AppendLine("==============START PARAMS==============");
            for (int i = 0; i < request.Params.Count; i++)
            {
                sb.AppendLine($"{request.Params.GetKey(i)}={request.Params.Get(i)}");
            }
            sb.AppendLine("==============END PARAMS================");

            var databaseLogger = DependencyResolver.Current.GetService<MemoryLogger>();
            if (databaseLogger != null && databaseLogger.Lines != null && databaseLogger.Lines.Any())
            {
                sb.AppendLine("===========START DATABASE LOG===========");
                foreach (var item in databaseLogger.Lines)
                {
                    sb.AppendLine(item);
                }
                sb.AppendLine("===========END DATABASE LOG=============");
            }
            sb.AppendLine(ex?.ToString());

            return Exception(ex?.Message, sb.ToString());
        }

        /// <summary>
        /// Уведомление об ошибке
        /// </summary>
        /// <returns></returns>
        public bool Exception(Exception ex, IPrincipal principal, HttpRequestMessage requestMessage, string body = null)
        {
            if (principal == null)
                return Exception(ex);

            var userName = principal?.Identity?.Name;

            var sb = new StringBuilder();

            sb.AppendLine($"User = {userName}");
            sb.AppendLine($"RawUrl = {requestMessage.RequestUri.PathAndQuery}");
            sb.AppendLine($"Url = {requestMessage.RequestUri.AbsoluteUri}");

            if (body != null)
            {
                sb.AppendLine("==================BODY==================");
                sb.AppendLine(body);
            }

            if (requestMessage.Headers != null || requestMessage.Content?.Headers != null)
            {
                sb.AppendLine("================HEADERS=================");
                if (requestMessage.Headers != null)
                {
                    sb.AppendLine(requestMessage.Headers.ToString());
                }
                if (requestMessage.Content?.Headers != null)
                {
                    sb.AppendLine(requestMessage.Content.Headers.ToString());
                }
            }

            var databaseLogger = DependencyResolver.Current.GetService<MemoryLogger>();
            if (databaseLogger != null && databaseLogger.Lines != null && databaseLogger.Lines.Any())
            {
                sb.AppendLine("===========START DATABASE LOG===========");
                foreach (var item in databaseLogger.Lines)
                {
                    sb.AppendLine(item);
                }
                sb.AppendLine("===========END DATABASE LOG=============");
            }
            if (ex != null)
            {
                sb.AppendLine("================EXCEPTION===============");
                sb.AppendLine(ex.ToString());
            }

            return Exception(ex?.Message, sb.ToString());
        }

        /// <summary>
        /// Уведомление об ошибке
        /// </summary>
        /// <param name="message">Текст ошибки</param>
        /// <returns></returns>
        public bool Exception(string message, string stackTrace)
        {
            return MakeRequest(message, stackTrace, "exception");
        }

        private bool MakeRequest(string text, string text2, string type)
        {
            var url = "";

            var dict = new Dictionary<string, string>()
                {
                    {"text", text},
                    {"type", type},
                    {"site", Site},
                };
            if (!string.IsNullOrWhiteSpace(text2))
            {
                var chunks = text2
                    .Select((x, index) => new { item = x, index })
                    .GroupBy(x => x.index / 60000, x => x.item)
                    .Select(x => new string(x.ToArray()))
                    .ToArray();

                for (int i = 0; i < chunks.Length; i++)
                {
                    dict.Add($"text2[{i}]", chunks[i]);
                }
            }

            var content = new FormUrlEncodedContent(dict);

            new HttpClient().PostAsync(url, content).ConfigureAwait(false);

            return true;
        }
    }
}