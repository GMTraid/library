﻿using Castle.Windsor;
using Library.Extensions.WindsorContainer;
using System.Web.Http;
using System.Web.Mvc;

namespace Library.Extensions.Helpers
{
    public static class WindsorMvcHelper
    {
        public static void SetDependencyResolver(IWindsorContainer container, HttpConfiguration configuration)
        {
            configuration.DependencyResolver = new WindsorDependencyResolver(container);
        }

        public static void SetControllerFactory(IWindsorContainer container)
        {
            ControllerBuilder.Current.SetControllerFactory(new WindsorControllerFactory(container));
        }

        public static void InitDI(IWindsorContainer container)
        {
            SetDependencyResolver(container, GlobalConfiguration.Configuration);
            SetControllerFactory(container);
        }
    }
}