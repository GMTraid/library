﻿using Library.Database.Entities.Content;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Library.Extensions.Helpers
{
    public static class DataHelpers
    {
        public static IEnumerable<string> HumanizeBusinessHours(IEnumerable<BranchHours> branchHours)
        {
            var hours = new List<string>();

            LibraryBranch branch = new LibraryBranch();

            try
            {
                foreach (var time in branchHours)
                {
                    var day = HumanizeDayOfWeek(Convert.ToInt32(time.DayOfWeek));
                    var openTime = HumanizeTime(Convert.ToInt32(time.OpenTime));
                    var closeTime = HumanizeTime(Convert.ToInt32(time.CloseTime));
                    var timeEntry = $"{day} {openTime} to {closeTime}";
                    hours.Add(timeEntry);
                }
            }
            catch
            {
                hours.Add(DateTime.Now.ToShortTimeString());
            }

            return hours;
        }

        private static string HumanizeDayOfWeek(int number)
        {
            return Enum.GetName(typeof(DayOfWeek), number);
        }

        private static string HumanizeTime(int time)
        {
            var result = TimeSpan.FromHours(time);
            return result.ToString("hh':'mm");
        }
    }
}