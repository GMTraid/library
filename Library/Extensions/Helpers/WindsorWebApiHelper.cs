﻿using Castle.Windsor;
using Library.Extensions.WindsorContainer;
using System.Web.Http;
using System.Web.Http.Dispatcher;

namespace Library.Extensions.Helpers
{
    public static class WindsorWebApiHelper
    {
        public static void RegisterControllerActivator(IWindsorContainer container)
        {
            GlobalConfiguration.Configuration.Services.Replace(typeof(IHttpControllerActivator), new WindsorControllerActivator(container));
        }

        public static void InitDI(IWindsorContainer container)
        {
            RegisterControllerActivator(container);
        }
    }
}