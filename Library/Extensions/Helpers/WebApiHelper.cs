﻿using Library.Models;
using Newtonsoft.Json;
using System;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;

namespace Library.Extensions
{
    public static class WebApiHelper
    {
        public const string AuthorizationSchema = "Digest";

        public static bool TryParseAuthorizationHeader(string headerValue, out AuthorizationHeader authorizationHeader)
        {
            var headerValues = (headerValue ?? string.Empty).Split(new[] { ' ' }, StringSplitOptions.RemoveEmptyEntries)
                .Select(x => x.Trim())
                .ToArray();

            if (headerValues.Count() == 2)
            {
                authorizationHeader = new AuthorizationHeader
                {
                    Schema = headerValues[0],
                    Parameter = headerValues[1],
                };
                return true;
            }
            authorizationHeader = null;
            return false;
        }

        public static bool TryParseAuthorizationParameter(string parameter, out AuthorizationParametr authorizationParametr)
        {
            var authData = parameter.Split(':');
            if (authData.Count() == 3)
            {
                var clientNumber = authData[0];
                var stringNonce = authData[1];
                var hash = authData[2];

                if (long.TryParse(stringNonce, out long nonce))
                {
                    authorizationParametr = new AuthorizationParametr
                    {
                        ClientNumber = clientNumber,
                        Hash = hash,
                        Nonce = nonce
                    };
                    return true;
                }
            }

            authorizationParametr = null;
            return false;
        }

        public static bool TryParseAuthorizationParameterByHeader(string headerValue, out AuthorizationParametr authorizationParametr)
        {
            if (TryParseAuthorizationHeader(headerValue, out AuthorizationHeader authorizationHeader)
                && authorizationHeader.Schema == WebApiHelper.AuthorizationSchema
                && TryParseAuthorizationParameter(authorizationHeader.Parameter, out authorizationParametr))
            {
                return true;
            }

            authorizationParametr = null;
            return false;
        }

        public static string GetHash(string clientNumber, string secret, long nonce, string data)
        {
            var str = $"{clientNumber}{data}{secret}{nonce}";
            var bytes = Encoding.UTF8.GetBytes(str);
            var shaM = new SHA512Managed();
            var hash = shaM.ComputeHash(bytes);
            var computedHash = hash.Aggregate(string.Empty, (current, b) => current + b.ToString("X2"));
            return computedHash;
        }

        public static string GetAuthorizationHeaderString(string clientNumber, long nonce, string hash)
        {
            var result = $"{AuthorizationSchema} {clientNumber}:{nonce}:{hash}";
            return result;
        }

        public static AuthorizationHeader GetAuthorizationHeaderParameter(string clientNumber, long nonce, string hash)
        {
            var parameter = $"{clientNumber}:{nonce}:{hash}";
            return new AuthorizationHeader { Schema = AuthorizationSchema, Parameter = parameter };
        }

        public static void SetAuthorizationHeaderParameterToHeader(WebHeaderCollection headerCollection, string clientNumber, long nonce, string hash)
        {
            var parameter = GetAuthorizationHeaderString(clientNumber, nonce, hash);
            SetAuthorizationHeaderParameterToHeader(headerCollection, parameter);
        }

        public static void SetAuthorizationHeaderParameterToHeader(WebHeaderCollection headerCollection, string parameter)
        {
            headerCollection.Add(HttpRequestHeader.Authorization, parameter);
        }

        public static JsonSerializerSettings GetJsonSerializerSettings()
        {
            return JsonConvert.DefaultSettings?.Invoke() ?? new JsonSerializerSettings();
        }
    }
}