﻿using System;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Http.Filters;

namespace Library.Extensions.Helpers
{
    public class NotificationHelper : DeployNotificationHelper
    {
        public static string StaticSite { get; set; }

        public override string Site
        {
            get
            {
                return OverrideSite ?? StaticSite ?? (StaticSite = base.Site);
            }
        }

        protected string OverrideSite { get; set; }

        public static async Task SendExceptionAsync(Exception ex, HttpActionExecutedContext context)
        {
            var helper = new NotificationHelper();
            await helper.ExceptionAsync(ex, context);
        }

        public static void SendException(string message, string stackTrace)
        {
            SendException(message, stackTrace, null);
        }

        public static void SendException(string message, string stackTrace, string site)
        {
            var helper = new NotificationHelper() { OverrideSite = site };
            helper.Exception(message, stackTrace);
        }

        public async Task ExceptionAsync(Exception error, HttpActionExecutedContext context)
        {
            string body = null;
            if (context.Request.Content.IsMimeMultipartContent())
            {
                body = await GetMimeMultipartContent(context, body);
            }
            else
            {
                body = await context.Request.Content.ReadAsStringAsync();
            }
            Exception(error, context.ActionContext.RequestContext.Principal, context.ActionContext.Request, body);
        }

        private static async Task<string> GetMimeMultipartContent(HttpActionExecutedContext context, string body)
        {
            var stream = await context.Request.Content.ReadAsStreamAsync();
            stream.Seek(0, System.IO.SeekOrigin.Begin);
            var bodyBuilder = new StringBuilder();
            var provider = await context.Request.Content.ReadAsMultipartAsync();

            bodyBuilder.AppendLine("----boundary--");
            foreach (var part in provider.Contents)
            {
                bodyBuilder.AppendLine(part.Headers.ToString());
                if (part.Headers.ContentDisposition.FileName == null)
                {
                    var partBody = await part.ReadAsStringAsync();
                    bodyBuilder.AppendLine(partBody);
                }
                bodyBuilder.AppendLine("----boundary");
            }
            body = bodyBuilder.ToString();
            return body;
        }
    }
}