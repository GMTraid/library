﻿using System;

namespace Library.Extensions
{
    public static class IntExtensions
    {
        /// <summary>
        /// Random value for DeweyIndex
        /// </summary>
        /// <param name="DeweyIndex"></param>
        /// <returns></returns>
        public static string GetRandomDeweyIndex(this string count)
        {
            Random rnd = new Random();
            return String.Format("{0}-{1}" , rnd.Next(0 , 100) , rnd.Next(0 ,100));
        }
    }
}