﻿using Library.Extensions;

namespace Library.Domain.Attributes
{

    public class DigestAuthorizeAttribute : GenericDigestAuthenticationAttribute
    {
        public DigestAuthorizeAttribute() : base(typeof(IApiClientManager))
        {
        }

        protected override IApiClientManager GetApiClientManager()
        {
            return IoC.GlobalContainer.Resolve<IApiClientManager>();
        }
    }

}