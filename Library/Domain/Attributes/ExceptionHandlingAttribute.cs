﻿using Library.Domain.Services.Exceptions;
using Library.Extensions.Helpers;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace Library.Domain.Attributes
{
    public class ExceptionHandlingAttribute : ActionFilterAttribute, IActionFilter, IExceptionFilter
    {
        public override async Task OnActionExecutingAsync(HttpActionContext actionContext, CancellationToken cancellationToken)
        {
            if (!actionContext.ModelState.IsValid)
            {
                actionContext.Response = actionContext.Request.CreateErrorResponse(HttpStatusCode.BadRequest, actionContext.ModelState);
                return;
            }
            await actionContext.Request.Content.LoadIntoBufferAsync();
        }

        public async Task ExecuteExceptionFilterAsync(HttpActionExecutedContext actionExecutedContext, CancellationToken cancellationToken)
        {
            await OnExceptionAsync(actionExecutedContext, cancellationToken);
        }

        public virtual async Task OnExceptionAsync(HttpActionExecutedContext context, CancellationToken cancellationToken)
        {
            if (cancellationToken.IsCancellationRequested)
            {
                return;
            }
            var error = context.Exception;
            if (error != null)
            {
                if (error is ApplicationValidationException)
                {
                    context.Response = context.Request.CreateErrorResponse(HttpStatusCode.BadRequest, error.Message);
                    NLog.LogManager.GetCurrentClassLogger().Debug(error);
                    return;
                }
                var helper = new NotificationHelper();
                var httpError = error as HttpException;
                if (httpError?.GetHttpCode() != 404)
                {
                    if (helper.Site != "localhost")
                    {
                        await helper.ExceptionAsync(error, context);
                    }
                }
            }
        }
    }
}