﻿using System.Collections;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace Library.Domain.Attributes
{
    public class RegExListAttribute : ValidationAttribute
    {
        private readonly string _regEx;
        public RegExListAttribute(string regEx)
        {
            _regEx = regEx;
        }

        public override bool IsValid(object value)
        {

            if (!(value is IList list)) return false;

            var regex = new Regex(_regEx);
            foreach (string listItem in list)
            {
                if (!regex.IsMatch(listItem))
                {
                    return false;
                }
            }
            return true;
        }
    }
}