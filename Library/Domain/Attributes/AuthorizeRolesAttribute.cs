﻿using System.Linq;
using Library.Extensions;
using System.Web.Http;
using System.Web.Http.Controllers;

namespace Library.Domain.Attributes
{
    public class AuthorizeRolesAttribute : AuthorizeAttribute
    {
        public AuthorizeRolesAttribute(params string[] roles) : base()
        {
            Roles = string.Join(",", roles);
        }

        public override void OnAuthorization(HttpActionContext actionContext)
        {
            if (actionContext.IsInternalRequest())
                return;

            if (actionContext.ActionDescriptor.GetCustomAttributes<DigestAuthorizeAttribute>().Any())
                return;

            base.OnAuthorization(actionContext);
        }
    }
}