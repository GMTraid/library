﻿using Library.Extensions;
using Library.Models;
using System;
using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Web.Http.Results;
using System.Web.Mvc;
using System.Web.Mvc.Filters;

namespace Library.Domain.Attributes
{
    public class GenericDigestAuthenticationAttribute : BaseDigestAuthenticationAttribute, System.Web.Http.Filters.IAuthenticationFilter, System.Web.Mvc.Filters.IAuthenticationFilter, System.Web.Http.Filters.IActionFilter, System.Web.Mvc.IResultFilter
    {
        public GenericDigestAuthenticationAttribute(Type clientManagerType) : base(clientManagerType) { }

        /// <summary> Http </summary>
        public async Task AuthenticateAsync(HttpAuthenticationContext context, CancellationToken cancellationToken)
        {
            context.Principal = null;
            var body = await context.ActionContext.Request.Content.ReadAsStringAsync();
            var authorization = context.Request.Headers.Authorization;

            if (authorization != null)
            {
                context.Principal = GetPrincipal(authorization.Scheme, authorization.Parameter, body);
            }
            if (context.Principal == null)
            {
                context.ErrorResult = new UnauthorizedResult(new AuthenticationHeaderValue[] { new AuthenticationHeaderValue(WebApiHelper.AuthorizationSchema) }, context.Request);
            }
        }

        public Task ChallengeAsync(HttpAuthenticationChallengeContext context, CancellationToken cancellationToken)
        {
            return Task.FromResult<object>(null);
        }

        public Task<HttpResponseMessage> ExecuteAuthorizationFilterAsync(HttpActionContext actionContext, CancellationToken cancellationToken, Func<Task<HttpResponseMessage>> continuation)
        {
            throw new NotImplementedException();
        }

        /// <summary> Mvc  </summary>
        public void OnAuthentication(AuthenticationContext filterContext)
        {
            var authorizationHeaderString = filterContext.HttpContext.Request.Headers["Authorization"];

            AuthorizationHeader authorizationHeader;
            if (WebApiHelper.TryParseAuthorizationHeader(authorizationHeaderString, out authorizationHeader))
            {
                var body = GetAuthenticationContextBody();
                filterContext.HttpContext.User = GetPrincipal(authorizationHeader.Schema, authorizationHeader.Parameter, body);
            }

            if ((!filterContext.HttpContext.User?.Identity.IsAuthenticated) ?? true)
            {
                filterContext.Result = new HttpUnauthorizedResult();
                filterContext.HttpContext.Response.StatusCode = 401;
                filterContext.HttpContext.Response.SuppressFormsAuthenticationRedirect = true;
            }
        }

        private string GetAuthenticationContextBody()
        {
            var body = new StreamReader(HttpContext.Current.Request.InputStream).ReadToEnd();
            HttpContext.Current.Request.InputStream.Position = 0;
            return body;
        }

        public void OnAuthenticationChallenge(AuthenticationChallengeContext filterContext)
        {
        }

        public async Task<HttpResponseMessage> ExecuteActionFilterAsync(HttpActionContext actionContext, CancellationToken cancellationToken, Func<Task<HttpResponseMessage>> continuation)
        {
            HttpResponseMessage result = await continuation();

            var identity = actionContext.RequestContext?.Principal?.Identity as DigestIdentity;
            if (identity != null)
            {
                var body = result.Content.ReadAsStringAsync().Result;
                var hash = WebApiHelper.GetHash(identity.Name, identity.Secret, identity.Nonce, body);
                var header = WebApiHelper.GetAuthorizationHeaderString(identity.Name, identity.Nonce, hash);

                result.Headers.Add("WWW-Authenticate", header);
            }
            return result;
        }

        public void OnResultExecuting(ResultExecutingContext filterContext)
        {

        }

        public void OnResultExecuted(ResultExecutedContext filterContext)
        {
            var identity = filterContext.HttpContext.User?.Identity as DigestIdentity;
            if (identity != null)
            {
                var result = filterContext.Result as WebApiJsonNetResult;
                if (result != null)
                {
                    var body = result.StringData;
                    var hash = WebApiHelper.GetHash(identity.Name, identity.Secret, identity.Nonce, body);
                    var header = WebApiHelper.GetAuthorizationHeaderString(identity.Name, identity.Nonce, hash);

                    filterContext.HttpContext.Response.Headers.Add("WWW-Authenticate", header);
                }
            }
        }
    }
}