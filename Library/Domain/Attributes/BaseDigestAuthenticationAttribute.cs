﻿using Library.Extensions;
using Library.Models;
using System;
using System.Security.Principal;
using System.Web.Mvc;

namespace Library.Domain.Attributes
{
    public abstract class BaseDigestAuthenticationAttribute : FilterAttribute
    {
        protected Type ClientManagerType { get; set; }

        public BaseDigestAuthenticationAttribute(Type clientManagerType) : base()
        {
            var interfaceType = typeof(IApiClientManager);
            if (!interfaceType.IsAssignableFrom(clientManagerType))
            {
                throw new ArgumentException($"A type that implements the interface is required {interfaceType.Name}", nameof(clientManagerType));
            }
            ClientManagerType = clientManagerType;
        }

        protected virtual IApiClientManager GetApiClientManager()
        {
            return (IApiClientManager)DependencyResolver.Current.GetService(ClientManagerType);
        }

        private bool TryAuth(AuthorizationParametr authorizationParametr, string data)
        {
            return TryAuth(authorizationParametr, data, out BaseApiClient client);
        }

        private bool TryAuth(AuthorizationParametr authorizationParametr, string data, out BaseApiClient client)
        {
            var hash = authorizationParametr.Hash;
            var nonce = authorizationParametr.Nonce;
            var clientNumber = authorizationParametr.ClientNumber;

            var clientManager = GetApiClientManager();
            client = clientManager.GetClient(clientNumber);

            if (client == null || client.LastNonce >= nonce)
            {
                return false;
            }

            var currentHash = WebApiHelper.GetHash(clientNumber, client.SecretKey, nonce, data);
            if (currentHash.ToLower() != hash.ToLower())
            {
                return false;
            }

            clientManager.SetLastNonce(client, nonce);

            return true;
        }

        protected IPrincipal GetPrincipal(string authorizationSchema, string authorizationParams, string body)
        {
            if (authorizationSchema == WebApiHelper.AuthorizationSchema)
            {
                if (WebApiHelper.TryParseAuthorizationParameter(authorizationParams, out AuthorizationParametr authorizationParametr))
                {
                    if (TryAuth(authorizationParametr, body, out BaseApiClient client))
                    {
                        return new GenericPrincipal(new DigestIdentity(client.ClientNumber, authorizationParametr.Nonce, client.SecretKey), new string[0]);
                    }
                }
            }
            return null;
        }
    }
}