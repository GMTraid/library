﻿using System;
using System.Linq;

namespace Library.Domain.Extensions
{
    public static class PasswordHelper
    {
        public static string GeneratePassword(int commonLength, PasswordChars defaultCharsType, PasswordChars requireCharsType = PasswordChars.None)
        {
            const string digit = "0123456789";
            const string lowercase = "abcdefghijklmnopqrstuvwxyz";
            const string uppercase = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
            const string nonLetterOrDigit = @"~`!@#$%^&*()-_+={}[]|\;:<>,./?";

            if (defaultCharsType == PasswordChars.None)
            {
                throw new ArgumentException($"Argument {nameof(defaultCharsType)} must be defined.");
            }

            var random = new Random(new object().GetHashCode());

            var requiredCharList = new Tuple<bool, string>[]
            {
                Tuple.Create(requireCharsType.HasFlag(PasswordChars.Digit), digit),
                Tuple.Create(requireCharsType.HasFlag(PasswordChars.Lowercase), lowercase),
                Tuple.Create(requireCharsType.HasFlag(PasswordChars.Uppercase), uppercase),
                Tuple.Create(requireCharsType.HasFlag(PasswordChars.NonLetterOrDigit), nonLetterOrDigit)
            };

            var requiredChars = string.Join("", requiredCharList
                .Where(x => x.Item1)
                .Select(x => x.Item2[random.Next(x.Item2.Length)]));

            var defaultChars = string.Format("{0}{1}{2}{3}",
                defaultCharsType.HasFlag(PasswordChars.Digit) ? digit : "",
                defaultCharsType.HasFlag(PasswordChars.Lowercase) ? lowercase : "",
                defaultCharsType.HasFlag(PasswordChars.Uppercase) ? uppercase : "",
                defaultCharsType.HasFlag(PasswordChars.NonLetterOrDigit) ? nonLetterOrDigit : "");

            var passwordLength = commonLength - requiredChars.Length;
            if (passwordLength < 0)
            {
                passwordLength = 0;
            }
            var result = requiredChars + string.Join("", Enumerable.Range(0, passwordLength)
                .Select(x => defaultChars[random.Next(defaultChars.Length)]));
            return result;
        }
    }

    [Flags]
    public enum PasswordChars
    {
        None = 0,
        Digit = 1,
        Lowercase = 2,
        Uppercase = 4,
        NonLetterOrDigit = 8,
    }
}