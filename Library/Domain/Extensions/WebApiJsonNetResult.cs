﻿using Library.Extensions;
using Newtonsoft.Json;
using System;
using System.IO;
using System.Web.Mvc;

namespace Library.Models
{
    public class WebApiJsonNetResult : JsonResult
    {
        public JsonSerializerSettings SerializerSettings { get; set; }
        public Formatting Formatting { get; set; }

        public string StringData { get; set; }

        public WebApiJsonNetResult()
        {
            Formatting = Formatting.None;
            SerializerSettings = WebApiHelper.GetJsonSerializerSettings();
            JsonRequestBehavior = JsonRequestBehavior.DenyGet;
        }

        public WebApiJsonNetResult(object data) : this()
        {
            Data = data;
        }

        public override void ExecuteResult(ControllerContext context)
        {
            if (context == null)
                throw new ArgumentNullException(nameof(context));

            var response = context.HttpContext.Response;
            response.ContentType = !string.IsNullOrEmpty(ContentType) ? ContentType : "application/json";

            if (ContentEncoding != null)
                response.ContentEncoding = ContentEncoding;

            if (Data != null)
            {
                using (var textWriter = new StringWriter())
                {
                    using (var writer = new JsonTextWriter(textWriter) { Formatting = Formatting })
                    {
                        var serializer = JsonSerializer.Create(SerializerSettings);
                        serializer.Serialize(writer, Data);

                        StringData = textWriter.ToString();
                        response.Output.Write(StringData);
                        writer.Flush();
                    }
                }
            }
        }
    }
}