﻿namespace Library.Domain.SMTPClient.Interfaces
{
    public interface IRandom
    {
        void GetBytes(byte[] data);
        void GetBytes(byte[] data, int offset, int count);

        ulong GetUInt64();
    }
}
