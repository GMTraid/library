﻿using System.IO;

namespace Library.Domain.SMTPClient.Interfaces
{
    public interface ITlsProvider
    {
        Stream AuthenticateAsClient(Stream innerStream, string host);
    }
}
