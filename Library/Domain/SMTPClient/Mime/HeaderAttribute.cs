﻿namespace Library.Domain.SMTPClient.Mime
{
    public class HeaderAttribute
    {
        public string Key;
        public string Value;

        public HeaderAttribute(string key, string value)
        {
            this.Key = key;
            this.Value = value;
        }
    }
}