﻿namespace Library.Domain.SMTPClient
{
    public enum ByteOrder
    {
        LittleEndian,
        BigEndian
    }
}