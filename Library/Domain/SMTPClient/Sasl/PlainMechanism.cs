﻿using System;
using System.Text;

namespace Library.Domain.SMTPClient.Sasl
{
    public static class PlainMechanism
    {
        public static string Encode(string authorizationIdentity, string authenticationIdentity, string password)
        {
            string message = authorizationIdentity + '\0' + authenticationIdentity + '\0' + password;
            return Convert.ToBase64String(Encoding.UTF8.GetBytes(message));
        }
    }
}