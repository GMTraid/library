﻿namespace Library.Domain.SMTPClient.Sasl
{
    public static class Mechanism
    {
        public const string Plain = "PLAIN";
        public const string XOAuth2 = "XOAUTH2";
    }
}