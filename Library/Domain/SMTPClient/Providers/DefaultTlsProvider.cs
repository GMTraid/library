﻿using Library.Domain.SMTPClient.Interfaces;
using System.IO;
using System.Net.Security;

namespace Library.Domain.SMTPClient.Providers
{
    public class DefaultTlsProvider : ITlsProvider
    {
        public Stream AuthenticateAsClient(Stream innerStream, string host)
        {
            var ssl = new SslStream(innerStream, true);
            ssl.AuthenticateAsClient(host, null, System.Security.Authentication.SslProtocols.Tls12, true);
            return ssl;
        }
    }
}