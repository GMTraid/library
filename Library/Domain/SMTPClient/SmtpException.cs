﻿using System;

namespace Library.Domain.SMTPClient
{
    public class SmtpException : Exception
    {
        public SmtpException()
        {
        }

        public SmtpException(string message) : base(message)
        {
        }
    }
}