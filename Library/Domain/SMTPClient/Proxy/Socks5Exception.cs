﻿using System;

namespace Library.Domain.SMTPClient.Proxy
{
    public class Socks5Exception : Exception
    {
        public Socks5Exception(string message)
            : base(message)
        {
        }
    }
}