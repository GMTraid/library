﻿using System;

namespace Library.Domain.SMTPClient.Proxy.Constans
{
    public class Constants
    {
        public static readonly string TelnetEndOfLine = new string(new[] { (char)0x0D, (char)0x0A });
        public static readonly DateTime UnixTimeStart = new DateTime(1970, 1, 1);
    }
}