﻿namespace Library.Domain.SMTPClient
{
    public static class ContentTransferEncoding
    {
        public const string Base64 = "BASE64";
        public const string Bit7 = "7BIT";
        public const string Bit8 = "8BIT";
        public const string Binary = "BINARY";
    }
}