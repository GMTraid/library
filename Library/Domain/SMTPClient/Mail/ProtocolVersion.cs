﻿namespace Library.Domain.SMTPClient.Mail
{
    public enum ProtocolVersion : ushort
    {
        Tls12 = 0x0303,
        Tls13 = 0x0304
    }
}