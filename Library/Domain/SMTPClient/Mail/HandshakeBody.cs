﻿using System.IO;

namespace Library.Domain.SMTPClient.Mail
{
    internal abstract class HandshakeBody
    {
        public HandshakeType Type { get; private set; }

        public HandshakeBody(HandshakeType type)
        {
            this.Type = type;
        }

        public abstract void Read(BinaryReader reader);
        public abstract void WriteTo(BinaryWriter writer);
    }
}