﻿using System.Text;

namespace Library.Domain.SMTPClient.Mail
{
    public class MailAttachment
    {
        public string Filename { get; set; }
        public byte[] Content { get; set; }
        public string ContentType { get; set; }
        public Encoding TextEncoding { get; set; } = Encoding.UTF8;
    }
}