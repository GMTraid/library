﻿using System;
using System.Data.Entity;
using System.Linq.Expressions;

namespace Library.Domain.CrudFramework
{
    public class DirtyCheck : IDirtyCheck
    {
        public DbContext DbContext { get; set; }

        public DirtyCheck(DbContext context)
        {
            DbContext = context;
        }

        public bool IsDirtyEntity(object entity)
        {
            var entry = DbContext.Entry(entity);
            return !(entry.State == EntityState.Unchanged);
        }

        public bool IsDirtyEntity<T>(T entity) where T : class
        {
            var entry = DbContext.Entry(entity);
            return !(entry.State == EntityState.Unchanged);
        }

        public bool IsDirtyProperty<T, U>(T entity, Expression<Func<T, U>> propertySelector) where T : class
        {
            var entry = DbContext.Entry(entity);
            var property = entry.Property(propertySelector);
            return property.IsModified;
        }

        public U GetOriginalValue<T, U>(T entity, Expression<Func<T, U>> propertySelector) where T : class
        {
            var entry = DbContext.Entry(entity);
            var property = entry.Property(propertySelector);
            return property.OriginalValue;
        }
    }
}