﻿using System;
using System.Linq.Expressions;

namespace Library.Domain.CrudFramework
{
    public interface IDirtyCheck
    {
        bool IsDirtyEntity(object entity);
        bool IsDirtyEntity<T>(T entity) where T : class;
        bool IsDirtyProperty<T, U>(T entity, Expression<Func<T, U>> propertySelector) where T : class;
        U GetOriginalValue<T, U>(T entity, Expression<Func<T, U>> propertySelector) where T : class;
    }
}
