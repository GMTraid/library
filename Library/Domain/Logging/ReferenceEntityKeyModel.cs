﻿using System.Data.Entity.Core;

namespace Library.Domain.Logging
{
    public class ReferenceEntityKeyModel
    {
        public EntityKey SourceKey { get; set; }
        public EntityKey TargetKey { get; set; }
    }
}