﻿using System;
using System.Collections.Generic;

namespace Library.Domain.Logging
{
    public class MemoryLogger
    {
        public List<string> Lines { get; private set; } = new List<string>();

        public virtual void Log(string line)
        {
            Lines.AddRange(line.Split(new[] { Environment.NewLine }, StringSplitOptions.RemoveEmptyEntries));
        }
    }
}