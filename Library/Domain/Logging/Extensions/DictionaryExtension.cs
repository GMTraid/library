﻿using System;
using System.Collections.Generic;

namespace Library.Domain.Logging.Extensions
{
    public static class DictionaryExtension
    {
        public static U GetOrCreate<T, U>(this Dictionary<T, U> dictionary, T key) where U : new()
        {
            return dictionary.GetOrCreate(key, new U());
        }

        public static U GetOrCreate<T, U>(this Dictionary<T, U> dictionary, T key, U createValue)
        {
            if (dictionary.ContainsKey(key))
            {
                return dictionary[key];
            }
            else
            {
                dictionary[key] = createValue;
                return createValue;
            }

        }

        public static U GetOrCreate<T, U>(this Dictionary<T, U> dictionary, T key, Func<U> createValueFunc)
        {
            if (dictionary.ContainsKey(key))
            {
                return dictionary[key];
            }
            else
            {
                var createValue = createValueFunc();
                dictionary[key] = createValue;
                return createValue;
            }
        }
    }
}
