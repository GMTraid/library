﻿using System.ComponentModel;

namespace Library.Domain.Logging
{
    public enum EntityLogActionType
    {
        [Description("Редактирование")]
        Update = 0,

        [Description("Создание")]
        Create = 10,

        [Description("Удаление")]
        Delete = 20,
    }
}