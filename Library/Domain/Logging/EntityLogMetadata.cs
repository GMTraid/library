﻿using Library.Domain.Logging.Base;
using Library.Domain.Logging.Base.Interfaces;

namespace Library.Domain.Logging
{
    public class EntityLogMetadata : BaseEntityLogMetadata
    {
        public IEntityLogMap[] EntityLogMaps { get; set; }

        protected override IEntityLogMap[] GetEntityLogMaps()
        {
            return EntityLogMaps;
        }
    }
}