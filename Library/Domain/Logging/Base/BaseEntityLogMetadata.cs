﻿using Library.Domain.Logging.Base.Interfaces;
using Library.Domain.Logging.Extensions;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Core.Metadata.Edm;
using System.Data.Entity.Infrastructure;
using System.Linq;

namespace Library.Domain.Logging.Base
{
    public abstract class BaseEntityLogMetadata
    {
        protected IEntityLogMap[] AllMainEntityLogMaps { get; set; }

        protected Dictionary<string, List<IEntityLogMap>> EntityMapByStructuralTypeName { get; set; } = new Dictionary<string, List<IEntityLogMap>>();
        public Dictionary<string, HashSet<IEntityPropertyLogMap>> RefPropertyMapsByStructuralTypeName { get; set; }
            = new Dictionary<string, HashSet<IEntityPropertyLogMap>>();
        public Dictionary<string, Dictionary<string, List<IEntityPropertyLogMap>>> PropertyMapsByStructuralTypeName { get; set; }
            = new Dictionary<string, Dictionary<string, List<IEntityPropertyLogMap>>>();

        protected bool IsInit { get; set; }

        public void Init(DbContext dbContext)
        {
            if (!IsInit)
            {
                var objectContextAdapter = dbContext as IObjectContextAdapter;
                var metadataWorkspace = objectContextAdapter.ObjectContext.MetadataWorkspace;
                Init(metadataWorkspace);
            }
        }

        protected void Init(MetadataWorkspace metadataWorkspace)
        {
            InitMaps();

            foreach (var entityMap in AllMainEntityLogMaps)
            {
                WriteMetadata(metadataWorkspace, entityMap);
            }

            IsInit = true;
        }

        private void WriteMetadata(MetadataWorkspace metadataWorkspace, IEntityLogMap entityMap)
        {
            var entityType = entityMap.EntityType;

            var entityTypeMetadate = metadataWorkspace.GetType(entityType.Name, entityType.Namespace, DataSpace.OSpace) as EntityType;
            EntityMapByStructuralTypeName.GetOrCreate(entityTypeMetadate.Name).Add(entityMap);


            foreach (var propertyMap in entityMap.Maps)
            {
                var propertyType = propertyMap.LastPropertyInfo.PropertyType;
                var declaringType = propertyMap.LastPropertyInfo.ReflectedType;

                var metadate = metadataWorkspace.GetType(declaringType.Name, declaringType.Namespace, DataSpace.OSpace) as EntityType;
                if (metadate != null)
                {
                    foreach (var mapProperty in propertyMap.Properties)
                    {
                        var member = metadate.Members[mapProperty.Name];
                        switch (member)
                        {
                            case NavigationProperty navigationProperty:
                                {
                                    var typeName = navigationProperty.RelationshipType.Name;
                                    var propertyMaps = RefPropertyMapsByStructuralTypeName.GetOrCreate(typeName);
                                    propertyMaps.Add(propertyMap);
                                    entityMap.RefMaps.Add(propertyMap);
                                }
                                break;
                            case EdmProperty property:
                                {
                                    var typeName = property.DeclaringType.Name;
                                    var propertyName = property.Name;
                                    var propertyMapsByPropertyName = PropertyMapsByStructuralTypeName.GetOrCreate(typeName);
                                    var propertyMaps = propertyMapsByPropertyName.GetOrCreate(propertyName);
                                    propertyMaps.Add(propertyMap);
                                    entityMap.PropertyMaps.Add(propertyMap);
                                    entityMap.PropertyMapsByPropertyName.Add(property.Name, propertyMap);
                                }
                                break;
                        }
                    }
                }
            }
            foreach (var relatedEntityMap in entityMap.RelatedEnityLogMaps)
            {
                WriteMetadata(metadataWorkspace, relatedEntityMap);
            }
        }

        public bool TryGetEntityLogMapByEdmTypeName(string edmTypeName, out List<IEntityLogMap> value)
        {
            return EntityMapByStructuralTypeName.TryGetValue(edmTypeName, out value);
        }

        protected virtual void InitMaps()
        {
            try
            {
                AllMainEntityLogMaps = GetEntityLogMaps().Select(x => GetMainMaps(x)).ToArray();
            }
            catch (Exception ex)
            {
                throw new Exception("An error occurred while generating metadata for entity logging", ex);
            }
        }

        private IEntityLogMap GetMainMaps(IEntityLogMap x)
        {
            return x.IsMain ? x : GetMainMaps(x.ParentEntityLogMap);
        }

        protected abstract IEntityLogMap[] GetEntityLogMaps();
    }
}