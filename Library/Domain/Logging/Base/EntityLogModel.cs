﻿using Library.Domain.Logging.Base.Interfaces;
using Library.Domain.Logging.Extensions;
using Library.Domain.Models.Entity;
using System;
using System.Collections.Generic;
using System.Data.Entity.Core;
using System.Data.Entity.Core.Objects;
using System.Linq;

namespace Library.Domain.Logging.Base
{
    public class EntityLogModel
    {
        public bool IsMain => EntityLogMap.IsMain;

        protected Lazy<string> LazyDescription = null;
        protected IEntityLogMap EntityLogMap { get; set; }

        public EntityLogModel ParentEntityLogModel { get; set; }

        public EntityLogActionType ActionType { get; protected set; }
        public string Description => LazyDescription.Value;
        protected object Entity { get; set; }

        public string DescriptionForPropery => IsMain ? "" : $"{ParentEntityLogModel?.DescriptionForPropery}{Description}|";
        public ObjectStateEntry ObjectStateEntry { get; set; }
        public Dictionary<IEntityPropertyLogMap, EntityPropertyLogModel> Properties { get; set; } = new Dictionary<IEntityPropertyLogMap, EntityPropertyLogModel>();

        protected List<EntityLogModel> RelatedEntityLogs { get; set; } = new List<EntityLogModel>();
        protected Dictionary<Tuple<IEntityPropertyLogMap, EntityKey>, IEntityPropertyLogMap> EntityPropertyLogMapByEntityKey { get; set; } = new Dictionary<Tuple<IEntityPropertyLogMap, EntityKey>, IEntityPropertyLogMap>();

        public int EntityTypeId { get; internal set; }
        public string EntityTypeName { get; internal set; }


        public EntityLogModel(ObjectStateEntry objectStateEntry, IEntityLogMap entityLogMap)
        {
            EntityLogMap = entityLogMap;
            ObjectStateEntry = objectStateEntry;
            Entity = objectStateEntry.Entity;
            ActionType = GetActionType();
            EntityTypeId = entityLogMap.TypeId;
            EntityTypeName = entityLogMap.EntityType.Name;

            LazyDescription = new Lazy<string>(() => EntityLogMap.GetDescription(Entity));
        }

        public void InitDescription()
        {
            var initValue = LazyDescription.Value;
        }

        public void SetNewPropertyValue(IEntityPropertyLogMap propertyLogMap, EntityKey sourceEntityKey, Func<object> getValueFunc)
        {
            propertyLogMap = GetPropertyLogMap(propertyLogMap, sourceEntityKey);
            var property = Properties.GetOrCreate(propertyLogMap, new EntityPropertyLogModel(this, propertyLogMap));
            property.GetNewValueFunc = getValueFunc;
        }

        public void SetOldPropertyValue(IEntityPropertyLogMap propertyLogMap, EntityKey sourceEntityKey, Func<object> getValueFunc)
        {
            propertyLogMap = GetPropertyLogMap(propertyLogMap, sourceEntityKey);
            var property = Properties.GetOrCreate(propertyLogMap, new EntityPropertyLogModel(this, propertyLogMap));
            property.GetOldValueFunc = getValueFunc;
        }

        /// <summary>
        /// Возвращает IEntityPropertyLogMap по указанному ключу, нужен для правильного логирования изменений по связанным коллекциям
        /// </summary>
        protected IEntityPropertyLogMap GetPropertyLogMap(IEntityPropertyLogMap propertyLogMap, EntityKey entityKey)
        {
            if (entityKey == null)
            {
                return propertyLogMap;
            }
            var result = EntityPropertyLogMapByEntityKey.GetOrCreate(Tuple.Create(propertyLogMap, entityKey), propertyLogMap.Copy());
            return result;
        }

        protected EntityLogActionType GetActionType()
        {
            switch (ObjectStateEntry.State)
            {
                case System.Data.Entity.EntityState.Added:
                    return EntityLogActionType.Create;
                case System.Data.Entity.EntityState.Deleted:
                    return EntityLogActionType.Delete;
                case System.Data.Entity.EntityState.Modified:
                default:
                    return EntityLogActionType.Update;
            }
        }

        public void AddRelatedEntityLog(EntityLogModel relatedEntityLog)
        {
            relatedEntityLog.ParentEntityLogModel = this;
            RelatedEntityLogs.Add(relatedEntityLog);
        }

        public IEnumerable<EntityPropertyLogModel> GetProperties()
        {
            return Properties.Values.Concat(RelatedEntityLogs.SelectMany(x => x.GetProperties()));
        }
    }
}