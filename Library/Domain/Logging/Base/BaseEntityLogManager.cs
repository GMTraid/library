﻿using Library.Domain.Logging.Base.Interfaces;
using Library.Domain.Logging.Extensions;
using Library.Domain.Models.Entity;
using System;
using System.Collections.Generic;
using System.Data.Common;
using System.Data.Entity;
using System.Data.Entity.Core;
using System.Data.Entity.Core.Objects;
using System.Data.Entity.Infrastructure;
using System.Linq;

namespace Library.Domain.Logging.Base
{
    public abstract class BaseEntityLogManager
    {
        public BaseEntityLogMetadata EntityLogMetadata { get; set; }

        protected DbContext DbContext { get; set; }
        protected ObjectContext ObjectContext { get; set; }
        protected bool IsInit { get; set; }

        private ObjectState[] ObjectStates { get; set; }

        protected Dictionary<int, Dictionary<ObjectStateEntry, EntityLogModel>> EntityLogsByTypeId;

        protected List<EntityLogModel> EntityLogs { get; set; }

        public BaseEntityLogManager()
        {
            Clear();
        }

        public void Clear()
        {
            EntityLogsByTypeId = new Dictionary<int, Dictionary<ObjectStateEntry, EntityLogModel>>();
            EntityLogs = new List<EntityLogModel>();
        }

        public virtual void Init(DbContext dbContext)
        {
            DbContext = dbContext;
            var objectContextAdapter = dbContext as IObjectContextAdapter;
            ObjectContext = objectContextAdapter.ObjectContext;
            EntityLogMetadata.Init(dbContext);
            IsInit = true;
        }

        protected void CheckIsInit()
        {
            if (!IsInit)
            {
                throw new Exception("Log manager has not been initialized");
            }
        }

        protected void RememberObjectStates()
        {
            CheckIsInit();

            ObjectStates = ObjectContext.ObjectStateManager.GetObjectStateEntries(EntityState.Modified | EntityState.Deleted | EntityState.Added)
                .Select(x => new ObjectState { ObjectStateEntry = x, State = x.State })
                .ToArray();
        }

        public void BuildChanges()
        {
            CheckIsInit();

            var states = ObjectContext.ObjectStateManager.GetObjectStateEntries(EntityState.Modified | EntityState.Deleted | EntityState.Added);
            foreach (var objectState in states)
            {
                var state = objectState.State;
                var objectStateEntry = objectState;
                BuildChangesByState(objectStateEntry, state, false);
            }
        }

        public void BuildChangesByState(object entity, EntityState state)
        {
            var objectStateEntry = ObjectContext.ObjectStateManager.GetObjectStateEntry(entity);
            BuildChangesByState(objectStateEntry, state, true);
        }

        public void BuildChangesByState(ObjectStateEntry objectStateEntry, EntityState state, bool buildDeleteState)
        {
            if (objectStateEntry.IsRelationship)
            {
                switch (state)
                {
                    case EntityState.Added:
                        {
                            var values = objectStateEntry.CurrentValues;
                            ReferenceEntityKeyModel referenceEntityKeyModel = GetReferenceEntityKeys(values);
                            var targetKey = referenceEntityKeyModel.TargetKey;
                            var sourceKey = referenceEntityKeyModel.SourceKey;

                            if (TryGetRefPropertyLogMaps(objectStateEntry, out HashSet<IEntityPropertyLogMap> propertiyLogMaps))
                            {
                                var propertyValue = ObjectContext.GetObjectByKey(targetKey);
                                foreach (var propertyLogMap in propertiyLogMaps)
                                {
                                    var sourceObjectStateEntry = ObjectContext.ObjectStateManager.GetObjectStateEntry(sourceKey);
                                    var entityObjectStateEntry = propertyLogMap.GetEntityEntryByPropertyEntry(ObjectContext, sourceObjectStateEntry);
                                    SetNewRefValue(propertyLogMap, entityObjectStateEntry, propertyValue, sourceObjectStateEntry.EntityKey);
                                }
                            }
                        }
                        break;
                    case EntityState.Deleted:
                        {
                            var values = objectStateEntry.OriginalValues;
                            ReferenceEntityKeyModel referenceEntityKeyModel = GetReferenceEntityKeys(values);
                            var targetKey = referenceEntityKeyModel.TargetKey;
                            var sourceKey = referenceEntityKeyModel.SourceKey;

                            if (TryGetRefPropertyLogMaps(objectStateEntry, out HashSet<IEntityPropertyLogMap> propertiyLogMaps))
                            {
                                var propertyValue = ObjectContext.GetObjectByKey(targetKey);
                                foreach (var propertyLogMap in propertiyLogMaps)
                                {
                                    var sourceObjectStateEntry = ObjectContext.ObjectStateManager.GetObjectStateEntry(sourceKey);
                                    var entityObjectStateEntry = propertyLogMap.GetEntityEntryByPropertyEntry(ObjectContext, sourceObjectStateEntry);
                                    SetOldRefValue(propertyLogMap, entityObjectStateEntry, propertyValue, sourceObjectStateEntry.EntityKey);
                                }
                            }
                        }
                        break;
                }
            }
            else
            {
                switch (state)
                {
                    case EntityState.Added:
                        {

                            if (TryGetEntityLogMap(objectStateEntry, out List<IEntityLogMap> entityLogMaps))
                            {
                                foreach (var entityLogMap in entityLogMaps)
                                {
                                    var entityLog = GetEntityLog(entityLogMap, objectStateEntry);
                                    if (entityLog == null)
                                    {
                                        continue;
                                    }
                                    foreach (var propertyLogMap in entityLogMap.Maps)
                                    {
                                        var entity = objectStateEntry.Entity;
                                        object newValueFunc() => propertyLogMap.GetPropertyValueByEntity(objectStateEntry.Entity);
                                        entityLog.SetNewPropertyValue(propertyLogMap, objectStateEntry.EntityKey, newValueFunc);
                                    }
                                }
                            }
                        }
                        break;
                    case EntityState.Deleted:
                        {
                            if (buildDeleteState && TryGetEntityLogMap(objectStateEntry, out List<IEntityLogMap> entityLogMaps))
                            {
                                foreach (var entityLogMap in entityLogMaps)
                                {
                                    var entityLog = GetEntityLog(entityLogMap, objectStateEntry);
                                    if (entityLog == null)
                                    {
                                        continue;
                                    }
                                    entityLog.InitDescription();
                                    foreach (var propertyLogMap in entityLogMap.Maps)
                                    {
                                        var oldValue = propertyLogMap.GetPropertyValueByEntity(objectStateEntry.Entity);
                                        entityLog.SetOldPropertyValue(propertyLogMap, objectStateEntry.EntityKey, () => oldValue);
                                    }
                                }
                            }
                        }
                        break;
                    case EntityState.Modified:
                        {
                            if (TryGetEntityLogMap(objectStateEntry, out List<IEntityLogMap> entityLogMaps))
                            {
                                foreach (var entityLogMap in entityLogMaps)
                                {
                                    foreach (var propertyName in objectStateEntry.GetModifiedProperties())
                                    {
                                        var entityLog = GetEntityLog(entityLogMap, objectStateEntry);
                                        if (entityLog == null)
                                        {
                                            continue;
                                        }
                                        if (entityLogMap.PropertyMapsByPropertyName.TryGetValue(propertyName, out IEntityPropertyLogMap propertyLogMap))
                                        {
                                            var currentValues = objectStateEntry.CurrentValues;
                                            object newValueFunc() => currentValues[propertyName];
                                            var oldValue = objectStateEntry.OriginalValues[propertyName];

                                            entityLog.SetNewPropertyValue(propertyLogMap, objectStateEntry.EntityKey, newValueFunc);
                                            entityLog.SetOldPropertyValue(propertyLogMap, objectStateEntry.EntityKey, () => oldValue);
                                        }
                                    }
                                }
                            }
                            break;
                        }
                }
            }
        }

        private bool TryGetPropertyLogMapsByPropertyName(ObjectStateEntry objectState, out Dictionary<string, List<IEntityPropertyLogMap>> propertyLogMapsByPropertyName)
        {
            return EntityLogMetadata.PropertyMapsByStructuralTypeName.TryGetValue(objectState.EntitySet.ElementType.Name, out propertyLogMapsByPropertyName);
        }

        private bool TryGetRefPropertyLogMaps(ObjectStateEntry objectState, out HashSet<IEntityPropertyLogMap> propertiesLogMaps)
        {
            return EntityLogMetadata.RefPropertyMapsByStructuralTypeName.TryGetValue(objectState.EntitySet.ElementType.Name, out propertiesLogMaps);
        }

        private bool TryGetEntityLogMap(ObjectStateEntry objectState, out List<IEntityLogMap> entityLogMaps)
        {
            return EntityLogMetadata.TryGetEntityLogMapByEdmTypeName(objectState.EntitySet.ElementType.Name, out entityLogMaps);
        }

        private ReferenceEntityKeyModel GetReferenceEntityKeys(DbDataRecord values)
        {
            var sourceKey = values[0] as EntityKey ?? throw new Exception("Failed to cast to type EntityKey");
            var targetKey = values[1] as EntityKey ?? throw new Exception("Failed to cast to type EntityKey");

            var result = new ReferenceEntityKeyModel
            {
                SourceKey = sourceKey,
                TargetKey = targetKey
            };

            return result;
        }

        private void SetNewRefValue(IEntityPropertyLogMap propertyLogMap, ObjectStateEntry objectStateEntry, object propertyValue, EntityKey sourceEntityKey)
        {
            var entityLog = GetEntityLog(propertyLogMap.EntityLogMap, objectStateEntry);
            if (entityLog == null)
            {
                return;
            }
            entityLog.SetNewPropertyValue(propertyLogMap, sourceEntityKey, () => propertyValue);
        }

        private void SetOldRefValue(IEntityPropertyLogMap propertyLogMap, ObjectStateEntry objectStateEntry, object propertyValue, EntityKey sourceEntityKey)
        {
            var entityLog = GetEntityLog(propertyLogMap.EntityLogMap, objectStateEntry);
            if (entityLog == null)
            {
                return;
            }
            entityLog.SetOldPropertyValue(propertyLogMap, sourceEntityKey, () => propertyValue);
        }

        private EntityLogModel GetEntityLog(IEntityLogMap entityLogMap, ObjectStateEntry objectStateEntry)
        {
            var entityTypeId = entityLogMap.TypeId;
            var entityLogsByStateEntry = EntityLogsByTypeId.GetOrCreate(entityTypeId);
            var entityLog = entityLogsByStateEntry.GetOrCreate(objectStateEntry, () => CreateNewEntityLogModel(entityLogMap, objectStateEntry));

            return entityLog;
        }

        private EntityLogModel CreateNewEntityLogModel(IEntityLogMap entityLogMap, ObjectStateEntry objectStateEntry)
        {
            var result = new EntityLogModel(objectStateEntry, entityLogMap);
            if (entityLogMap.IsMain)
            {
                EntityLogs.Add(result);
            }
            else
            {
                var parentEntityLogMap = entityLogMap.ParentEntityLogMap;
                var parentEntity = entityLogMap.GetParentEntity(objectStateEntry.Entity);

                if (parentEntity == null)
                {
                    return null;
                }

                var parentObjectStateEntry = ObjectContext.ObjectStateManager.GetObjectStateEntry(parentEntity);

                var entityLogsByStateEntry = EntityLogsByTypeId.GetOrCreate(parentEntityLogMap.TypeId);
                var parentEntityLog = entityLogsByStateEntry.GetOrCreate(parentObjectStateEntry, () => CreateNewEntityLogModel(parentEntityLogMap, parentObjectStateEntry));
                parentEntityLog.AddRelatedEntityLog(result);
            }
            return result;
        }

        public void SaveLogs()
        {
            CheckIsInit();
            var entityLogModels = EntityLogs;
            Clear();
            SaveLogsImplementation(entityLogModels);
        }

        protected abstract void SaveLogsImplementation(List<EntityLogModel> entityLogModels);

        protected EntityKey GetKey(EntityLogModel entityLog)
        {
            return entityLog.ObjectStateEntry.EntityKey;
        }

        private class ObjectState
        {
            public ObjectStateEntry ObjectStateEntry { get; set; }
            public EntityState State { get; set; }
        }
    }
}