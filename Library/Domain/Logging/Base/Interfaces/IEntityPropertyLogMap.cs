﻿using System.Collections.Generic;
using System.Data.Entity.Core.Objects;
using System.Reflection;

namespace Library.Domain.Logging.Base.Interfaces
{
    public interface IEntityPropertyLogMap
    {
        IEntityLogMap EntityLogMap { get; set; }
        PropertyInfo LastPropertyInfo { get; set; }
        IEnumerable<PropertyInfo> Properties { get; set; }
        string PropertyDescription { get; set; }

        /// <summary>
        /// Возвращает описание
        /// </summary>
        string GetDescription(object value);

        ObjectStateEntry GetEntityEntryByPropertyEntry(ObjectContext objectContext, ObjectStateEntry sourceObjectStateEntry);
        IEntityPropertyLogMap Copy();

        object GetPropertyValueByEntity(object entity);
    }
}
