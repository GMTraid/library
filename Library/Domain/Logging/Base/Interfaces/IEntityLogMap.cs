﻿using System;
using System.Collections.Generic;

namespace Library.Domain.Logging.Base.Interfaces
{
    public interface IEntityLogMap
    {
        IEntityLogMap ParentEntityLogMap { get; }

        Type EntityType { get; set; }
        Dictionary<string, IEntityPropertyLogMap> PropertyMapsByPropertyName { get; set; }
        List<IEntityPropertyLogMap> Maps { get; set; }
        List<IEntityPropertyLogMap> RefMaps { get; set; }
        List<IEntityPropertyLogMap> PropertyMaps { get; set; }
        List<IEntityLogMap> RelatedEnityLogMaps { get; set; }
        int TypeId { get; set; }
        bool IsMain { get; }

        string GetDescription(object value);
        object GetParentEntity(object entity);
    }
}
