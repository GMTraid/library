﻿using Library.Database.Entities.EntityLog;
using Library.Database.Entities.Identity;
using Library.Database.Enums;
using Library.Domain.Config;
using Library.Domain.Logging.Base;
using Library.Domain.Models.Entity;
using Library.Extensions;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Library.Domain.Logging
{
    public class EntityLogManager : BaseEntityLogManager
    {
        public LibraryConfiguration LibraryConfiguration { get; set; }

        protected bool EnableLogInFile => LibraryConfiguration.EnableEntityLogInFile;

        protected override void SaveLogsImplementation(List<EntityLogModel> entityLogModels)
        {
            try
            {
                var createDate = DateTimeOffset.Now;

                var userId = HttpContext.Current?.User?.Identity?.GetUserId<long>();
                var user = userId != null ? DbContext.Set<User>().Find(userId) : null;

                var entityLogs = new List<EntityLog>();
                var entityPropertyLogs = new List<EntityPropertyLog>();
                foreach (var entityLogModel in entityLogModels.Where(x => x.ObjectStateEntry.State != EntityState.Detached))
                {
                    var properties = entityLogModel.GetProperties();
                    if (properties.Any())
                    {
                        var entityLog = new EntityLog
                        {
                            //ActionType = GetInDatabaseActionType(entityLogModel),
                            CreateDate = createDate,
                            EntityId = GetLongKey(entityLogModel),
                            EntityDescription = entityLogModel.Description,
                            EntityTypeId = (EntityType)entityLogModel.EntityTypeId,
                            EntityTypeName = entityLogModel.EntityTypeName,
                            User = user,
                            UserDescription = GetUserDescription(user)
                        };
                        entityLogs.Add(entityLog);

                        foreach (var entityPropertyLogModel in properties)
                        {
                            var entityPropertyLog = new EntityPropertyLog
                            {
                                EntityLog = entityLog,
                                NewValue = entityPropertyLogModel.NewValueDescription,
                                OldValue = entityPropertyLogModel.OldValueDescription,
                                PropertyDescription = entityPropertyLogModel.PropertyDescription,
                                Property = entityPropertyLogModel.Property
                            };
                            entityPropertyLogs.Add(entityPropertyLog);
                        }

                        if (EnableLogInFile)
                        {
                            LogInFile(entityLog, entityPropertyLogs);
                        }
                    }
                }

                if (entityLogs?.Count > 0)
                {
                    DbContext.Set<EntityLog>().AddRange(entityLogs);
                    DbContext.Set<EntityPropertyLog>().AddRange(entityPropertyLogs);
                    DbContext.SaveChanges();
                }
            }
            catch (Exception ex)
            {
                throw new Exception("When you save a log of changes to the entity an error occurred", ex);
            }
        }

        private static EntityLogActionType GetInDatabaseActionType(EntityLogModel entityLogModel)
        {
            switch (entityLogModel.ActionType)
            {
                case EntityLogActionType.Create: return EntityLogActionType.Create;
                case EntityLogActionType.Delete: return EntityLogActionType.Delete;
                case EntityLogActionType.Update: return EntityLogActionType.Update;
                default: throw new NotSupportedException($"No conversion {entityLogModel.ActionType} in the type {typeof(EntityLogActionType)}");
            }
        }

        private void LogInFile(EntityLog entityLog, IEnumerable<EntityPropertyLog> entityPropertyLog)
        {
            var log = NLog.LogManager.GetCurrentClassLogger();
            log.Debug($"{entityLog.ActionType.GetDescription()} | {entityLog.EntityTypeId} | {entityLog.EntityTypeName} | {entityLog.EntityId} | Description: {entityLog.EntityDescription}");
            foreach (var property in entityPropertyLog)
            {
                log.Debug($"--- {property.PropertyDescription} {property.Property} OldValue: {property.OldValue} NewValue: {property.NewValue}");
            }

            log.Debug($"--- -------------------------------------------------------------- ---");
        }

        private long GetLongKey(EntityLogModel entityLogModel)
        {
            var entityKey = GetKey(entityLogModel);
            var keyValue = entityKey.EntityKeyValues?[0]?.Value;
            return keyValue as long? ?? 0L;
        }

        private string GetUserDescription(User user)
        {
            if (user == null)
            {
                return "System";
            }
            return $"{string.Join(", ", user.Roles.Select(x => x.Role.Name))} | {user.LastName} {user.FirstName} {user.Patronymic}";
        }
    }
}