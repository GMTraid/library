﻿namespace Library.Models
{
    public class AuthorizationHeader
    {
        /// <summary>
        /// Схема авторизации
        /// </summary>
        public string Schema { get; set; }

        /// <summary>
        /// Параметры
        /// </summary>
        public string Parameter { get; set; }
    }
}