﻿using Library.Domain.Logging.Base.Interfaces;
using System;

namespace Library.Domain.Models.Entity
{
    public class EntityPropertyLogModel
    {
        public EntityPropertyLogModel(EntityLogModel entityLogModel, IEntityPropertyLogMap propertyLogMap)
        {
            EntityLogModel = entityLogModel;
            PropertyLogMap = propertyLogMap;
        }

        protected object NewValue { get; set; }
        protected object OldValue { get; set; }

        public string NewValueDescription => GetNewValueFunc != null ? PropertyLogMap.GetDescription(GetNewValueFunc.Invoke()) : null;
        public string OldValueDescription => GetOldValueFunc != null ? PropertyLogMap.GetDescription(GetOldValueFunc.Invoke()) : null;

        public EntityLogModel EntityLogModel { get; set; }
        public IEntityPropertyLogMap PropertyLogMap { get; set; }

        public string PropertyDescription => $"{EntityLogModel.DescriptionForPropery}{PropertyLogMap.PropertyDescription}";

        public string Property => PropertyLogMap.LastPropertyInfo.Name;

        public Func<object> GetNewValueFunc { get; internal set; }
        public Func<object> GetOldValueFunc { get; internal set; }
    }
}