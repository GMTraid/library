﻿using Newtonsoft.Json;
using System.Collections;

namespace Library.Domain.Models
{
    public class ListResult
    {
        [JsonProperty(PropertyName = "data")]
        public IEnumerable Data { get; set; }

        [JsonProperty(PropertyName = "metaData")]
        public object MetaData { get; set; }

        [JsonProperty(PropertyName = "recordsTotal")]
        public int RecordsTotal { get; set; }

        [JsonProperty(PropertyName = "recordsFiltered")]
        public int RecordsFiltered { get; set; }

        [JsonProperty(PropertyName = "draw")]
        public int Draw { get; set; }

        public ListResult()
        {
        }

        public ListResult(IEnumerable data, int totalCount, int draw) : this(data, totalCount, draw, null)
        {
        }

        public ListResult(IEnumerable data, int totalCount, int draw, object metaData)
        {
            Data = data;
            MetaData = metaData;
            RecordsTotal = totalCount;
            RecordsFiltered = totalCount;
            Draw = draw;
        }
    }
}