﻿namespace Library.Domain.Models.Filters
{
    public static class FilterActions
    {
        public const string Default = "default";

        public const string Eq = "eq";

        public const string Contains = "contains";

        public const string And = "and";

        public const string Or = "or";
    }
}