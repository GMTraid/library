﻿using Library.Domain.Attributes;
using Library.Domain.Config;
using Library.Models;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Linq;

namespace Library.Domain.Models
{
    public class ApiClientManager : IApiClientManager
    {
        private List<BaseApiClient> ApiClients { get; set; }

        public ApiClientManager(LibraryConfiguration liderTransConfiguration)
        {
            ApiClients = new List<BaseApiClient>();

            ApiClients = JsonConvert.DeserializeObject<List<BaseApiClient>>(liderTransConfiguration.ApiClientOptions.Clients);
        }


        public BaseApiClient GetClient(string clientNumber)
        {
            return ApiClients.FirstOrDefault(x => x.ClientNumber == clientNumber);
        }

        public void SetLastNonce(BaseApiClient client, long nonce)
        {
            //client.LastNonce = nonce;
        }
    }
}