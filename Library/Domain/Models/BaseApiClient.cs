﻿namespace Library.Models
{
    public class BaseApiClient
    {
        /// <summary>
        /// Уникальный номер
        /// </summary>
        public string ClientNumber { get; set; }

        /// <summary>
        /// Наименование
        /// </summary>
        public string Name { get; set; }

        public long LastNonce { get; set; }

        /// <summary>
        /// Секретный ключ
        /// </summary>
        public string SecretKey { get; set; }
    }
}