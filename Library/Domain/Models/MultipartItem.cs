﻿using System.Net.Http;

namespace Library.Domain.Models
{
    public class MultipartItem
    {
        public HttpContent Content { get; set; }
        public string ArrayKey { get; set; }
        public string MainProperty { get; set; }
        public string PropertyName { get; set; }
    }
}