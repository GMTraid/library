﻿namespace Library.Models
{
    public class AuthorizationParametr
    {
        /// <summary>
        /// Номер клиента
        /// </summary>
        public string ClientNumber { get; set; }

        /// <summary>
        /// Хеш
        /// </summary>
        public string Hash { get; set; }

        /// <summary>
        /// Идентификатор отправки
        /// </summary>
        public long Nonce { get; set; }
    }
}