﻿namespace Library.Domain.Models
{
    public class Result<TData>
    {
        public bool Success { get; set; }
        public TData Data { get; set; }
        public string Message { get; set; }

        public Result()
        {

        }

        public Result(bool success, string message = null)
        {
            Success = success;
            Message = message ?? "";
        }

        public static Result<T> SuccessResult<T>(T data)
        {
            return new Result<T>(true, null) { Data = data };
        }
    }

    public class Result : Result<object>
    {
        public Result()
        {
        }

        public Result(bool success, string message = null) : base(success, message)
        {
        }
    }
}