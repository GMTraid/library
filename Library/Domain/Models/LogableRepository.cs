﻿using Library.Database.Interfaces.Base;
using Library.Models;
using System.Collections.Generic;
using System.Data.Entity;

namespace Library.Domain.Models
{
    public class LogableRepository<T, TKey> : Repository<T, TKey> where T : class, IEntity<TKey>, new()
    {
        public override void Delete(T entity)
        {
            if (DbContext is LogableDbContext logableDbContext)
            {
                logableDbContext.BuildChangesByState(entity, EntityState.Deleted);
            }
            base.Delete(entity);
        }

        public override void DeleteRange(IEnumerable<T> entities)
        {
            foreach (var entity in entities)
            {
                if (DbContext is LogableDbContext logableDbContext)
                {
                    logableDbContext.BuildChangesByState(entity, EntityState.Deleted);
                }
            }
            base.DeleteRange(entities);
        }
    }
}