﻿using Library.Database.Interfaces.Base;
using Library.Domain.Models.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using Library.Extensions;

namespace Library.Models
{
    public class Repository<T, TKey> : IRepository<T, TKey> where T : class, IEntity<TKey>, new()
    {
        public DbContext DbContext { get; set; }

        protected DbSet<T> DbSet => DbContext.Set<T>();

        public T Get(TKey id)
        {
            return DbSet.Find(id);
        }

        public async Task<T> GetAsync(TKey id)
        {
            return await DbSet.FindAsync(id);
        }

        public IQueryable<T> GetAll()
        {
            return DbSet;
        }

        public IQueryable<T> GetAllForOwner<TOwnerKey>(TOwnerKey ownerId)
        {
            bool implementsOwnerInterface = typeof(IOwner<TOwnerKey>).IsAssignableFrom(typeof(T));

            if (implementsOwnerInterface)
            {
                Expression<Func<IOwner<TOwnerKey>, TOwnerKey>> selector = x => x.OwnerId;
                return ((IQueryable<IOwner<TOwnerKey>>)DbSet)
                    .Where(selector.Equal(ownerId))
                    .Cast<T>();
            }
            else
            {
                return GetAll();
            }
        }

        public IQueryable<T> GetAllNoTracking()
        {
            return DbSet.AsNoTracking();
        }

        public void Delete(TKey id)
        {
            var entity = Get(id);
            Delete(entity);
        }

        public virtual void Delete(T entity)
        {
            var entry = DbContext.Entry(entity);
            DbSet.Remove(entry.Entity);
            DbContext.SaveChanges();
        }

        public Task DeleteAsync(T entity)
        {
            var entry = DbContext.Entry(entity);
            DbSet.Remove(entry.Entity);
            return DbContext.SaveChangesAsync();
        }

        public virtual void DeleteRange(IEnumerable<T> entities)
        {
            DbSet.RemoveRange(entities);
            DbContext.SaveChanges();
        }

        public Task DeleteRangeAsync(IEnumerable<T> entities)
        {
            DbSet.RemoveRange(entities);
            return DbContext.SaveChangesAsync();
        }

        public void Create(T entity)
        {
            DbSet.Add(entity);
            DbContext.SaveChanges();
        }

        public Task CreateAsync(T entity)
        {
            DbSet.Add(entity);
            return DbContext.SaveChangesAsync();
        }

        public void CreateRange(IEnumerable<T> entities)
        {
            DbSet.AddRange(entities);
            DbContext.SaveChanges();
        }

        public Task CreateRangeAsync(IEnumerable<T> entities)
        {
            DbSet.AddRange(entities);
            return DbContext.SaveChangesAsync();
        }

        public void Update(T entity)
        {
            DbContext.SaveChanges();
        }

        public Task UpdateAsync(T entity)
        {
            return DbContext.SaveChangesAsync();
        }

        public void UpdateRange(IEnumerable<T> entities)
        {
            DbContext.SaveChanges();
        }

        public Task UpdateRangeAsync(IEnumerable<T> entities)
        {
            return DbContext.SaveChangesAsync();
        }

        public async Task CreateOrUpdateAsync(T entity)
        {
            if (IsEmptyKey(entity.Id))
            {
                await CreateAsync(entity);
            }
            else
            {
                await UpdateAsync(entity);
            }
        }

        public bool IsEmptyKey(TKey id)
        {
            return object.Equals(id, default(TKey));
        }

        public void Attach(T entity)
        {
            var entry = DbContext.Entry(entity);
            if (entry.State == EntityState.Detached)
            {
                DbSet.Attach(entity);
            }
        }

        public void AttachRange(IEnumerable<T> entities)
        {
            foreach (var entity in entities)
            {
                Attach(entity);
            }
        }

        /// <summary>
        /// Освободить сущность. Перестать отслеживать.
        /// </summary>
        public void Detach(T entity)
        {
            var entry = DbContext.Entry(entity);
            entry.State = EntityState.Detached;
        }

        /// <summary>
        /// Освободить сущности. Перестать отслеживать.
        /// </summary>
        public void DetachRange(IEnumerable<T> entities)
        {
            foreach (var entity in entities)
            {
                Detach(entity);
            }
        }

        public void SetModifiedAndUpdate(T entity)
        {
            var entry = DbContext.Entry(entity);
            entry.State = EntityState.Modified;
            Update(entity);
        }

        public T Load(TKey id)
        {
            return DbSet.Local.FirstOrDefault(x => object.Equals(x.Id, id))
                ?? DbSet.Attach(new T() { Id = id });
        }

        public Task BatchInsertAsync(IEnumerable<T> entities, int? batchSize = null)
        {
            return EFBatchOperation.For(DbContext, DbSet).InsertAllAsync(entities, batchSize: batchSize);
        }
    }
}