﻿using Library.Extensions;
using System.Security.Principal;

namespace Library.Models
{
    public class DigestIdentity : GenericIdentity
    {
        public long Nonce { get; set; }
        public string Secret { get; set; }

        public DigestIdentity(string clientNumber, long nonce, string secret) : base(clientNumber, WebApiHelper.AuthorizationSchema)
        {
            Nonce = nonce;
            Secret = secret;
        }
    }
}