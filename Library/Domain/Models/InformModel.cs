﻿using Library.Database.Enums;

namespace Library.Domain.Models
{
    public class InformModel
    {
        public string Message { get; set; }

        public InformType Type { get; set; }

        public InformModel(string message, InformType type)
        {
            Message = message;
            Type = type;
        }
    }
}