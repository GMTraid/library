﻿using Library.Database.Interfaces.Base;
using Library.Domain.Models;
using System;
using System.Data.Entity;
using System.Linq;

namespace Library.Models
{
    public class Repository<TKey> : IRepository<TKey>
    {
        public DbContext DbContext { get; set; }

        public T Get<T>(TKey id) where T : class, IEntity<TKey>
        {
            return DbContext.Set<T>().Find(id);
        }

        public object Get(Type type, TKey id)
        {
            return DbContext.Set(type).Find(id);
        }

        public IQueryable<T> GetAll<T>() where T : class, IEntity<TKey>
        {
            return DbContext.Set<T>();
        }
    }
}