﻿using Library.Database.Interfaces.Base;
using System;
using System.Linq;

namespace Library.Domain.Models
{
    public interface IRepository<TKey>
    {
        T Get<T>(TKey id) where T : class, IEntity<TKey>;
        object Get(Type type, TKey id);
        IQueryable<T> GetAll<T>() where T : class, IEntity<TKey>;
    }
}
