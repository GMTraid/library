﻿namespace Library.Domain.Models
{
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "15.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    internal class Resources
    {

        private static global::System.Resources.ResourceManager resourceMan;

        private static global::System.Globalization.CultureInfo resourceCulture;

        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources()
        {
        }

        /// <summary>
        ///   Возвращает кэшированный экземпляр ResourceManager, использованный этим классом.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Resources.ResourceManager ResourceManager
        {
            get
            {
                if (object.ReferenceEquals(resourceMan, null))
                {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("Library.Main.Properties.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }

        /// <summary>
        ///   Перезаписывает свойство CurrentUICulture текущего потока для всех
        ///   обращений к ресурсу с помощью этого класса ресурса со строгой типизацией.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        internal static global::System.Globalization.CultureInfo Culture
        {
            get
            {
                return resourceCulture;
            }
            set
            {
                resourceCulture = value;
            }
        }

        /// <summary>
        ///   Ищет локализованную строку, похожую на В поле {0} можно вводить строчные, прописные буквы (кириллица / латиница).
        /// </summary>
        internal static string AllowCyrillicLatin
        {
            get
            {
                return ResourceManager.GetString("AllowCyrillicLatin", resourceCulture);
            }
        }

        /// <summary>
        ///   Ищет локализованную строку, похожую на В поле {0} можно вводить строчные, прописные буквы (кириллица / латиница) и цифры.
        /// </summary>
        internal static string AllowCyrillicLatinAndNumbers
        {
            get
            {
                return ResourceManager.GetString("AllowCyrillicLatinAndNumbers", resourceCulture);
            }
        }

        /// <summary>
        ///   Ищет локализованную строку, похожую на В поле {0} допустимы только буквы, пробел и точка.
        /// </summary>
        internal static string AllowUnicodeLettersSpaceAndPoint
        {
            get
            {
                return ResourceManager.GetString("AllowUnicodeLettersSpaceAndPoint", resourceCulture);
            }
        }

        /// <summary>
        ///   Ищет локализованную строку, похожую на Элемент может изменяться только пользоватлем, создавшим его.
        /// </summary>
        internal static string CanModifiedOnlyByOwner
        {
            get
            {
                return ResourceManager.GetString("CanModifiedOnlyByOwner", resourceCulture);
            }
        }

        /// <summary>
        ///   Ищет локализованную строку, похожую на Email &apos;{0}&apos; занят..
        /// </summary>
        internal static string DuplicateEmail
        {
            get
            {
                return ResourceManager.GetString("DuplicateEmail", resourceCulture);
            }
        }

        /// <summary>
        ///   Ищет локализованную строку, похожую на {0} занято..
        /// </summary>
        internal static string DuplicateName
        {
            get
            {
                return ResourceManager.GetString("DuplicateName", resourceCulture);
            }
        }

        /// <summary>
        ///   Ищет локализованную строку, похожую на Номер телефона &apos;{0}&apos; занят..
        /// </summary>
        internal static string DuplicatePhone
        {
            get
            {
                return ResourceManager.GetString("DuplicatePhone", resourceCulture);
            }
        }

        /// <summary>
        ///   Ищет локализованную строку, похожую на Неправильный номер телефона.
        /// </summary>
        internal static string IncorrectPhoneNumber
        {
            get
            {
                return ResourceManager.GetString("IncorrectPhoneNumber", resourceCulture);
            }
        }

        /// <summary>
        ///   Ищет локализованную строку, похожую на Значение в поле {0} не удовлетворяет требованиям.
        /// </summary>
        internal static string IncorrectRegExp
        {
            get
            {
                return ResourceManager.GetString("IncorrectRegExp", resourceCulture);
            }
        }

        /// <summary>
        ///   Ищет локализованную строку, похожую на Email &apos;{0}&apos; не допустим..
        /// </summary>
        internal static string InvalidEmail
        {
            get
            {
                return ResourceManager.GetString("InvalidEmail", resourceCulture);
            }
        }

        /// <summary>
        ///   Ищет локализованную строку, похожую на {0} не корректно, может содержать только буквы и\или цифры..
        /// </summary>
        internal static string InvalidUserName
        {
            get
            {
                return ResourceManager.GetString("InvalidUserName", resourceCulture);
            }
        }

        /// <summary>
        ///   Ищет локализованную строку, похожую на Значение {0} должно содержать не более 3 цифр..
        /// </summary>
        internal static string LowerThan3Number
        {
            get
            {
                return ResourceManager.GetString("LowerThan3Number", resourceCulture);
            }
        }

        /// <summary>
        ///   Ищет локализованную строку, похожую на В поле {0} допустимы только буквы кириллического алфавита.
        /// </summary>
        internal static string OnlyCyrillic
        {
            get
            {
                return ResourceManager.GetString("OnlyCyrillic", resourceCulture);
            }
        }

        /// <summary>
        ///   Ищет локализованную строку, похожую на В поле {0} допустимы только цифры.
        /// </summary>
        internal static string OnlyInteger
        {
            get
            {
                return ResourceManager.GetString("OnlyInteger", resourceCulture);
            }
        }

        /// <summary>
        ///   Ищет локализованную строку, похожую на Неправильный номер телефона, требуется формат &quot;89998887766&quot;.
        /// </summary>
        internal static string PhoneValidation
        {
            get
            {
                return ResourceManager.GetString("PhoneValidation", resourceCulture);
            }
        }

        /// <summary>
        ///   Ищет локализованную строку, похожую на {0} не может быть пустым..
        /// </summary>
        internal static string PropertyTooShort
        {
            get
            {
                return ResourceManager.GetString("PropertyTooShort", resourceCulture);
            }
        }

        /// <summary>
        ///   Ищет локализованную строку, похожую на Значение {0} должно быть больше {1}..
        /// </summary>
        internal static string RangeMin
        {
            get
            {
                return ResourceManager.GetString("RangeMin", resourceCulture);
            }
        }

        /// <summary>
        ///   Ищет локализованную строку, похожую на Значение {0} должно содержать не более {1} символов..
        /// </summary>
        internal static string StringLengthValidation
        {
            get
            {
                return ResourceManager.GetString("StringLengthValidation", resourceCulture);
            }
        }

        /// <summary>
        ///   Ищет локализованную строку, похожую на Значение {0} должно содержать не менее {2} символов..
        /// </summary>
        internal static string StringMinLengthValidation
        {
            get
            {
                return ResourceManager.GetString("StringMinLengthValidation", resourceCulture);
            }
        }
    }
}