﻿using Library.Database.Domain;
using Library.Domain.Logging;
using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Threading.Tasks;

namespace Library.Domain.Models
{
    public class LogableDbContext : ApplicationDbContext
    {
        private bool IsInTransaction => Database.CurrentTransaction != null;
        public EntityLogManager EntityLogManager { get; set; }
        public bool LogManagerDisable { get; set; }

        protected bool IsInitSavingChangesEvent { get; set; }

        public override int SaveChanges()
        {
            LogManageInit();
            var result = base.SaveChanges();
            SaveLogs();
            return result;
        }

        public void BuildChangesByState(object entity, EntityState deleted)
        {
            if (LogManagerDisable)
            {
                return;
            }
            LogManageInit();
            EntityLogManager?.BuildChangesByState(entity, EntityState.Deleted);
        }

        public async override Task<int> SaveChangesAsync()
        {
            LogManageInit();
            var result = await base.SaveChangesAsync();
            SaveLogs();
            return result;
        }

        protected void LogManageInit()
        {
            if (LogManagerDisable)
            {
                return;
            }

            EntityLogManager?.Init(this);

            InitObjectContextSavingChangesEvent();
        }

        private void InitObjectContextSavingChangesEvent()
        {
            if (!IsInitSavingChangesEvent)
            {
                var objectContextAdapter = this as IObjectContextAdapter;
                objectContextAdapter.ObjectContext.SavingChanges += ObjectContext_SavingChanges;
                IsInitSavingChangesEvent = true;
            }
        }

        private void ObjectContext_SavingChanges(object sender, EventArgs e)
        {
            EntityLogManager?.BuildChanges();
        }

        protected void SaveLogs()
        {
            if (IsInTransaction)
            {
                return;
            }
            SaveLogsImplementation();
        }

        public void SaveLogsImplementation()
        {
            if (LogManagerDisable)
            {
                return;
            }
            EntityLogManager?.SaveLogs();
        }

        public void ClearLogs()
        {
            if (LogManagerDisable)
            {
                return;
            }
            EntityLogManager?.Clear();
        }
    }
}