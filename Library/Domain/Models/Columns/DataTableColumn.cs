﻿namespace Library.Domain.Models.Columns
{
    public class DataTableColumn
    {
        public string Data { get; set; }
        public ColumnSearch Search { get; set; }
        public bool CheckValueContains { get; set; }
    }
}