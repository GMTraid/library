﻿namespace Library.Domain.Models.Columns
{
    public class ColumnSearch
    {
        public string Value { get; set; }
        public bool Regex { get; set; }
    }
}