﻿namespace Library.Domain.Models.Columns
{
    public class DataTableOrderInfo
    {
        public int Column { get; set; }
        public string Dir { get; set; }
        public bool NullsInEnd { get; set; }
    }
}