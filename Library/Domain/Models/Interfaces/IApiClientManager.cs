﻿using Library.Models;

namespace Library.Domain.Attributes
{
    public interface IApiClientManager
    {
        /// <summary>
        /// Метод для получения WebApi клиента
        /// </summary>
        /// <param name="clientNumber"> Номер клиента </param>
        BaseApiClient GetClient(string clientNumber);

        /// <summary>
        /// Метод по установке последнего Nonce клиенту
        /// </summary>
        void SetLastNonce(BaseApiClient client, long nonce);
    }
}
