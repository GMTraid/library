﻿namespace Library.Domain.Models.Interfaces
{
    public interface IOwner<TKey>
    {
        TKey OwnerId { get; set; }
    }
}
