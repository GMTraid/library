﻿using Library.Database.Interfaces.Base;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Library.Domain.Models.Interfaces
{
    public interface IRepository<T, TKey> where T : class, IEntity<TKey>
    {
        void Create(T entity);
        Task CreateAsync(T entity);
        void CreateRange(IEnumerable<T> entities);
        Task CreateRangeAsync(IEnumerable<T> entities);
        Task CreateOrUpdateAsync(T entity);
        bool IsEmptyKey(TKey id);
        void Delete(T entity);
        Task DeleteAsync(T entity);
        void Delete(TKey id);
        void DeleteRange(IEnumerable<T> entities);
        Task DeleteRangeAsync(IEnumerable<T> entities);
        T Get(TKey id);
        Task<T> GetAsync(TKey id);
        IQueryable<T> GetAll();
        IQueryable<T> GetAllForOwner<TOwnerKey>(TOwnerKey ownerId);
        IQueryable<T> GetAllNoTracking();
        void Update(T entity);
        Task UpdateAsync(T entity);
        void UpdateRange(IEnumerable<T> entities);
        Task UpdateRangeAsync(IEnumerable<T> entities);
        void SetModifiedAndUpdate(T entity);

        /// <summary>
        /// Освободить сущность. Перестать отслеживать.
        /// </summary>
        void Detach(T entity);

        void Attach(T entity);
        void AttachRange(IEnumerable<T> entities);

        /// <summary>
        /// Освободить сущности. Перестать отслеживать.
        /// </summary>
        void DetachRange(IEnumerable<T> entities);
        T Load(TKey id);
        Task BatchInsertAsync(IEnumerable<T> entities, int? batchSize = null);
    }
}
