﻿using Castle.Windsor;
using Quartz;
using Quartz.Simpl;
using Quartz.Spi;
using System.Linq;

namespace Library.Domain.Tasks.JobFactory
{
    public class WindsorJobFactory : SimpleJobFactory
    {
        private readonly IWindsorContainer _container;

        public WindsorJobFactory(IWindsorContainer container)
        {
            _container = container;
        }

        public override IJob NewJob(TriggerFiredBundle bundle, IScheduler scheduler)
        {
            var job = _container.ResolveAll(bundle.JobDetail.JobType).Cast<IJob>().FirstOrDefault();
            return job;
        }

        public override void ReturnJob(IJob job)
        {
            _container.Release(job);
        }
    }
}