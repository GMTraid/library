﻿using Castle.MicroKernel.Lifestyle;
using Library.App_Start;
using Library.Domain.Config;
using Library.Domain.Tasks.JobFactory;
using Library.Tasks.Intefaces;
using NLog;
using Quartz;
using Quartz.Impl;
using System;
using System.Web.Hosting;

namespace Library.Domain.Tasks
{
    public class ApplicationPreload : IProcessHostPreloadClient
    {
        protected (BaseTaskConfig Config, Type TaskType) GetTasks(LibraryConfiguration config) => (config.SendMessageTask, typeof(SendMessageTask));

        public async void Preload(string[] parameters)
        {
            var logger = LogManager.GetCurrentClassLogger();
            try
            {
                logger.Info("ApplicationPreload - Start");

                var container = WebApiApplication.InitApplication();
                var jobFactory = new WindsorJobFactory(container);

                var scheduler = await new StdSchedulerFactory().GetScheduler();
                scheduler.JobFactory = jobFactory;

                var config = container.Resolve<LibraryConfiguration>();

                var tasks = GetTasks(config);

                if (tasks.Config.Enabled)
                {
                    await scheduler.ScheduleJob(
                        JobBuilder.Create(tasks.TaskType).Build(),
                        TriggerBuilder.Create()
                            .WithCronSchedule(tasks.Config.CronString)
                            .Build());
                }

                using (container.BeginScope())
                {
                    var jobDispatcher = container.Resolve<IJobDispatcher>();
                    await jobDispatcher.InitJobs();
                    container.Release(jobDispatcher);
                }

                await scheduler.Start();
                logger.Info("ApplicationPreload - Run");
            }
            catch (Exception ex)
            {
                logger.Error(ex);
            }
        }
    }
}