﻿using Library.Domain.Models.Columns;
using Library.Domain.Models.Filters;
using Library.Domain.Params.RepresentInfo;
using System.Collections.Generic;
using System.Linq;

namespace Library.Domain.Params
{
    public class DataTableLoadParams : LoadParams
    {
        public DataTableColumn[] Columns { get; set; }
        public DataTableOrderInfo[] Order { get; set; }
        public OrderInfo[] OrderInfo { get; set; }
        public FilterInfo[] Filters { get; set; }

        public int Draw { get; set; }

        public override OrderInfo[] GetOrdersInfo()
        {
            return OrderInfo ?? Order?.Select(x => new OrderInfo
            {
                DataIndex = Columns[x.Column].Data,
                IsDesc = x.Dir?.Trim().ToUpper() == "DESC",
                NullsInEnd = x.NullsInEnd
            })
            .ToArray() ?? new OrderInfo[0];
        }

        public override FilterInfo[] GetFiltersInfo()
        {
            return Filters ?? Columns?.Select(x => new FilterInfo
            {
                DataIndex = x.Data,
                Value = x.Search?.Value,
                CheckValueContains = x.CheckValueContains
            })
            .ToArray() ?? new FilterInfo[0];
        }

        public static FilterInfo Or(List<FilterInfo> filters)
        {
            if (filters.Count() == 1)
            {
                return filters[0];
            }

            var left = filters[0];
            filters.RemoveAt(0);
            return new FilterInfo
            {
                Left = left,
                Right = Or(filters),
                Action = FilterActions.Or
            };
        }
    }
}