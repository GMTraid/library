﻿namespace Library.Domain.Params
{
    public class DeleteParams
    {
        public long Id { get; set; }
    }
}