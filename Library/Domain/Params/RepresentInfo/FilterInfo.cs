﻿namespace Library.Domain.Params.RepresentInfo
{
    public class FilterInfo
    {

        public string DataIndex { get; set; }

        public string Value { get; set; }

        public bool CheckValueContains { get; set; }

        public string Action { get; set; }

        public FilterInfo Left { get; set; }
        public FilterInfo Right { get; set; }
    }
}