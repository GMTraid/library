﻿namespace Library.Domain.Params.RepresentInfo
{
    public class OrderInfo
    {
        public string DataIndex { get; set; }
        public bool IsDesc { get; set; }
        public bool NullsInEnd { get; set; }
    }
}