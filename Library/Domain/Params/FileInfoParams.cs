﻿namespace Library.Domain.Params
{
    public class FileInfoParams
    {
        public string FileName { get; set; }
        public byte[] Buffer { get; set; }
        public long Size { get; set; }
    }
}