﻿using Library.Domain.Params.RepresentInfo;

namespace Library.Domain.Params
{
    public abstract class LoadParams
    {
        public virtual int Start { get; set; }
        public virtual int Length { get; set; }

        public abstract OrderInfo[] GetOrdersInfo();
        public abstract FilterInfo[] GetFiltersInfo();
    }
}