﻿using System.Collections.Generic;

namespace Library.Domain.Params
{
    public class BaseParams
    {
        public IDictionary<string, object> Params { get; set; }
        public IDictionary<string, FileInfoParams> Files { get; set; }
    }
}