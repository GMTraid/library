﻿using Library.Database.Interfaces;

namespace Library.Domain.Services.Interfaces
{
    public interface IBindingService<T> : IBindingService<T, long> where T : class, IEntity
    {
    }
}
