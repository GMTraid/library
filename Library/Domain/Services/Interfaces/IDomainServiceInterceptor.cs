﻿using Library.Database.Interfaces.Base;
using System.Threading.Tasks;

namespace Library.Domain.Services.Interfaces
{
    public interface IDomainServiceInterceptor<T, TKey> where T : class, IEntity<TKey>
    {
        Task BeforeCreateAsync(T entity);
        Task AfterCreateAsync(T entity);

        Task BeforeUpdateAsync(T entity);
        Task AfterUpdateAsync(T entity);

        Task BeforeDeleteAsync(T entity);
        Task AfterDeleteAsync(T entity);
    }
}
