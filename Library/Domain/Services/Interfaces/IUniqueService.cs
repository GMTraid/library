﻿namespace Library.Domain.Services.Interfaces
{
    public interface IUniqueService
    {
        string GetErrorMessage<T>(string pgErrorDetail);
    }
}
