﻿using Library.Database.Interfaces.Base;
using Library.Domain.Models.Interfaces;
using System.Linq;
using System.Threading.Tasks;

namespace Library.Domain.Services.Interfaces.Domain.Base
{
    public interface IDomainService<T, TKey> where T : class, IEntity<TKey>
    {
        IRepository<T, TKey> Repository { get; set; }

        Task CreateOrUpdateAsync(T entity);
        Task CreateAsync(T entity);
        Task DeleteAsync(T entity);
        Task DeleteAsync(TKey id);
        Task UpdateAsync(T entity);

        bool IsEmptyKey(TKey id);

        IQueryable<T> GetAll();
        IQueryable<T> GetAllForOwner<TOwnerKey>(TOwnerKey ownerId);
        T Get(TKey id);
        Task<T> GetAsync(TKey id);
    }
}
