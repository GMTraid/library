﻿using Library.Database.Interfaces;
using Library.Domain.Services.Interfaces.Domain.Base;

namespace Library.Domain.Services.Interfaces.Domain
{
    public interface IDomainService<T> : IDomainService<T, long> where T : class, IEntity
    {
    }
}