﻿using Library.Database.Entities;
using Library.Domain.Params;
using System.IO;

namespace Library.Domain.Services.Interfaces
{
    public interface IFileService
    {
        FileWrapper CreateFileWrapper(FileInfoParams fileInfoParams);
        FileWrapper CreateFileWrapper(string fileNameAndExtension, string fileContent);
        FileWrapper CreateFileWrapper(string fileNameAndExtension, byte[] buffer, long size);
        FileStream GetFileStream(long fileWrapperId);
        FileStream GetFileStream(FileWrapper fileWrapper);
        FileStream GetFileStream(string fullNameInStore);
        GetFileResult GetFile(string fullNameInStore);
        /// <summary>
        /// Возвращает эскиз картинки
        /// </summary>
        /// <param name="maxSideSize">максимальный размер стороны эскиза </param>
        GetFileResult GetImageThumbnail(string fullNameInStore, float maxSideSize);
        string GetLocalPath(string fullNameInStore);
    }
}
