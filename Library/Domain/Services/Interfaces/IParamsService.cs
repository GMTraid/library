﻿namespace Library.Domain.Services.Interfaces
{
    public interface IParamsService
    {
        void Add<T>(T param);
        T Get<T>(T defaultValue = default(T));
    }
}
