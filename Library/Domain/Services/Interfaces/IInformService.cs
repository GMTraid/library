﻿using Library.Database.Enums;
using Library.Domain.Models;
using System.Collections.Generic;

namespace Library.Domain.Services.Interfaces
{
    public interface IInformService
    {
        void Add(string message, InformType type);
        List<InformModel> Get();
    }
}