﻿namespace Library.Domain.Services.Interfaces
{
    public interface IBaseDbExceptionService
    {
        string GetErrorMessage<T>(string pgErrorDetail);
    }
}
