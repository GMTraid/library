﻿using Library.Database.Interfaces.Base;

namespace Library.Domain.Services.Interfaces
{
    /// <summary>
    /// Переносит значения из вьюхи в сущность
    /// </summary>
    public interface IBindingService<T, TKey> : Binding.IBindingService<TKey> where T : class, IEntity<TKey>
    {
        T Bind<U>(T originalObject, U vmObject);
        T Bind<U>(U vmObject) where U : IEntity<TKey>;
    }
}
