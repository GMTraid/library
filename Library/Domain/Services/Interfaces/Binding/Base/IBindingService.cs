﻿using Library.Database.Interfaces.Base;

namespace Library.Domain.Services.Interfaces.Binding
{
    public interface IBindingService<TKey>
    {
        object BindObject<U>(U vmObject) where U : IEntity<TKey>;
        object BindObject<U>(object originalObject, U vmObject) where U : IEntity<TKey>;
    }
}
