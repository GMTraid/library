﻿using System;
using System.Threading.Tasks;

namespace Library.Domain.Services.Interfaces.Transaction
{
    /// <summary>
    /// Сервис для выполнения кода внутри транзакции, с автоматическим коммитом и ревертом.
    /// </summary>
    public interface ITransactionService : IService
    {
        /// <summary>
        /// Выполнить код внутри транзакции
        /// </summary>
        void InTransaction(Action action, Action<Exception> errorAction = null);

        /// <summary>
        /// Выполнить код внутри транзакции
        /// </summary>
        Task InTransactionAsync(Func<Task> action, Func<Exception, Task> errorAction = null);
        Task<T> InTransactionAsync<T>(Func<Task<T>> action, Func<Exception, Task> errorAction = null);
    }
}
