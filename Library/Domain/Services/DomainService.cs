﻿using Castle.Windsor;
using Library.Database.Interfaces.Base;
using Library.Domain.Models.Interfaces;
using Library.Domain.Services.Interfaces;
using Library.Domain.Services.Interfaces.Domain.Base;
using Library.Domain.Services.Interfaces.Transaction;
using System.Linq;
using System.Threading.Tasks;

namespace Library.Domain.Services
{
    public class DomainService<T, TKey> : IDomainService<T, TKey> where T : class, IEntity<TKey>
    {
        public IWindsorContainer Container { get; set; }
        public IRepository<T, TKey> Repository { get; set; }
        public ITransactionService TransactinService { get; set; }

        public virtual async Task CreateOrUpdateAsync(T entity)
        {
            if (IsEmptyKey(entity.Id))
            {
                await CreateAsync(entity);
            }
            else
            {
                await UpdateAsync(entity);
            }
        }

        public virtual bool IsEmptyKey(TKey id)
        {
            return Repository.IsEmptyKey(id);
        }

        public virtual async Task CreateAsync(T entity)
        {
            await TransactinService.InTransactionAsync(async () =>
            {
                var interceptors = ResolveInterceptors();

                await BeforeCreateInterceptorsAsync(interceptors, entity);
                CreateImmediately(entity);
                await AfterCreateInterceptorsAsync(interceptors, entity);

                ReleaseInterceptors(interceptors);
            });
        }

        public virtual async Task UpdateAsync(T entity)
        {
            await TransactinService.InTransactionAsync(async () =>
            {
                var interceptors = ResolveInterceptors();

                await BeforeUpdateInterceptorsAsync(interceptors, entity);
                UpdateImmediately(entity);
                await AfterUpdateInterceptorsAsync(interceptors, entity);

                ReleaseInterceptors(interceptors);
            });
        }

        public virtual async Task DeleteAsync(T entity)
        {
            await TransactinService.InTransactionAsync(async () =>
            {
                var interceptors = ResolveInterceptors();

                await BeforeDeleteInterceptorsAsync(interceptors, entity);
                DeleteImmediately(entity);
                await AfterDeleteInterceptorsAsync(interceptors, entity);

                ReleaseInterceptors(interceptors);
            });
        }

        public virtual async Task DeleteAsync(TKey id)
        {
            var entity = Repository.Get(id);
            await DeleteAsync(entity);
        }

        public IQueryable<T> GetAll()
        {
            return Repository.GetAll();
        }

        public IQueryable<T> GetAllForOwner<TOwnerKey>(TOwnerKey ownerId)
        {
            return Repository.GetAllForOwner(ownerId);
        }

        public T Get(TKey id)
        {
            return Repository.Get(id);
        }

        public Task<T> GetAsync(TKey id)
        {
            return Repository.GetAsync(id);
        }

        protected virtual void CreateImmediately(T entity)
        {
            Repository.Create(entity);
        }

        protected virtual void UpdateImmediately(T entity)
        {
            Repository.Update(entity);
        }

        protected virtual Task CreateImmediatelyAsync(T entity)
        {
            return Repository.CreateAsync(entity);
        }

        protected virtual Task UpdateImmediatelyAsync(T entity)
        {
            return Repository.UpdateAsync(entity);
        }

        protected virtual void DeleteImmediately(T entity)
        {
            Repository.Delete(entity);
        }

        protected async Task BeforeCreateInterceptorsAsync(IDomainServiceInterceptor<T, TKey>[] interceptors, T entity)
        {
            foreach (var interceptor in interceptors)
            {
                await interceptor.BeforeCreateAsync(entity);
            }
        }

        protected async Task AfterCreateInterceptorsAsync(IDomainServiceInterceptor<T, TKey>[] interceptors, T entity)
        {
            foreach (var interceptor in interceptors)
            {
                await interceptor.AfterCreateAsync(entity);
            }
        }


        protected async Task BeforeUpdateInterceptorsAsync(IDomainServiceInterceptor<T, TKey>[] interceptors, T entity)
        {
            foreach (var interceptor in interceptors)
            {
                await interceptor.BeforeUpdateAsync(entity);
            }
        }

        protected async Task AfterUpdateInterceptorsAsync(IDomainServiceInterceptor<T, TKey>[] interceptors, T entity)
        {
            foreach (var interceptor in interceptors)
            {
                await interceptor.AfterUpdateAsync(entity);
            }
        }

        protected async Task BeforeDeleteInterceptorsAsync(IDomainServiceInterceptor<T, TKey>[] interceptors, T entity)
        {
            foreach (var interceptor in interceptors)
            {
                await interceptor.BeforeDeleteAsync(entity);
            }
        }

        protected async Task AfterDeleteInterceptorsAsync(IDomainServiceInterceptor<T, TKey>[] interceptors, T entity)
        {
            foreach (var interceptor in interceptors)
            {
                await interceptor.AfterDeleteAsync(entity);
            }
        }

        protected IDomainServiceInterceptor<T, TKey>[] ResolveInterceptors()
        {
            return Container.ResolveAll<IDomainServiceInterceptor<T, TKey>>();
        }

        protected void ReleaseInterceptors(IDomainServiceInterceptor<T, TKey>[] interceptors)
        {
            foreach (var interceptor in interceptors)
            {
                Container.Release(interceptor);
            }
        }
    }
}