﻿using Library.Database.Enums;
using Library.Domain.Models;
using Library.Domain.Services.Interfaces;
using System.Collections.Generic;

namespace Library.Domain.Services
{
    public class InformService : IInformService
    {
        protected List<InformModel> InformContainer = new List<InformModel>();

        public void Add(string message, InformType type)
        {
            InformContainer.Add(new InformModel(message, type));
        }

        public List<InformModel> Get()
        {
            return InformContainer;
        }
    }
}