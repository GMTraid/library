﻿using Library.Database.Entities;
using System.IO;

namespace Library.Domain.Services
{
    public class GetFileResult
    {
        public FileWrapper FileWrapper { get; set; }
        public Stream FileStream { get; set; }
        public string FullFileName => Path.ChangeExtension(FileWrapper.FileName, FileWrapper.Extension);
    }
}