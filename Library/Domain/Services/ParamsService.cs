﻿using Library.Domain.Services.Interfaces;
using System;
using System.Collections.Generic;

namespace Library.Domain.Services
{
    public class ParamsService : IParamsService
    {
        protected IDictionary<Type, object> ParamsContainer = new Dictionary<Type, object>();

        public void Add<T>(T param)
        {
            ParamsContainer[typeof(T)] = param;
        }

        public T Get<T>(T defaultValue = default(T))
        {
            if (ParamsContainer.TryGetValue(typeof(T), out object result))
            {
                return (T)result;
            }

            return defaultValue;
        }
    }
}