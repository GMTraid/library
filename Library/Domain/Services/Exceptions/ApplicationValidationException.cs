﻿using System;

namespace Library.Domain.Services.Exceptions
{
    public class ApplicationValidationException : Exception
    {
        public ApplicationValidationException()
        {
        }

        public ApplicationValidationException(string message) : base(message)
        {
        }
    }
}