﻿using Library.Database.Interfaces;
using Library.Domain.Services.Interfaces;

namespace Library.Domain.Services
{
    public class BindingService<T> : BindingService<T, long>, IBindingService<T> where T : class, IEntity, new()
    {
    }
}