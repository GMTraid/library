﻿using Library.Database.Interfaces;
using Library.Domain.Services.Exceptions;
using Library.Domain.Services.Interfaces;
using Library.Domain.Services.Interfaces.Domain;
using Npgsql;
using System.Data.Entity.Infrastructure;
using System.Threading.Tasks;

namespace Library.Domain.Services
{
    public class BaseDomainService<T> : DomainService<T, long>, IDomainService<T> where T : class, IEntity
    {
        public IUniqueService UniqueService { get; set; }
        public IForeignKeyService ForeignKeyService { get; set; }

        private const string DublicatePrimaryKeyExcCode = "23505";
        private const string ForeignKeyConstraintExcCode = "23503";

        public override async Task CreateAsync(T entity)
        {
            try
            {
                await base.CreateAsync(entity);
            }
            catch (DbUpdateException ex)
            {
                var postgresExc = ex.GetBaseException() as PostgresException;
                if (postgresExc != null)
                {
                    switch (postgresExc.SqlState)
                    {
                        case DublicatePrimaryKeyExcCode:
                            {
                                var message = UniqueService.GetErrorMessage<T>(postgresExc.Detail);
                                throw new ApplicationValidationException(message);
                            }
                        case ForeignKeyConstraintExcCode:
                            {
                                var message = ForeignKeyService.GetErrorMessage<T>(postgresExc.Detail);
                                throw new ApplicationValidationException(message);
                            }
                        default:
                            break;
                    }
                }

                throw;
            }
        }

        public override async Task UpdateAsync(T entity)
        {
            try
            {
                await base.UpdateAsync(entity);
            }
            catch (DbUpdateException ex) when (ex.GetBaseException() is PostgresException exeption && exeption.SqlState == "23505")
            {
                var message = UniqueService.GetErrorMessage<T>(exeption.Detail);
                throw new ApplicationValidationException(message);
            }
        }
    }
}