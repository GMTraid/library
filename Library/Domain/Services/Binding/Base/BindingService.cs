﻿using Castle.Windsor;
using Library.Database.Interfaces.Base;
using Library.Domain.Models;
using Library.Domain.Models.Interfaces;
using Library.Domain.Services.Interfaces;
using Library.Domain.Services.Interfaces.Binding;
using Library.Extensions.WindsorContainer;
using System;
using System.Linq;
using System.Reflection;

namespace Library.Domain.Services.Binding
{
    /// <summary>
    /// Переносит значения из вьюхи в сущность
    /// </summary>
    public class BindingService<T, TKey> : IBindingService<T, TKey> where T : class, IEntity<TKey>, new()
    {
        public IWindsorContainer Container { get; set; }
        public IRepository<TKey> Repository { get; set; }
        public IRepository<T, TKey> RsT { get; set; }

        protected Type EntityType = typeof(IEntity<TKey>);
        protected Type KeyType = typeof(TKey);

        public virtual T Bind<U>(U vmObject) where U : IEntity<TKey>
        {
            var originalObject = RsT.Get(vmObject.Id);
            return Bind(originalObject, vmObject);
        }

        public virtual T Bind<U>(T originalObject, U vmObject)
        {
            var type = typeof(T);
            var vmType = vmObject.GetType();
            var ignoreType = typeof(IEntity<TKey>);

            var ignorePropertyNames = ignoreType.GetProperties()
                .Select(x => x.Name)
                .ToArray();

            var vmProperties = vmType.GetProperties()
                .Where(x => !ignorePropertyNames.Contains(x.Name))
                .Where(IsBindingPropertyType);

            foreach (var vmProperty in vmProperties)
            {
                var vmValue = vmProperty.GetValue(vmObject);
                var property = type.GetProperty(vmProperty.Name);
                if (property != null)
                {
                    var originalValue = property.GetValue(originalObject);
                    var propertyType = property.PropertyType;

                    if (IsEntityType(propertyType))
                    {
                        SetEntityPropertyValue(originalObject, vmProperty, vmValue, property, originalValue, propertyType);
                    }
                    else
                    {
                        if (!object.Equals(vmValue, originalValue))
                        {
                            property.SetValue(originalObject, vmValue);
                        }
                    }
                }
            }
            return originalObject;
        }

        protected virtual void SetEntityPropertyFileValue(T originalObject, PropertyInfo vmProperty, object vmValue, PropertyInfo property, Type propertyType, IEntity<TKey> originalPropertyEntity)
        {
            throw new NotImplementedException();
        }

        protected virtual bool IsFileInfoType(Type type)
        {
            return false;
        }

        protected virtual bool IsFileInfoType(object value)
        {
            return false;
        }

        private void SetEntityPropertyValue(T originalObject, PropertyInfo vmProperty, object vmValue, PropertyInfo property, object originalValue, Type propertyType)
        {
            var originalPropertyEntity = (IEntity<TKey>)originalValue;
            if (IsFileInfoType(vmValue))
            {
                SetEntityPropertyFileValue(originalObject, vmProperty, vmValue, property, propertyType, originalPropertyEntity);
            }
            else if (IsKeyType(vmValue))
            {
                SetReferencePropertyValueByKey(originalObject, (TKey)vmValue, property, propertyType, originalPropertyEntity);
            }
            else if (IsEntityType(vmValue))
            {
                SetReferencePropertyValueByEntity(originalObject, vmProperty, (IEntity<TKey>)vmValue, property, propertyType, originalPropertyEntity);
            }
            else if (vmValue == null)
            {
                if (originalValue != null)
                {
                    property.SetValue(originalObject, null);
                }
            }
            else
            {
                throw new Exception("The wrong type of key");
            }
        }

        /// <summary>
        /// Установить значение ссылочного свойства по значению типа "сущность"
        /// </summary>
        private void SetReferencePropertyValueByEntity(T originalObject, PropertyInfo vmProperty, IEntity<TKey> vmValueEntity, PropertyInfo property, Type propertyType, IEntity<TKey> originalPropertyEntity)
        {
            var binder = GetNewBinder(vmProperty, propertyType);

            var vmPropertyEntity = object.Equals(vmValueEntity.Id, default(TKey)) ?
                (IEntity<TKey>)binder.BindObject(null, vmValueEntity) :
                (IEntity<TKey>)Repository.Get(propertyType, vmValueEntity.Id);

            SetReferencePropertyValue(originalObject, property, vmPropertyEntity, originalPropertyEntity);
        }

        /// <summary>
        /// Установить значение ссылочного свойства по ключу
        /// </summary>
        private void SetReferencePropertyValueByKey(T originalObject, TKey keyValue, PropertyInfo property, Type propertyType, IEntity<TKey> originalPropertyEntity)
        {
            var vmPropertyEntity = (IEntity<TKey>)Repository.Get(propertyType, keyValue);
            SetReferencePropertyValue(originalObject, property, vmPropertyEntity, originalPropertyEntity);
        }

        /// <summary>
        /// Является типом ключа
        /// </summary>
        private bool IsKeyType(object vmValue)
        {
            return KeyType.IsInstanceOfType(vmValue);
        }

        /// <summary>
        /// Является типом "сущность"
        /// </summary>
        private bool IsEntityType(Type type)
        {
            return EntityType.IsAssignableFrom(type);
        }

        /// <summary>
        /// Является типом "сущность"
        /// </summary>
        private bool IsEntityType(object value)
        {
            return EntityType.IsInstanceOfType(value);
        }

        private Interfaces.Binding.IBindingService<TKey> GetNewBinder(PropertyInfo vmProperty, Type propertyType)
        {
            var vmPropertyType = vmProperty.PropertyType;
            var binderType = typeof(IBindingService<,>);
            binderType = binderType.MakeGenericType(propertyType, KeyType);
            var binder = (Interfaces.Binding.IBindingService<TKey>)Container.ResolveAndRelease(binderType);
            return binder;
        }

        protected void SetReferencePropertyValue(T originalObject, PropertyInfo property, IEntity<TKey> vmPropertyEntity, IEntity<TKey> originalPropertyEntity)
        {
            if (vmPropertyEntity == null)
            {
                throw new Exception("Entity with specified ID not found");
            }
            if (originalPropertyEntity == null || !object.Equals(originalPropertyEntity.Id, vmPropertyEntity.Id))
            {
                property.SetValue(originalObject, vmPropertyEntity);
            }
        }

        protected bool IsBindingPropertyType(PropertyInfo property)
        {
            var propertyType = property.PropertyType;
            if (EntityType.IsAssignableFrom(propertyType))
            {
                return true;
            }
            if (IsNullable(propertyType))
            {
                propertyType = Nullable.GetUnderlyingType(propertyType);
            }
            return IsBindingType(propertyType);
        }

        private bool IsBindingType(Type propertyType)
        {
            return propertyType.IsPrimitive || propertyType.IsEnum || BindingTypes.Contains(propertyType);
        }

        protected bool IsNullable(Type type)
        {
            return type.IsGenericType && type.GetGenericTypeDefinition() == typeof(Nullable<>);
        }

        public object BindObject<U>(U vmObject) where U : IEntity<TKey>
        {
            return Bind(vmObject);
        }

        public object BindObject<U>(object originalObject, U vmObject) where U : IEntity<TKey>
        {
            var originalTypedObject = (T)originalObject;
            if (originalTypedObject == null)
            {
                originalTypedObject = new T();
            }
            return Bind(originalTypedObject, vmObject);
        }

        protected virtual Type[] BindingTypes => new[] { typeof(decimal), typeof(string), typeof(DateTime), typeof(DateTimeOffset) };
    }
}