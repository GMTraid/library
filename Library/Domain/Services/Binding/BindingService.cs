﻿using Library.Database.Interfaces.Base;
using Library.Domain.Params;
using Library.Domain.Services.Interfaces;
using System;
using System.Linq;
using System.Reflection;

namespace Library.Domain.Services
{
    public class BindingService<T, TKey> : Binding.BindingService<T, TKey> where T : class, IEntity<TKey>, new()
    {
        public IFileService FileService { get; set; }

        protected Type FileInfoType => typeof(FileInfoParams);

        protected override bool IsFileInfoType(Type type)
        {
            ValidateKeyType();

            return FileInfoType.IsAssignableFrom(type);
        }

        protected override bool IsFileInfoType(object value)
        {
            ValidateKeyType();

            return FileInfoType.IsInstanceOfType(value);
        }

        protected override void SetEntityPropertyFileValue(T originalObject, PropertyInfo vmProperty, object vmValue, PropertyInfo property, Type propertyType, IEntity<TKey> originalPropertyEntity)
        {
            ValidateKeyType();

            var fileInfoParams = (FileInfoParams)vmValue;
            var vmPropertyEntity = (IEntity<TKey>)FileService.CreateFileWrapper(fileInfoParams);
            SetReferencePropertyValue(originalObject, property, vmPropertyEntity, originalPropertyEntity);
        }

        private static void ValidateKeyType()
        {
            if (typeof(TKey) != typeof(long))
            {
                throw new NotImplementedException($"The method is not implemented for the type of key: {typeof(TKey).Name}");
            }
        }

        protected override Type[] BindingTypes => base.BindingTypes.Concat(new[] { typeof(FileInfoParams) }).ToArray();
    }
}