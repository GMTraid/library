﻿using System.Configuration;

namespace Library.Domain.Config
{
    public class LibraryConfiguration : ConfigurationSection
    {
        public static LibraryConfiguration GetConfig()
        {
            return (LibraryConfiguration)ConfigurationManager.GetSection("LibraryConfiguration") ?? new LibraryConfiguration();
        }

        [ConfigurationProperty("EnableEntityLogInFile", IsRequired = true)]
        public bool EnableEntityLogInFile => this["EnableEntityLogInFile"] as bool? ?? false;

        [ConfigurationProperty("ApiClientOptions", IsRequired = false)]
        public ApiClientOptions ApiClientOptions => this["ApiClientOptions"] as ApiClientOptions;

        [ConfigurationProperty("DbOptions", IsRequired = false)]
        public DbOptions DbOptions => this["DbOptions"] as DbOptions;

        [ConfigurationProperty("HttpClientFactory", IsRequired = false)]
        public HttpClientFactoryOptions HttpClientFactory => this["HttpClientFactory"] as HttpClientFactoryOptions;

        [ConfigurationProperty("Security", IsRequired = true)]
        public Security Security => this["Security"] as Security;

        [ConfigurationProperty(nameof(SendMessageTask), IsRequired = true)]
        public SendMessageTask SendMessageTask => this[nameof(SendMessageTask)] as SendMessageTask;
    }

    public class HttpClientFactoryOptions : ConfigurationElement
    {
        [ConfigurationProperty("UseTransientClient", IsRequired = true, DefaultValue = false)]
        public bool UseTransientClient => this["UseTransientClient"] as bool? ?? false;

        [ConfigurationProperty("ConnectionLeaseTimeout", IsRequired = false)]
        [IntegerValidator]
        public int ConnectionLeaseTimeout => this["ConnectionLeaseTimeout"] as int? ?? 0;
    }
    public class SendMessageTask : BaseTaskConfig
    {
        /// <summary>
        /// Interval for checking data and sending notification to e-mail
        /// </summary>
        [ConfigurationProperty("SaveIntervalSeconds", IsRequired = true)]
        [IntegerValidator]
        public int SaveIntervalSeconds => this["SaveIntervalSeconds"] as int? ?? 0;
    }

    public class Security : ConfigurationElement
    {
        [ConfigurationProperty("RootPassword", IsRequired = true)]
        public string RootPassword => this["RootPassword"] as string;
    }


    public class ApiClientOptions : ConfigurationElement
    {
        [ConfigurationProperty("Clients", IsRequired = true)]
        public string Clients => this["Clients"] as string;
    }

    public class DbOptions : ConfigurationElement
    {
        [ConfigurationProperty("BatchSize", IsRequired = true)]
        public int BatchSize => this["BatchSize"] as int? ?? 5000;
    }

    public class BaseEnableConfig : ConfigurationElement
    {
        [ConfigurationProperty("Enabled", IsRequired = true, DefaultValue = false)]
        public bool Enabled => this["Enabled"] as bool? ?? false;
    }

    public class BaseTaskConfig : BaseEnableConfig
    {
        [ConfigurationProperty("CronString", IsRequired = true)]
        public string CronString => this["CronString"] as string;
    }
}
