﻿using Library.Database.Entities.Identity;
using Library.Domain.Models;
using Library.Domain.Models.Interfaces;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Data.Entity;
using System.Data.Entity.SqlServer.Utilities;
using System.Threading.Tasks;

namespace Library.App_Start.IdentityConfig
{
    public class ApplicationUserStore : UserStore<User, Role, long, UserLogin, UserRole, UserClaim>
    {
        private readonly DbContext context;
        private readonly IRepository<UserRole, long> rsUserRole;
        private readonly IRepository<User, long> rsUser;

        public ApplicationUserStore(DbContext context) : base(context)
        {
            this.context = context;
            this.rsUserRole = new LogableRepository<UserRole, long>() { DbContext = context };
            this.rsUser = new LogableRepository<User, long>() { DbContext = context };
        }

        public override async Task RemoveFromRoleAsync(User user, string roleName)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            if (String.IsNullOrWhiteSpace(roleName))
            {
                throw new ArgumentException("Value cannot be null", "roleName");
            }
            var roleEntity = await context.Set<Role>().SingleOrDefaultAsync(r => r.Name.ToUpper() == roleName.ToUpper()).WithCurrentCulture();
            if (roleEntity != null)
            {
                var roleId = roleEntity.Id;
                var userId = user.Id;
                var userRole = await context.Set<UserRole>().FirstOrDefaultAsync(r => roleId.Equals(r.RoleId) && r.UserId.Equals(userId)).WithCurrentCulture();
                if (userRole != null)
                {
                    rsUserRole.Delete(userRole);
                }
            }
        }

        public override Task UpdateAsync(User user)
        {
            if (user == null)
            {
                throw new ArgumentNullException("user");
            }
            rsUser.Update(user);
            return Task.CompletedTask;
        }
    }
}