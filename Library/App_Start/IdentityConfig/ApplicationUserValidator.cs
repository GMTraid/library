﻿using Library.Database.Entities.Identity;
using Library.Domain.Models;
using Library.Extensions;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Globalization;
using System.Linq;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Library.App_Start.IdentityConfig
{
    public class ApplicationUserValidator<TUser> : UserValidator<TUser, long> where TUser : User
    {
        public bool EmailCanBeNull { get; set; }
        public bool PhoneIsUnique { get; set; }
        public bool PhoneCanBeNull { get; set; }

        private UserManager<TUser, long> Manager { get; set; }

        public ApplicationUserValidator(UserManager<TUser, long> manager) : base(manager)
        {
            Manager = manager;
        }

        public override async Task<IdentityResult> ValidateAsync(TUser item)
        {
            if (item == null)
            {
                throw new ArgumentNullException("item");
            }
            var errors = new List<string>();
            await ValidateUserName(item, errors);
            if (RequireUniqueEmail)
            {
                await ValidateEmail(item, errors);
            }
            if (PhoneIsUnique)
            {
                await ValidatePhone(item, errors);
            }
            if (errors.Count > 0)
            {
                return IdentityResult.Failed(errors.ToArray());
            }
            return IdentityResult.Success;
        }

        private async Task ValidateUserName(TUser user, List<string> errors)
        {
            if (string.IsNullOrWhiteSpace(user.UserName))
            {
                errors.Add(String.Format(CultureInfo.CurrentCulture, Resources.PropertyTooShort, "Login"));
            }
            else if (AllowOnlyAlphanumericUserNames && !Regex.IsMatch(user.UserName, @"^[A-Za-z0-9@_\.]+$"))
            {
                // If any characters are not letters or digits, its an illegal user name
                errors.Add(String.Format(CultureInfo.CurrentCulture, Resources.InvalidUserName, user.UserName));
            }
            else
            {
                var owner = await Manager.FindByNameAsync(user.UserName);
                if (owner != null && !EqualityComparer<long>.Default.Equals(owner.Id, user.Id))
                {
                    errors.Add(String.Format(CultureInfo.CurrentCulture, Resources.DuplicateName, user.UserName));
                }
            }
        }

        // Make sure email is not empty, valid, and unique
        private async Task ValidateEmail(TUser user, List<string> errors)
        {
            if (!user.Email.HasValue() && EmailCanBeNull)
            {
                return;
            }

            if (!user.Email.HasValue())
            {
                errors.Add(String.Format(CultureInfo.CurrentCulture, Resources.PropertyTooShort, "Email"));
                return;
            }
            try
            {
                var m = new MailAddress(user.Email);
            }
            catch (FormatException)
            {
                errors.Add(String.Format(CultureInfo.CurrentCulture, Resources.InvalidEmail, user.Email));
                return;
            }
            var owner = await Manager.FindByEmailAsync(user.Email);
            if (owner != null && !EqualityComparer<long>.Default.Equals(owner.Id, user.Id))
            {
                errors.Add(String.Format(CultureInfo.CurrentCulture, Resources.DuplicateEmail, user.Email));
            }
        }

        private async Task ValidatePhone(TUser user, List<string> errors)
        {
            if (!user.PhoneNumber.HasValue() && PhoneCanBeNull)
            {
                return;
            }

            if (!user.PhoneNumber.HasValue())
            {
                errors.Add(String.Format(CultureInfo.CurrentCulture, Resources.PropertyTooShort, "Phone number"));
                return;
            }

            var owner = await Manager.Users.Where(x => user.PhoneNumber == x.PhoneNumber).FirstOrDefaultAsync();
            if (owner != null && !EqualityComparer<long>.Default.Equals(owner.Id, user.Id))
            {
                errors.Add(String.Format(CultureInfo.CurrentCulture, Resources.DuplicatePhone, user.PhoneNumber));
            }
        }
    }
}