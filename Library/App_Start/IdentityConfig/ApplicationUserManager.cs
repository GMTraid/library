﻿using Library.Database.Domain;
using Library.Database.Entities.Identity;
using Library.Domain.Extensions;
using Library.Extensions;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin;
using System.Data.Entity;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Library.App_Start.IdentityConfig
{
    public class ApplicationUserManager : UserManager<User, long>
    {
        public ApplicationUserManager(IUserStore<User, long> store) : base(store)
        {
        }

        public async Task<ClaimsIdentity> CreateIdentityAsync(User user)
        {
            return await CreateIdentityAsync(user, DefaultAuthenticationTypes.ApplicationCookie);
        }

        public string GeneratePassword(int? length = null)
        {
            var passwordValidator = GetPasswordValidatorOrDefault();

            var commonLength = length ?? passwordValidator.RequiredLength;

            var requireDigit = passwordValidator.RequireDigit ? PasswordChars.Digit : PasswordChars.None;
            var requireLowercase = passwordValidator.RequireLowercase ? PasswordChars.Lowercase : PasswordChars.None;
            var requireUppercase = passwordValidator.RequireUppercase ? PasswordChars.Uppercase : PasswordChars.None;
            var requireNonLetterOrDigit = passwordValidator.RequireNonLetterOrDigit ? PasswordChars.NonLetterOrDigit : PasswordChars.None;

            return PasswordHelper.GeneratePassword(commonLength,
                PasswordChars.Digit | PasswordChars.Lowercase,
                requireDigit | requireLowercase | requireUppercase | requireNonLetterOrDigit);
        }

        public string GeneratePasswordDigit(int? length = null)
        {
            var passwordValidator = GetPasswordValidatorOrDefault();
            var commonLength = length ?? passwordValidator.RequiredLength;
            return PasswordHelper.GeneratePassword(commonLength, PasswordChars.Digit);
        }

        private PasswordValidator GetPasswordValidatorOrDefault()
        {
            return PasswordValidator as PasswordValidator ?? new PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = false,
                RequireDigit = false,
                RequireLowercase = false,
                RequireUppercase = false,
            };
        }

        public string GetLoginByPhoneOrByName(User user)
        {
            if (string.IsNullOrWhiteSpace(user.PhoneNumber))
            {
                return GetLoginByNames(user);
            }
            return user.PhoneNumber;
        }

        public string GetLoginByNames(User user)
        {
            var login = $"{user.FirstName?.Select(x => x.ToString()).FirstOrDefault()}{user.Patronymic?.Select(x => x.ToString()).FirstOrDefault()}{user.LastName}";
            var translitLogin = EnglishRussianConverter.TranslitToEnglish(login.ToLower());
            return translitLogin;
        }

        public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, IOwinContext context)
        {
            return Create(options, context.Get<ApplicationDbContext>());
        }

        public static ApplicationUserManager Create(IdentityFactoryOptions<ApplicationUserManager> options, DbContext dbContext)
        {
            var manager = new ApplicationUserManager(new ApplicationUserStore(dbContext));
            // Настройка логики проверки имен пользователей
            manager.UserValidator = new ApplicationUserValidator<User>(manager)
            {
                AllowOnlyAlphanumericUserNames = false,
                RequireUniqueEmail = true,
                EmailCanBeNull = true,
                PhoneIsUnique = false,
                PhoneCanBeNull = true
            };
            // Настройка логики проверки паролей
            manager.PasswordValidator = new PasswordValidator
            {
                RequiredLength = 6,
                RequireNonLetterOrDigit = false,
                RequireDigit = false,
                RequireLowercase = false,
                RequireUppercase = false,
            };

            var dataProtectionProvider = options?.DataProtectionProvider;
            if (dataProtectionProvider != null)
            {
                manager.UserTokenProvider = new DataProtectorTokenProvider<User, long>(dataProtectionProvider.Create("ASP.NET Identity"));
            }
            return manager;
        }
    }
}