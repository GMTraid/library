﻿using System;

namespace Library.App_Start
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class SkipAssemblyCheckAttribute : Attribute
    {
    }
}