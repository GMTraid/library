﻿using AutoMapper;
using Library.Database.Entities;
using Library.Domain.Params;
using Library.Models.File;
using System;
using System.Linq.Expressions;
using System.Reflection;

namespace Library.App_Start
{
    public class MapperConfig
    {
        public static void Init()
        {
            Mapper.Initialize(x =>
            {
                x.CreateMap<FileWrapper, FileWrapperModel>();

                x.CreateMap<FileWrapper, FileInfoParams>();

                x.AddProfiles(Assembly.GetExecutingAssembly());
            });
        }
    }

    public static class MappingExpressionExtension
    {
        public static IMappingExpression<T, U> ForMemberFrom<T, U, TMember, UMember>(this IMappingExpression<T, U> expression,
            Expression<Func<U, UMember>> uExpression, Expression<Func<T, TMember>> tExpression)
        {
            return expression.ForMember(uExpression, y => y.MapFrom(tExpression));
        }

        public static IMappingExpression<T, U> Ignore<T, U, UMember>(this IMappingExpression<T, U> expression,
            Expression<Func<U, UMember>> uExpression)
        {
            return expression.ForMember(uExpression, y => y.Ignore());
        }
    }
}