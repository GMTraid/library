﻿using Library.Extensions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;

namespace Library.App_Start
{
    public class CheckAssemblyAttribute : ActionFilterAttribute, IActionFilter
    {
        public override async Task OnActionExecutingAsync(HttpActionContext actionContext, CancellationToken cancellationToken)
        {
            if (SkipAssemblyCheck(actionContext))
            {
                await base.OnActionExecutingAsync(actionContext, cancellationToken);
                return;
            }

            int? statusCode = await CheckAssembly(actionContext.Request);
            if (statusCode == 499)
            {
                actionContext.Response = actionContext.Request.CreateResponse((HttpStatusCode)499);
            }
            await base.OnActionExecutingAsync(actionContext, cancellationToken);
        }

        public override async Task OnActionExecutedAsync(HttpActionExecutedContext actionExecutedContext, CancellationToken cancellationToken)
        {
            await base.OnActionExecutedAsync(actionExecutedContext, cancellationToken);

            if (SkipAssemblyCheck(actionExecutedContext.ActionContext))
            {
                return;
            }

            int? statusCode = await CheckAssembly(actionExecutedContext.Request);
            if (statusCode == 299)
            {
                actionExecutedContext.Response.StatusCode = (HttpStatusCode)299;
            }
        }

        private Task<int?> CheckAssembly(HttpRequestMessage request)
        {
            IEnumerable<string> requestAssemblyVersions;
            request.Headers.TryGetValues("AssemblyVersion", out requestAssemblyVersions);

            var requestAssemblyVersionString = requestAssemblyVersions?.FirstOrDefault();
            Version requestAssemblyVersion;
            Version currentAssemblyVersion = typeof(WebApiApplication).Assembly.GetName().Version;
            if (requestAssemblyVersionString.HasValue())
            {
                if (!Version.TryParse(requestAssemblyVersionString, out requestAssemblyVersion))
                {
                    return Task.FromResult((int?)499);
                }

                if (currentAssemblyVersion == requestAssemblyVersion)
                {
                    return Task.FromResult((int?)null);
                }

                if (requestAssemblyVersion.Revision != currentAssemblyVersion.Revision &&
                    requestAssemblyVersion.Build == currentAssemblyVersion.Build &&
                    requestAssemblyVersion.Minor == currentAssemblyVersion.Minor &&
                    requestAssemblyVersion.Major == currentAssemblyVersion.Major)
                {
                    return Task.FromResult((int?)299);
                }
            }
            else
            {
                return Task.FromResult((int?)null);
            }

            return Task.FromResult((int?)499);
        }

        private static bool SkipAssemblyCheck(HttpActionContext actionContext)
        {
            return actionContext.ActionDescriptor.GetCustomAttributes<SkipAssemblyCheckAttribute>().Any()
                   || actionContext.ControllerContext.ControllerDescriptor.GetCustomAttributes<SkipAssemblyCheckAttribute>().Any()
                   || actionContext.IsInternalRequest();
        }
    }
}