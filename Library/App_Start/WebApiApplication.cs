﻿using Castle.Windsor;
using Castle.Windsor.Installer;
using Library.Database.Domain;
using Library.Extensions.Helpers;
using System;
using System.Configuration;
using System.Data.Entity;
using System.Web.Http;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;

namespace Library.App_Start
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        public static IWindsorContainer Container;
        protected static bool IsInitApplication = false;

        private static object _syncInitApplication = new object();

        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            GlobalConfiguration.Configure(WebApiConfig.Register);
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);

            InitApplication();
        }

        public static IWindsorContainer InitApplication()
        {
            lock (_syncInitApplication)
            {
                try
                {
                    if (!IsInitApplication)
                    {
                        InitDatabase();
                        InitDIContainer();
                        MapperConfig.Init();
                        IsInitApplication = true;
                    }
                    return Container;
                }
                catch (Exception ex)
                {
                    var cfg = (System.Web.Configuration.CompilationSection)ConfigurationManager.GetSection("system.web/compilation");
                    if (cfg.Debug == false)
                    {
                        var site = ConfigurationManager.AppSettings["defaultSiteName"];
                        NotificationHelper.SendException(ex.Message, ex.ToString(), site);
                    }
                    throw;
                }
            }
        }

        private static void InitDatabase()
        {
            System.Data.Entity.Database.SetInitializer(new MigrateDatabaseToLatestVersion<ApplicationDbContext, Database.Migrations.Configuration>());
            var configuration = new Database.Migrations.Configuration();
            var migrator = new System.Data.Entity.Migrations.DbMigrator(configuration);
            migrator.Update();
        }

        public static IWindsorContainer InitDIContainer()
        {
            if (Container == null)
            {
                Container = WindsorHelper.InitDI();
                Container.Install(FromAssembly.InThisApplication());
            }
            return Container;
        }

        protected void Application_End()
        {
            Container.Dispose();
            base.Dispose();
        }

        protected void Application_Error()
        {
            var error = Server?.GetLastError();
            if (error != null)
            {
                var httpError = error as System.Web.HttpException;
                if (httpError?.GetHttpCode() != 404)
                {
                    var helper = new NotificationHelper();
                    if (helper.Site != "localhost")
                    {
                        helper.Exception(error, Request);
                    }
                }
            }
        }
    }
}